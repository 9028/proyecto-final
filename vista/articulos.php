<!DOCTYPE html>
    <html lang="es">
        <head> 
            <!-- ============ DEPENDENCIAS ============ -->
           <?php include_once("../vista/includes/dependenciasHeader.html"); ?>
           <link rel="stylesheet" href="../vista/css/articulos.css">
           <link rel="stylesheet" href="../vista/css/iconosPlataforma.css">
        </head>
        <body>
            <!-- ============ NAVBAR ============ -->
            <?php include_once("../vista/navbar.php"); ?>
            
            <!-- ============ WRAPPER ============ -->
            <main class="wrapper">

                <!-- ============ ICONOS PLATAFORMAS ============ -->
                <section class="plataformas d-flex justify-content-around w-75">
                    <article><a href="./filtros.php?plataforma=1"><span class="iconify" data-icon="cib:playstation" data-inline="false"></span></a></article>    
                    <article><a href="./filtros.php?plataforma=2"><span class="iconify" data-icon="cib:xbox" data-inline="false"></span></a></article>
                    <article><a href="./filtros.php?plataforma=3"><span class="iconify" data-icon="fa-brands:nintendo-switch" data-inline="false"></span></a></article>
                    <article><a href="./filtros.php?plataforma=4"><span class="iconify" data-icon="cib:steam" data-inline="false"></span></a></article>        
                </section>
            
                <!-- ============ NOVEDADES (ESCAPARATE) ============ -->
                <div class="escaparate mt-4">
                    <h1 class="tituloSeccion">NOVEDADES</h1>
                </div>
                <section class="escaparate encuadre">
                    <?php
                        foreach($ultimosVideojuegos as $videojuego) {
                    ?>
                            <article class="tarjeta">
                                <a href='./videojuego.php?id=<?=$videojuego->id?>'><?= $videojuego->obtenerPortada(); ?></a>
                                <div class="precioTarjeta"><?=$videojuego->obtenerPrecioActual()?>€</div>
                                <div class="titulo"> <?=$videojuego->titulo?> </div>
                            </article>
                    <?php
                        }
                    ?>
                </section>    

                <!-- ============ BANNER PUBLICITARIO ============ -->
                <a href="./filtros.php?genero=3"><img class='banner mt-5 mb-5' src="../vista/img/banners/banner-home.jpg" alt="Banner publicitario"></a>
                

                <!-- ============ RECOMENDADOS (ESCAPARATE) ============ -->
                <div class="escaparate">
                    <h1 class="tituloSeccion">RECOMENDADOS</h1>
                </div>
                <section class="escaparate mb-4 encuadre">
                    <?php
                        foreach($videojuegosMasBaratos as $videojuego) {
                    ?>
                            <article class="tarjeta">
                                <a href='./videojuego.php?id=<?=$videojuego->id?>'><?= $videojuego->obtenerPortada(); ?></a>
                                <div class="precioTarjeta"><?=$videojuego->obtenerPrecioActual()?>€</div>
                                <div class="titulo"> <?=$videojuego->titulo?> </div>
                            </article>
                    <?php
                        }
                    ?>
                </section>    
            </main>
            <!-- ============ FIN WRAPPER ============ -->

            <!-- ============ FOOTER Y DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/footer.html"); ?>
            <?php include_once("../vista/includes/dependenciasBody.html"); ?>
        </body>
    </html>