<!DOCTYPE html>
    <html lang="es">
        <head> 
            <!-- ============ DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/dependenciasHeader.html"); ?> 
            <link rel="stylesheet" href="../vista/css/cpanel.css">
            <link rel="stylesheet" href="../vista/css/tabla.css">
        </head>
        <body>
            <!-- ============ NAVBAR ============ -->
            <?php include_once("../vista/navbar.php"); ?>

            <!-- ============ WRAPPER ============ -->
            <main class="wrapper">

                <!-- ============ DROPDOWN ============ -->
                <?php include_once("../vista/includes/dropdownSecciones.html"); ?>

                <!-- ============ TABLA ============ -->
                <div class="table-responsive tablaUsuarios encuadreNoVideojuegos mb-4">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Título</th>
                                <th>Precio actual</th>
                                <th>Precio original</th>
                                <th>Descuento</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($videojuegos as $videojuego) {
                                  ?>
                                  <tr>
                                      <td><?= $videojuego->id ?></td>
                                      <td><?= $videojuego->titulo ?></td>
                                      <td><?= $videojuego->obtenerPrecioActual() ?>€</td>
                                      <td><?= $videojuego->precio ?>€</td>
                                      <td><?= $videojuego->rebaja ?>%</td>
                                  </tr>
                                  <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>

                <!-- ============ PESTAÑAS ============ -->
                <div class="desplegable encuadreNoVideojuegos">
                   
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#anadir">Añadir</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#modificar">Modificar precio</a>
                        </li>

                    </ul>

                    <div class="tab-content">

                        <!-- ============ AÑADIR VIDEOJUEGO ============ -->
                        <div class="tab-pane container active" id="anadir">
                            <h4 class="mt-2">Añade un videojuego</h4>
                            <form method="post" class="anadirVideojuego" enctype="multipart/form-data">
                                <div class="contenedorInputs">
                                    <div class="inputParticular">
                                        <div class="label">Título</div>
                                        <input type="text" name="tituloVideojuego" placeholder="Mario Bros" title="Título del videojuego." required>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Precio</div>
                                        <input type="number" name="precioVideojuego" min="0" max="999" placeholder="50"  title="Precio del videojuego." required>€        
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Descuento</div>
                                        <input type="number"  min="0" max="100" value="0" name="rebajaVideojuego" placeholder="70" title="Descuento del videojuego." required>%
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">PEGI</div>
                                        <input type="number" min="3" max="100" name="pegiVideojuego" placeholder="18" title="Edad recomendada para el videojuego." required>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Fecha salida</div>
                                        <input type="date" name="fechaVideojuego" placeholder="12/01/2018" title="Fecha de salida del videojuego." required>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Trailer</div>
                                        <input type="text" name="trailerVideojuego" placeholder="Link embed" title="Enlace del trailer en formato embebido." required><br>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Descripción</div>
                                        <textarea name="descripcionVideojuego" cols="50" rows="5" title="Descripción del videojuego." required></textarea>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Portada (jpg)</div>
                                        <input name="portadaVideojuego" type="file" required/>
                                        
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Screenshots (jpg)</div>
                                        <input name="screenshotVideojuego" type="file" required/><br>
                                        <input name="screenshot2Videojuego" type="file" required/>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Idiomas</div>
                                        <input type="checkbox" name="idiomaEspanol" value="1"> Español
                                        <input type="checkbox" name="idiomaIngles" value="2"> Inglés
                                        <input type="checkbox" name="idiomaFrances" value="3"> Francés<br>
                                        <input type="checkbox" name="idiomaItaliano" value="4"> Italiano
                                        <input type="checkbox" name="idiomaAleman" value="5"> Alemán
                                        <input type="checkbox" name="idiomaCheco" value="6"> Checo <br>
                                        <input type="checkbox" name="idiomaHolandes" value="7"> Holandés
                                        <input type="checkbox" name="idiomaJapones" value="8"> Japonés
                                        <input type="checkbox" name="idiomaCoreano" value="9"> Coreano<br>
                                        <input type="checkbox" name="idiomaPolaco" value="10"> Polaco
                                        <input type="checkbox" name="idiomaPortugues" value="11"> Portugués
                                        <input type="checkbox" name="idiomaRuso" value="12"> Ruso <br>
                                        <input type="checkbox" name="idiomaChino" value="13"> Chino
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Géneros</div>
                                        <input type="checkbox" name="generoAccion" value="1"> Acción
                                        <input type="checkbox" name="generoArcade" value="2"> Arcade
                                        <input type="checkbox" name="generoAventura" value="3"> Aventura <br>
                                        <input type="checkbox" name="generoBeatemall" value="4"> Beat'em all
                                        <input type="checkbox" name="generoCarreras" value="5"> Carreras
                                        <input type="checkbox" name="generoCooperacion" value="6"> Cooperación <br>
                                        <input type="checkbox" name="generoDeporte" value="7"> Deporte
                                        <input type="checkbox" name="generoEstrategia" value="8"> Estrategia
                                        <input type="checkbox" name="generoFPS" value="9"> FPS <br>
                                        <input type="checkbox" name="generoLucha" value="10"> Lucha
                                        <input type="checkbox" name="generoMultijugador" value="11"> Multijugador
                                        <input type="checkbox" name="generoPlataformas" value="12"> Plataformas <br>
                                        <input type="checkbox" name="generoRPG" value="13"> RPG
                                        <input type="checkbox" name="generoSimulacion" value="14"> Simulación
                                    </div>

                                </div>
                                <input name="operacion" type="hidden" value="anadirVideojuego">
                                <button class="btn d-block mt-2 boton" type="submit">Añadir</button>
                                <div class="invalid-feedback"></div>
                            </form>

                        </div>

                        <!-- ============ MODIFICAR PRECIO VIDEOJUEGO ============ -->
                        <div class="tab-pane container fade" id="modificar">
                            <h4 class="mt-2">Modifica el precio de un videojuego</h4>
                            <form method="post" class="modificarPrecioVideojuego">
                                <div class="contenedorInputs">
                                    <div class="inputParticular">
                                        <div class="label">ID Videojuego</div>
                                        <input type="number" name="idVideojuego" placeholder="1" min="1" max="9999" required>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Precio</div>
                                        <input type="number" name="precioVideojuego" min="0" max="9999" placeholder="50" required>€
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Descuento</div>
                                        <input type="number" name="descuentoVideojuego" min="0" max="9999" placeholder="0" required>%
                                    </div>
                                </div>
                                
                                <input name="operacion" type="hidden" value="modificarPrecioVideojuego">
                                <button class="btn d-block mt-2 boton" type="submit">Modificar</button>
                                <div class="invalid-feedback"></div>
                            </form>
                        </div>
                    </div>
                </div>

            </main>
            <!-- ============ FIN WRAPPER ============ -->

            <!-- ============ FOOTER Y DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/footer.html"); ?>
            <script src="../vista/js/dataTables/videojuego.js"></script>
            <?php include_once("../vista/includes/dependenciasBody.html"); ?>
        </body>
    </html>