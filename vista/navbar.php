 
<nav class="navbar">

    <!-- ============ BOTÓN DESPLEGABLE Y LOGO ============ -->
    <div class="box left">
            <button class="navbar-toggler toggler-example border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><i class="fas fa-bars"></i></button>
            <a href="./articulos.php">
                <img src="../vista/img/logo.png" alt="Logo getgames" width="80" height="55">
            </a>
        </div>
    </div>

    <!-- ============ BÚSQUEDA ============ -->
    <div class="box center">

        <!-- ============ MOVIL ============ -->
        <button type="button" class="search-icon"><i class="fas fa-search"></i></button>

        <!-- ============ TABLET Y ESCRITORIO ============ -->
        <div class="input-group searchbar">
                <input  name="inputBusqueda" id="inputBusqueda" type="text" class="form-control inputBusqueda" placeholder="FIFA, Minecraft..." aria-label="FIFA, Minecraft..." aria-describedby="basic-addon2">
                <div class="resultadoBusqueda">
                </div>
            <div class="input-group-append">
                <button class="btn botonAccionBusqueda" type="button">Buscar</button>
            </div>
        </div>

    </div>

    <!-- ============ USUARIO Y CARRITO ============ -->
    <div class="box right">
        <?php
            if(Usuario::usuarioLogeado()) {
                $usuario = $_SESSION['usuario'];
                $nombreUsuario = $usuario->usuario;
                echo "<a href='./biblioteca.php' class='icono'><i class='fas fa-user'></i><span class='nombreUsuario'>$nombreUsuario</span></a>";
            } else {
                echo '<button type="button" class="btn btn-primary icono" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-user"></i></button>';
            }
        ?>
        <a href="./carrito.php" class="icono"><i class="fas fa-shopping-cart"></i></a>
    </div>

    <!-- ============ DESPLEGABLE (ENLACES) ============ -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent1">

        <ul class="navbar-nav mr-auto">
            
            <?php
                if((Usuario::usuarioLogeado())) {
                    $usuario = $_SESSION['usuario'];

                    if($usuario->administrador) {
                        ?>
                            <li class="nav-item">
                                <a class="nav-link" href="cpanel.php">Panel de control</a>
                            </li>
                        <?php
                    }

                    
            ?>
                    <li class="nav-item">
                        <a class="nav-link" href="biblioteca.php">Biblioteca</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="configuracion.php">Configuración</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="cerrarSesion.php">Cerrar sesión</a>
                    </li>  
            <?php
                } else {
                ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="" data-toggle="modal" data-target="#exampleModalCenter">Iniciar sesión/Registro <span class="sr-only">(current)</span></a>
                    </li>
            <?php
                }
            ?>
        </ul>

    </div>
</nav>

<!-- ============ DEPENDENCIAS NAVBAR ============ -->
<?php include_once("../vista/login.php"); ?>
<script src="../vista/js/navbar.js"></script>