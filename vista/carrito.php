<!DOCTYPE html>
    <html lang="es">
        <head> 
            <!-- ============ DEPENDENCIAS ============ -->
            <meta charset="utf-8">
            <?php include_once("../vista/includes/dependenciasHeader.html"); ?> 
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link href="../vista/css/carrito.css" rel="stylesheet"/>
            <link href="../vista/css/tabla.css" rel="stylesheet"/>
        </head>
        <body>
            <!-- ============ NAVBAR ============ -->
            <?php include_once("../vista/navbar.php"); ?>

            <main class="wrapper">
                <h1>Carrito</h1>
                <section class="containerCarrito encuadre mb-4">
                    <?php
                        if(!isset($_SESSION['usuario'])) {
                            ?>
                                <h2><span class="iniciaSesion" data-toggle="modal" data-target="#exampleModalCenter">Inicia sesión</span> para poder usar el carrito.</h2>
                            <?php
                        } else if ($carrito->estaVacia()){
                            ?>
                                <h2>El carrito está vacío.</h2>
                            <?php
                        } else {
                            ?>
                                <!-- ============ CARRITO ============ -->
                                <h2>Lista de videojuegos</h2>
                                    <div class="table-responsive tablaCarrito">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Videojuego</th>
                                                    <th>Plataforma</th>
                                                    <th>Cantidad</th>
                                                    <th>Precio</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    
                                                    foreach($carrito->obtenerProductos() as $producto) {
                                                        $plataformas = $producto['plataformas'];
                                                        $videojuego = $producto['videojuego'];
                                                        foreach($plataformas as $key=>$plataforma) {
                                                            $nombrePlataforma = Videojuego::obtenerLogoPlataforma($key);
                                                            $cantidad = intval($plataforma["cantidad"]);
                                                            $idVideojuego = $videojuego->id;
                                                            echo "<form method='post' action=''>";
                                                            echo "<tr>";
                                                                echo "<td><strong><a href='./videojuego.php?id=$idVideojuego'>$videojuego->titulo</a><strong></td>";
                                                                echo "<td>$nombrePlataforma</td>";
                                                                echo "<td>x$cantidad</td>";
                                                                echo "<td>".$videojuego->obtenerPrecioActual()*$cantidad."€</td>";
                                                                // echo "<td>$cantidad</td>";
                                                                echo "<td class='botoneraCarrito'>
                                                                        <button type='submit' name='anadir' value='$idVideojuego' class='btn text-white boton' $disabled>+</button><button type='submit' name='restar' value='$idVideojuego' class='btn text-white boton'>-</button>
                                                                        <input type='hidden' name='plataforma' value='$key'>
                                                                    </td>";
                                                            echo "</tr>";
                                                            echo "</form>";
                                                        }
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                        <!-- ============ TOTAL Y PAGO ============ -->
                                        <div class="totalSubmit">
                                            <span><strong>TOTAL: <?=$carrito->obtenerTotalPrecio()."€";?></strong></span>
                                            <a class="btn text-white boton" href="pagar.php">Pagar</a>
                                        </div>
                                    </div>
                            <?php
                        }
                    ?>
                </section>
                
            </main>
            <!-- ============ FIN WRAPPER ============ -->

            <!-- ============ FOOTER Y DEPENDENCIAS ============ -->
            <script src="../vista/js/carrito.js"></script>
            <?php include_once("../vista/includes/footer.html"); ?>
            <?php include_once("../vista/includes/dependenciasBody.html"); ?> 
        </body>
    </html>