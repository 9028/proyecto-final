                    <!-- Modal -->
                    <div class="modal fade modalLogin" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Login</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="login">
                                <form method="POST">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Usuario:</label>
                                    <input type="text"  name="usuario" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Introduce tu usuario." required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Contraseña:</label>
                                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Introduce tu contraseña." required>
                                </div>
                                <button type="submit" class="btn btn-primary boton">Acceder</button>                                
                                </form>
                            </div>
                            <div class="invalid-feedback">
                                
                            </div>
                        </div>
                        <div class="modal-footer">
                            <span>¿No tienes usuario? <a href="./registro.php" class="enlace">Regístrate</a></span>
                        </div>
                        </div>
                    </div>
                    </div>