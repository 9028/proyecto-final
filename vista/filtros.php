<!DOCTYPE html>
    <html lang="es">
        <head> 
            <!-- ============ DEPENDENCIAS ============ -->
           <?php include_once("../vista/includes/dependenciasHeader.html"); ?>
           <link rel="stylesheet" href="../vista/css/articulos.css">
           <link rel="stylesheet" href="../vista/css/filtros.css">
        </head>
        <body>
            <!-- ============ NAVBAR ============ -->
            <?php include_once("../vista/navbar.php"); ?>
            
            <!-- ============ WRAPPER ============ -->
            <main class="wrapper">

                <h1>Buscador</h1>
                <form action="" class="filtros mb-3" method="get">
                    <div class="form-group">
                        <label for="plataforma">Plataforma</label>
                        <select class="form-control" name="plataforma">
                            <option selected value="todas">Todas</option>
                        <?php
                            foreach($plataformas as $plataforma) {
                                $id = $plataforma['id'];
                                $nombre = $plataforma['plataforma'];
                                echo "<option value='$id'>$nombre</option>";
                            }
                        ?>
                         </select>
                    </div>

                    <div class="form-group">
                        <label for="genero">Género</label>
                        <select class="form-control" name="genero">
                            <option disabled selected value>Elige un género</option>
                        <?php
                            foreach($generos as $genero) {
                                $id = $genero['id'];
                                $nombre = $genero['genero'];
                                echo "<option value='$id'>$nombre</option>";
                            }
                        ?>
                         </select>
                    </div>

                    <div class="form-group">
                        <label for="plataforma">Precio</label>
                        <select class="form-control" name="precio">
                            <option disabled selected value>Filtra por precio</option>
                            <option value="mayor">Precio ↑</option>
                            <option value="menor">Precio ↓</option>
                        </select>
                    </div>
                    
                    <button class="btn text-white boton" type="submit">Buscar</button>
                    <input type="hidden" name="operacion" value="busquedaFiltros">
                </form>
                <?php
                    if(isset($busqueda)) {
                        if($busqueda != "" && count($videojuegos)==0) {
                             echo "<div class='errorResultados text-center font-weight-bold'>No se han encontrado resultados de $busqueda</div>";
                        }

                    } else if(isset($genero)) {
                        if(isset($videojuegos) && count($videojuegos)==0) {
                            echo "<div class='errorResultados text-center font-weight-bold'>No se han encontrado resultados</div>";
                        }
                    }
                   
                ?>
                <section class="escaparate">
                    <?php
                        if(isset($videojuegos)) {

                            foreach($videojuegos as $videojuego) {
                            ?>
                            <article class="tarjeta">
                                <a href='./videojuego.php?id=<?=$videojuego->id?>'><?= $videojuego->obtenerPortada(); ?></a>
                                <div class="precioTarjeta"><?=$videojuego->obtenerPrecioActual()?>€</div>
                                <div class="titulo"> <?=$videojuego->titulo?> </div>
                            </article>
                            <?php
                            }
                        }
                    ?>
                </section>
                <section class="botoneraLoadMore">
                    <button class="loadMore btn text-white mt-3 boton">Cargar más</button> 
                </section>
                
            </main>
            <!-- ============ FIN WRAPPER ============ -->

            <!-- ============ FOOTER Y DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/footer.html"); ?>
            <?php include_once("../vista/includes/dependenciasBody.html"); ?>
            <script src="../vista/js/filtros.js"></script>
        </body>
    </html>