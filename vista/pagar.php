<!DOCTYPE html>
    <html lang="es">
        <head> 
            <!-- ============ DEPENDENCIAS ============ -->
            <meta charset="utf-8">
            <?php include_once("../vista/includes/dependenciasHeader.html"); ?> 
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="../vista/css/pagar.css">
        </head>
        <body>
            <!-- ============ NAVBAR ============ -->
            <?php include_once("../vista/navbar.php"); ?>

            <!-- ============ WRAPPER ============ -->
            <main class="wrapper">

                <!-- ============ COMPRA REALIZADA ============ -->
                <section class="compraRealizada text-center encuadre mb-4 mt-4">
                    <h2 class="text-success">¡Compra realizada! <i class="fas fa-check-circle"></i></h2>
                    <h3>Los videojuegos se encuentran en <a href="./biblioteca.php">tu biblioteca</a></h3>
                </section>

                <!-- ============ MÉTODOS PAGO ============ -->
                <h3>Selecciona un método de pago</h3>
                <section class="metodosPago encuadre p-5">
                    
                    <article class="tarjeta">
                        <div class="tituloPago">Tarjeta</div>
                        <div class="pagoPrincipal">
                            <span class="iconify" data-icon="emojione:credit-card" data-inline="false"></span>
                        </div>
                        <div class="pagoInfo">
                            <span class="iconify" data-icon="logos:visa" data-inline="false"></span>
                            <span class="iconify" data-icon="grommet-icons:mastercard" data-inline="false"></span>
                        </div>
                    </article>

                    <article class="paypal">
                        <div class="tituloPago">Paypal</div>
                        <div class="pagoPrincipal">
                            <span class="iconify" data-icon="logos:paypal" data-inline="false"></span>
                        </div>
                    </article>
                </div>

                <!-- ============ COMPRAR (TARJETA) ============ -->
                <form action="" method="post" class="formularioTarjeta">
                    <div class="infoPago">
                        <div class="form-group row">
                             <div class="col-12"><h4>Información de la tarjeta</h4></div>
                            <label for="numeroTarjeta" class=" col-sm-6">Número tarjeta</label>
                            <div class="col-12">
                                <input type="text" class="form-control" name="numeroTarjeta" placeholder="4012888888881881" pattern ="^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14})$" title="Número de tarjeta VISA o MasteCard" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fechaExpiracion" class="col-12 col-form-label">Fecha expiración</label>
                            <div class="col-6">
                                <input type="text" class="form-control" name="fechaExpiracionMes" placeholder="MM" pattern="^(0?[1-9]|1[012])$" title="Mes expiración (1-12)" required>
                                
                            </div>
                            <div class="col-6">
                                <input type="text" class="form-control" name="fechaExpiracionAnio" placeholder="YY" pattern="^([0-9]{4})$" title="Año expiración" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cv" class="col-12 col-form-label">CV</label>
                            <div class="col-6">
                                <input type="text" class="form-control" name="cv" placeholder="000" pattern="^([0-9]{3})$" title="Tres números." required>
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="infoFacturacion">
                        <div class="form-group row">
                            <div class="col-12"><h4>Información de facturación&nbsp;&nbsp;</h4></div>
                            <label for="nombre" class="col-12 col-form-label">Nombre</label>
                            <div class="col-12">
                                <input type="text" class="form-control" name="nombre" placeholder="John" pattern="^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$" title="Tu nombre sin apellidos." required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="apellidos" class="col-12 col-form-label">Apellidos</label>
                            <div class="col-12">
                                <input type="text" class="form-control" name="apellidos" placeholder="Doe" pattern="^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$" title="Tus apellidos." required>
                            </div>
                            
                        </div>

                        <div class="form-group row">
                            <label for="direccionFacturacion" class="col-12 col-form-label">Dirección de facturación</label>
                            <div class="col-12">
                                <input type="text" class="form-control" name="direccionFacturacion" placeholder="Av. España, Portal 5, 3º A" pattern="^(.+)$" title="Tu dirección personal." required>
                            </div>
                            
                        </div>

                        <div class="form-group row">
                            
                            <div class="col-6">
                                <label for="localidad" class="col-12 col-form-label">Localidad</label>
                                <input type="text" class="form-control" name="localidad" placeholder="Ceuta" pattern="^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$" title="Localidad donde vives." required>
                            </div>

                            <div class="col-6">
                                <label for="pais" class="col-12 col-form-label">Pais</label>
                                <input type="text" class="form-control" name="pais" placeholder="España" pattern="^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$" title="País donde vives." required>
                            </div>
                            
                        </div>

                        <div class="form-group row">
                            <label for="cp" class="col-12 col-form-label">Código postal o zip</label>
                            <div class="col-6">
                                <input type="text" class="form-control" name="cp" placeholder="51002" title="Tu código postal (solo números)." required>
                            </div>
                            <div class="col-6 d-flex justify-content-center">
                                <button type="submit" class="btn text-white boton">Pagar</button>
                            </div>
                            
                        </div>
                    </div>
                    <input type="hidden" name="operacion" value="pagar">
                
                </form>
                
                <!-- ============ COMPRAR (PAYPAL) ============ -->
                <form action="" method="post" class="formularioPaypal">
                    <div class="infoPago">
                        <div class="form-group row">
                             <div class="col-12"><h4>Información de Paypal</h4></div>
                            <label for="email" class=" col-sm-6">E-mail</label>
                            <div class="col-12">
                                <input type="email" class="form-control" name="email" placeholder="johndoe@gmail.com" title="Inserta tu e-mail." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-12 col-form-label">Contraseña</label>
                            <div class="col-12">
                                <input type="password" class="form-control" name="password" pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._/+:;=#?!@$%^&*-]).{8,}$" title="8 caracteres como mínimo, siendo obligatorio que contenga al menos un: caracter especial, número y letra mayúscula." required>
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="infoFacturacion">
                        <div class="form-group row">
                            <div class="col-12"><h4>Información de facturación&nbsp;&nbsp;</h4></div>
                            <label for="direccionFacturacion" class="col-12 col-form-label">Nombre</label>
                            <div class="col-12">
                                <input type="text" class="form-control" name="nombre" placeholder="John" pattern="^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$" title="Tu nombre sin apellidos." required>
                            </div>
                            
                        </div>

                        <div class="form-group row">
                            <label for="direccionFacturacion" class="col-12 col-form-label">Apellidos</label>
                            <div class="col-12">
                                <input type="text" class="form-control" name="apellidos" placeholder="Doe" pattern="^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$" title="Tus apellidos." required>
                            </div>
                            
                        </div>

                        <div class="form-group row">
                            <label for="direccionFacturacion" class="col-12 col-form-label">Dirección de facturación</label>
                            <div class="col-12">
                                <input type="text" class="form-control" name="direccionFacturacion" placeholder="Av. España, Portal 5, 3º A" pattern="^(.+)$" title="Tu dirección personal." required>
                            </div>
                            
                        </div>

                        <div class="form-group row">
                            
                            <div class="col-6">
                                <label for="direccionFacturacion" class="col-12 col-form-label">Localidad</label>
                                <input type="text" class="form-control" name="localidad" placeholder="Ceuta" pattern="^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$" title="Localidad donde vives." required>
                            </div>

                            <div class="col-6">
                                <label for="direccionFacturacion" class="col-12 col-form-label">Pais</label>
                                <input type="text" class="form-control" name="pais" placeholder="España" pattern="^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$" title="País donde vives." required>
                            </div>
                            
                        </div>



                        <div class="form-group row">
                            <label for="cp" class="col-12 col-form-label">Código postal o zip</label>
                            <div class="col-6">
                                <input type="text" class="form-control" name="cp" placeholder="51002" title="Tu código postal (solo números)." required>
                            </div>
                            <div class="col-6 d-flex justify-content-center">
                                <button type="submit" class="btn text-white boton">Pagar</button>
                            </div>
                            
                        </div>
                    </div>

                    <input type="hidden" name="operacion" value="pagar">
                </form>
                <div class="invalid-feedback fPago text-center font-weight-bold"></div>

            </main>
            <!-- ============ FIN WRAPPER ============ -->

            <!-- ============ FOOTER Y DEPENDENCIAS ============ -->
            <script type="module" src="../vista/js/pagar.js"></script>
            <?php include_once("../vista/includes/footer.html"); ?>
            <?php include_once("../vista/includes/dependenciasBody.html"); ?> 
        </body>
    </html>