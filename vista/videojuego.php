<!DOCTYPE html>
    <html lang="es">
        <head>
            <!-- ============ DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/dependenciasHeader.html"); ?> 
            <link rel="stylesheet" href="../vista/css/videojuego.css">
        </head>
        <body>  
            <!-- ============ NAVBAR ============ -->     
            <?php include_once("../vista/navbar.php"); ?>

            <!-- ============ WRAPPER ============ -->
            <main class="wrapper">
                <h1 class='titulo pt-3'><?=$videojuegoActual->titulo?></h1>

                <!-- ============ INFO PRINCIPAL ============ -->
                <section class="infoPrincipal">

                    <!-- ============ SLIDER ============ -->
                    <article class="galeria">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="false">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <?= $videojuegoActual->trailer ?>
                                </div>
                                <div class="carousel-item">
                                <img class="d-block w-100" src="../vista/img/screenshots/<?=$videojuegoActual->id?>/1.jpg">
                                </div>
                                <div class="carousel-item">
                                <img class="d-block w-100" src="../vista/img/screenshots/<?=$videojuegoActual->id?>/2.jpg">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </article>
 
                    <!-- ============ COMPRA ============ -->
                    <article class="compra encuadre">
                        <form method="post" action="">
                            <div class="form-group">

                                <!-- ============ PLATAFORMAS DISPONIBLES ============ -->
                                <select class="form-control" name="opcionPlataforma" id="opcionPlataforma" class="opcionPlataforma">
                                    <option disabled selected value>Elige una plataforma</option>
                                    <?php                          
                                        foreach($plataformasDisponibles as $plataforma) {
                                            $id = $plataforma['idPlataforma'];
                                            $nombre = $plataforma['nombrePlataforma'];
                                            echo "<option value='$id'>$nombre</option>";
                                        }
                                    ?>
                                </select>
                            </div>

                            <!-- ============ IDIOMAS DISPONIBLES ============ -->
                            <div class="idiomasDisponibles">
                                <span class="textoIdiomas">Idiomas disponibles:</span>
                                <?php
                                    foreach($idiomas as $idioma) {
                                        $codigoIdioma = $idioma['id_idioma'];
                                        echo "<img src='../vista/img/banderas/$codigoIdioma.png'>";
                                    }
                                ?>
                            </div>

                            <!-- ============ GÉNEROS DISPONIBLES ============ -->
                            <div class="generos">
                                <?php
                                    foreach($generos as $genero) {
                                        echo "<span class='genero'>$genero</span>";
                                    }
                                ?>
                            </div>
                            
                        
                            <!-- ============ PRECIO ============ -->
                            <div class="precio">
                                <span class="precioSinRebaja">Precio original: <?=$videojuegoActual->precio?>€</span>
                                <span class="precioConRebaja"><span class="descuento">-<?=$videojuegoActual->rebaja?>%</span><?= $videojuegoActual->obtenerPrecioActual()?>€</span>
                            </div>

                            <!-- ============ PAGAR ============ -->
                            <div class="botonDoble">
                                <input type="hidden" name="idVideojuego" value="<?=$idVideojuego?>">
                                <input type="hidden" name="operacion" value="anadirVideojuegoCarrito">
                                
                                <button class="btn botonCarrito boton" name="botonCarrito" type="submit"><i class="fas fa-cart-plus"></i></button>
                                <a class="btn botonComprar boton" onclick="detectarOption(event)" href="./carrito.php?v=<?=$idVideojuego?>">Comprar</a>
                            </div>
                        </form>
                        <div class="invalid-feedback text-center"></div>
                    </article>
                </section>

                <!-- ============ INFO SECUNDARIA ============ -->
                <h2 class='subtitulo'>INFORMACIÓN</h2>
                <section class="infoSecundaria">
                    <p class="text-justify">
                        <?= $videojuegoActual->descripcion ?>
                    </p>
                </section>

                <!-- ============ MÁS VENDIDOS ============ -->
                <h2 class='subtitulo'>MÁS VENDIDOS</h2>
                <section class="escaparate">
                    <?php
                        foreach($videojuegosMasVendidos as $videojuego) {
                    ?>
                        <article class="tarjeta">
                            <a href='./videojuego.php?id=<?=$videojuego->id?>'><?= $videojuego->obtenerPortada(); ?></a>
                            <div class="titulo"> <?=$videojuego->titulo?> </div>
                        </article>
                    <?php
                        }
                    ?>
                </section>  
            </main>
            <!-- ============ FIN WRAPPER ============ -->

            <!-- ============ FOOTER Y DEPENDENCIAS ============ -->
            <script src="../vista/js/compra.js"></script>
            <?php include_once("../vista/includes/footer.html"); ?>
            <?php include_once("../vista/includes/dependenciasBody.html"); ?>     
        </body>
    </html>