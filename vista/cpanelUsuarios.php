<!DOCTYPE html>
    <html lang="es">
        <head> 
            <!-- ============ DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/dependenciasHeader.html"); ?> 
            <link rel="stylesheet" href="../vista/css/cpanel.css">
            <link rel="stylesheet" href="../vista/css/tabla.css">
        </head>
        <body>
             <!-- ============ NAVBAR ============ -->
            <?php include_once("../vista/navbar.php"); ?>

            <!-- ============ WRAPPER ============ -->
            <main class="wrapper">

                <!-- ============ DROPDOWN ============ -->
                <?php include_once("../vista/includes/dropdownSecciones.html"); ?>

                <!-- ============ TABLA ============ -->
                <div class="table-responsive tablaUsuarios encuadreNoVideojuegos mb-4">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Contraseña</th>
                                <th>Admin</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($usuarios as $usuario) {
                                  ?>
                                  <tr>
                                      <td><?= $usuario->usuario ?></td>
                                      <td><?= $usuario->password ?></td>
                                      <td><?= Usuario::sino($usuario->administrador) ?></td>
                                  </tr>
                                  <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>

                <!-- ============ PESTAÑAS ============ -->
                <div class="desplegable encuadreNoVideojuegos">
                    
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#anadir">Añadir</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#modificar">Modificar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#eliminar">Eliminar</a>
                        </li>
                    </ul>

                    
                    <div class="tab-content">

                        <!-- ============ AÑADIR USUARIO ============ -->
                        <div class="tab-pane container active" id="anadir">
                            <h4 class="mt-2">Añade un usuario</h4>

                            <form method="post" class="anadirUsuario">
                                <div class="contenedorInputs">
                                    <div class="inputParticular">
                                        <div class="label">Nombre usuario</div>
                                        <input type="text" name="nombreUsuario" placeholder="JohnDoe" pattern="^[A-Za-z0-9]{1,8}$" title="Hasta 8 caracteres. Pueden usarse letras y números." required>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Contraseña</div>
                                        <input type="password" name="passwordUsuario" placeholder="_Example1" pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._/+:;=#?!@$%^&*-]).{8,}$" title="8 caracteres como mínimo, siendo obligatorio que contenga al menos un: caracter especial, número y letra mayúscula." required>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Administrador</div>
                                        <input type="text" name="administradorUsuario" placeholder="0" title="0 = No admin. 1 = Admin." pattern="^[0-1]$" required>    
                                    </div>
                                </div>
                                
                                <input name="operacion" type="hidden" value="anadirUsuario">
                                <button class="btn d-block mt-2 boton" type="submit">Añadir</button>
                                <div class="invalid-feedback"></div>
                            </form>
                        </div>

                        <!-- ============ MODIFICAR USUARIO ============ -->
                        <div class="tab-pane container fade" id="modificar">
                            <h4 class="mt-2">Modifica un usuario</h4>
                            <form method="post" class="modificarUsuario">

                                <div class="contenedorInputs">
                                    <div class="inputParticular">
                                        <div class="label">Nombre usuario</div>
                                        <input type="text" name="nombreUsuario" placeholder="JohnDoe" pattern="^[A-Za-z0-9]{1,8}$" title="Hasta 8 caracteres. Pueden usarse letras y números." required>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Contraseña</div>
                                        <input type="password" name="passwordUsuario" placeholder="_Example1" pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._/+:;=#?!@$%^&*-]).{8,}$" title="8 caracteres como mínimo, siendo obligatorio que contenga al menos un: caracter especial, número y letra mayúscula." required>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Administrador</div>
                                        <input type="text" name="administradorUsuario" placeholder="0" title="0 = No admin. 1 = Admin." pattern="^[0-1]$" required>    
                                    </div>
                                </div>
                                
                                <input name="operacion" type="hidden" value="modificarUsuario">
                                <button class="btn d-block mt-2 boton" type="submit">Modificar</button>
                                <div class="invalid-feedback"></div>
                            </form>
                        </div>

                        <!-- ============ ELIMINAR USUARIO ============ -->
                        <div class="tab-pane container fade" id="eliminar">
                            <h4 class="mt-2">Eliminar un usuario</h4>
                            <form method="post" class="eliminarUsuario">
                                <div class="contenedorInputs">
                                    <div class="inputParticular">
                                        <div class="label">Nombre usuario</div>
                                        <input type="text" name="nombreUsuario" placeholder="JohnDoe" pattern="^[A-Za-z0-9]{1,8}$" title="Hasta 8 caracteres. Pueden usarse letras y números." required>
                                    </div>
                                </div>
                                
                                <input name="operacion" type="hidden" value="eliminarUsuario">
                                <button type="submit" class="btn d-block mt-2 boton">Eliminar</button>
                                <div class="invalid-feedback"></div>
                            </form>
                        </div>
                    </div>
                </div>

            </main>
            <!-- ============ FIN WRAPPER ============ -->

            <!-- ============ FOOTER Y DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/footer.html"); ?>
            <script type="module" src="../vista/js/dataTables/usuario.js"></script>
            <?php include_once("../vista/includes/dependenciasBody.html"); ?>
        </body>
    </html>