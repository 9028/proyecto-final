<!-- VISTA LOGIN -->
<!DOCTYPE html>
    <html lang="es">
        <head> 
            <!-- ============ DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/dependenciasHeader.html"); ?>
            <link rel="stylesheet" href="../vista/css/registro.css">
        </head>
        <body>
            <!-- ============ NAVBAR ============ -->
            <?php include_once("../vista/navbar.php"); ?>

            <!-- ============ WRAPPER ============ -->
            <main class="content">
                <section class="registro encuadre p-3">
                    <h1>Cambia tu contraseña</h1>
                    <form method="POST">
                        <div class="form-group">
                            <label for="password">Contraseña actual:</label>
                            <input type="password" name="oldPassword" class="form-control" id="password" placeholder="_Example1" pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._/+:;=#?!@$%^&*-]).{8,}$" title="8 caracteres como mínimo, siendo obligatorio que contenga al menos un: caracter especial, número y letra mayúscula." required>
                        </div>
                      <div class="form-group">
                            <label for="password">Nueva contraseña:</label>
                            <input type="password" name="newPassword" class="form-control" id="password" placeholder="_Example1" pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._/+:;=#?!@$%^&*-]).{8,}$" title="8 caracteres como mínimo, siendo obligatorio que contenga al menos un: caracter especial, número y letra mayúscula." required>
                        </div>
                        <button type="submit" class="btn btn-primary boton">Enviar</button>
                    </form>
                    <div class="invalid-feedback"></div>
                </section>
                <section class="instrucciones">
                <h2>Instrucciones</h2>
                    <ul>
                        <li><strong>Contraseña</strong>: 8 caracteres como mínimo, siendo obligatorio que contenga al menos un: caracter especial, número y letra mayúscula.</li>
                    </ul>
                </section>
            </main>
            <!-- ============ FIN WRAPPER ============ -->

            <!-- ============ FOOTER Y DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/footer.html"); ?>
            <script type="module" src="../vista/js/cambiarContrasena.js"></script>
            <?php include_once("../vista/includes/dependenciasBody.html"); ?>
        </body>
    </html>