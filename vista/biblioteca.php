<!DOCTYPE html>
    <html lang="es">
        <head> 
            <!-- ============ DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/dependenciasHeader.html"); ?>  
            <link rel="stylesheet" href="../vista/css/articulos.css">    
            <link rel="stylesheet" href="../vista/css/tabla.css">         
        </head>
        <body>
            <!-- ============ NAVBAR ============ -->
            <?php include_once("../vista/navbar.php"); ?>

            <!-- ============ WRAPPER ============ -->
            <main class="wrapper">
                <h1>Tu biblioteca</h1>
                <section class="escaparate">
                    <?php
                        foreach($misVideojuegos as $videojuego) {
                    ?>
                            <article class="tarjeta">
                                <a data-id="<?=$videojuego->id?>" class="portadaBiblioteca" data-dismiss="modal" data-toggle="modal" data-target="#modalClaves" ><?= $videojuego->obtenerPortada(); ?></a>
                                <div class="precioTarjeta"><?=$videojuego->obtenerPrecioActual()?>€</div>
                                <div class="titulo"> <?=$videojuego->titulo?> </div>
                            </article>
                    <?php
                        }
                    ?>
                </section>    

                <!-- ============ MODAL ============ -->
                <div class="modal fade" id="modalClaves" tabindex="-1" role="dialog" aria-labelledby="modalClaves" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Tus claves</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <!-- ============ FIN WRAPPER ============ -->

            <!-- ============ FOOTER Y DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/footer.html"); ?>
            <script src="../vista/js/biblioteca.js"></script>
            <?php include_once("../vista/includes/dependenciasBody.html"); ?>
        </body>
    </html>