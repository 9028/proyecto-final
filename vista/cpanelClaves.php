<!DOCTYPE html>
    <html lang="es">
        <head> 
            <!-- ============ DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/dependenciasHeader.html"); ?> 
            <link rel="stylesheet" href="../vista/css/cpanel.css">
            <link rel="stylesheet" href="../vista/css/tabla.css">
        </head>
        <body>
            <!-- ============ NAVBAR ============ -->
            <?php include_once("../vista/navbar.php"); ?>

            <!-- ============ WRAPPER ============ -->
            <main class="wrapper">

                <!-- ============ DROPDOWN ============ -->
                <?php include_once("../vista/includes/dropdownSecciones.html"); ?>

                <!-- ============ TABLA ============ -->
                <div class="table-responsive tablaUsuarios encuadreNoVideojuegos mb-4">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Videojuego</th>
                                <th>Clave</th>
                                <th>Recibida</th>
                                <th>Dueño</th>
                                <th>Plataforma</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($claves as $clave) {
                                  ?>
                                  <tr>
                                    <td><?= Conexion::obtenerVideojuego($clave->id_videojuego)->titulo ?></td>
                                      <td><?= $clave->clave ?></td>
                                      <td><?= Clave::sino($clave->recibida) ?></td>
                                      <td><?= $clave->id_usuario ?></td>
                                      <td><?= Clave::plataformaClave($clave->id_plataforma) ?></td>
                                  </tr>
                                  <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>

                <!-- ============ PESTAÑAS ============ -->
                <section class="desplegable encuadreNoVideojuegos">
                    
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#anadir">Añadir</a>
                        </li>
                    </ul>

                    <div class="tab-content">

                        <!-- ============ AÑADIR CLAVE ============ -->
                        <div class="tab-pane container active" id="anadir">
                            <h4 class="mt-2">Añade una clave</h4>

                            <form method="post" class="anadirClave">
                                <div class="contenedorInputs">
                                    <div class="inputParticular">
                                        <div class="label">Plataforma</div>
                                        <select name="plataformaClave">
                                            <option disabled selected value>Elige una plataforma</option>
                                            <?php
                                           
                                              foreach($plataformas as $plataforma) {
                                                $id = $plataforma['id'];
                                                $nombre = $plataforma['plataforma'];
                                                echo "<option value='$id'>$nombre</option>";
                                                }  
                                            ?>
                                        </select>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Clave</div>
                                        <input type="text" name="claveClave" placeholder="XN12-T13D-60DZ" pattern="^([A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5})|([A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5})|([A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4})|([A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4})$" title="Mezcla de números y letras en mayúscula. PS4: 4-4-4. Nintendo: 4-4-4-4. PC: 5-5-5. XBOX: 5-5-5-5." required>
                                    </div>

                                    <div class="inputParticular">
                                        <div class="label">Videojuego</div>
                                            <select name="videojuegoClave" class="inputVideojuegoClave">
                                            <option disabled selected value>Elige una videojuego</option>
                                            <?php
                                           
                                              foreach($videojuegos as $videojuego) {
                                                $id = $videojuego->id;
                                                $titulo = $videojuego->titulo;
                                                echo "<option value='$id'>$titulo</option>";
                                                }  
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <input name="operacion" type="hidden" value="anadirClave">
                                <button class="btn d-block mt-2 boton" type="submit">Añadir</button>
                                <div class="invalid-feedback"></div>
                            </form>

                        </div>                     
                    </div>
                </section>

            </main>
            <!-- ============ FIN WRAPPER ============ -->

            <!-- ============ FOOTER Y DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/footer.html"); ?>
            <script type="module" src="../vista/js/dataTables/clave.js"></script>
            <?php include_once("../vista/includes/dependenciasBody.html"); ?>
        </body>
    </html>