import { validar, test } from "../validaciones.js";

$(document).ready(function () {
	var tabla = $(".table-responsive table").DataTable();

	// Formularios en los tabs
	let formulariosClave = {
		anadir: document.querySelector(".anadirClave"),
		desactivar: document.querySelector(".desactivarClave"),
	};

	// ============ FORMULARIO AÑADIR CLAVE ============
	formulariosClave.anadir.addEventListener("submit", (e) => {
		e.preventDefault();

		// Obtengo los valores de los input para más tarde actualizarlos en pantalla
		let valores = [];
		valores.push(document.querySelectorAll(".anadirClave select")[1].value);
		valores.push(document.querySelector(".anadirClave input").value);
		valores.push("No");
		valores.push("");
		valores.push(
			parseInt(document.querySelectorAll(".anadirClave select")[0].value)
		);

		// Conversión para mostrar el nombre de la plataforma y no un número
		if (valores[4] == 1) {
			valores[4] = "PS4";
		} else if (valores[4] == 2) {
			valores[4] = "PC";
		} else if (valores[4] == 3) {
			valores[4] = "Nintendo Switch";
		} else if (valores[4] == 4) {
			valores[4] = "XBOX One";
		}

		let options = document.querySelectorAll("option");
		for (let option of options) {
			if (option.value == valores[0]) {
				valores[0] = option.innerHTML;
			}
		}

		let datos = new FormData(formulariosClave.anadir);
		let feedback = document.querySelector(".anadirClave .invalid-feedback");

		// Valido que el formato de la clave sea correcto
		if (
			validar(
				datos.get("claveClave"),
				/^([A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5})|([A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5})|([A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4})|([A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4})$/
			)
		) {
			fetch("operaciones.php", {
				method: "POST",
				body: datos,
			})
				.then((respuesta) => respuesta.json())
				.then((datos) => {
					if (datos == "Clave añadida.") {
						feedback.classList.add("bg-success");
						// Actualizo la tabla con los nuevos valores
						tabla.row.add(valores).draw(false);
					} else {
						feedback.classList.remove("bg-success");
					}
					feedback.style.display = "block";
					feedback.innerHTML = datos;
				})
				.catch((error) => {
					// Control de excepciones
					feedback.classList.remove("bg-success");
					feedback.style.display = "block";
					feedback.innerHTML = `Error procesando los datos en el servidor. (${error})`;
				});
		} else {
			// Algún input no pasó la comprobación de las expresiones regulares
			feedback.classList.remove("bg-success");
			feedback.style.display = "block";
			feedback.innerHTML = "Formato de la clave invalida.";
		}
	});
});
