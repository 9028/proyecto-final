import { validar, test } from "../validaciones.js";

$(document).ready(function () {
	var tabla = $(".table-responsive table").DataTable();

	// Formularios en los tabs
	let formulariosUsuario = {
		anadir: document.querySelector(".anadirUsuario"),
		eliminar: document.querySelector(".eliminarUsuario"),
		modificar: document.querySelector(".modificarUsuario"),
	};

	// ============== AÑADIR ==============
	formulariosUsuario.anadir.addEventListener("submit", (e) => {
		e.preventDefault();
		let inputs = document.querySelectorAll(".anadirUsuario input");

		// Obtengo los valores para luego actualizarlos en la tabla
		let valores = [];
		for (let input of inputs) {
			valores.push(input.value);
		}

		let datos = new FormData(formulariosUsuario.anadir);
		let feedback = document.querySelector(".anadirUsuario .invalid-feedback");

		// Compruebo que los inputs pasan las expresiones regulares
		if (
			validar(datos.get("nombreUsuario"), /^[A-Za-z0-9]{1,8}$/) &&
			validar(
				datos.get("passwordUsuario"),
				/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._/+:;=#?!@$%^&*-]).{8,}$/
			) &&
			validar(datos.get("administradorUsuario"), /^[0-1]$/)
		) {
			fetch("operaciones.php", {
				method: "POST",
				body: datos,
			})
				.then((respuesta) => respuesta.json())
				.then((datos) => {
					if (datos == "Usuario creado.") {
						feedback.classList.add("bg-success");
					} else {
						feedback.classList.remove("bg-success");
					}

					feedback.style.display = "block";
					feedback.innerHTML = datos;

					if (datos == "Usuario creado.") {
						// Cambio los valores recogidos por los que se van a mostrar (0 = No es administrador)
						valores[1] = "Contraseña añadida.";
						if (valores[2] == "0") {
							valores[2] = "No";
						} else {
							valores[2] = "Sí";
						}
						// Actualizo la tabla
						tabla.row.add(valores).draw(false);
					}
				})
				.catch((error) => {
					// Control de excepciones
					feedback.style.display = "block";
					feedback.classList.remove("bg-success");
					feedback.innerHTML = `Error procesando los datos en el servidor. (${error})`;
				});
		} else {
			// Algún input no pasó las expresiones regulares
			feedback.style.display = "block";
			feedback.classList.remove("bg-success");

			if (
				datos.get("nombreUsuario").trim() == "" ||
				datos.get("passwordUsuario").trim() == "" ||
				datos.get("administradorUsuario").trim() == ""
			) {
				feedback.innerHTML = "Rellena todos los campos";
			} else {
				feedback.innerHTML = "Datos incorrectos. Revisa los campos.";
			}
		}
	});

	// ============== MODIFICAR ==============
	formulariosUsuario.modificar.addEventListener("submit", (e) => {
		e.preventDefault();

		let datos = new FormData(formulariosUsuario.modificar);

		let inputs = document.querySelectorAll(".modificarUsuario input");

		// Obtengo el valor de los inputs
		let valores = [];
		for (let input of inputs) {
			valores.push(input.value);
		}

		// Obtengo nombre de usuario y si es administrador
		let usuario = valores[0];
		let administrador;
		if (valores[2] == "0") {
			administrador = "No";
		} else {
			administrador = "Sí";
		}

		// Obtengo la tabla, y busco en ella el número de registro que tengo que editar
		let datosTabla = tabla.data();

		let registro;
		for (let i in datosTabla) {
			if (datosTabla[i][0] == usuario) {
				registro = i;
			}
		}

		let feedback = document.querySelector(
			".modificarUsuario .invalid-feedback"
		);

		// Si se ha encontado el registro y se pasa las expresiones regulares, modifico la tabla y se hace la petición
		if (registro != undefined) {
			if (
				validar(datos.get("nombreUsuario"), /^[A-Za-z0-9]{1,8}$/) &&
				validar(
					datos.get("passwordUsuario"),
					/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._/+:;=#?!@$%^&*-]).{8,}$/
				) &&
				validar(datos.get("administradorUsuario"), /^[0-1]$/)
			) {
				fetch("operaciones.php", {
					method: "POST",
					body: datos,
				})
					.then((respuesta) => respuesta.json())
					.then((datos) => {
						if (datos == "El usuario ha sido modificado.") {
							feedback.classList.add("bg-success");
						} else {
							feedback.classList.remove("bg-success");
						}

						feedback.style.display = "block";
						feedback.innerHTML = datos;

						if (datos == "") {
							// Muestro los nuevos datos en la tabla
							let nuevosDatos = [
								usuario,
								"Contraseña modificada.",
								administrador,
							];
							tabla.row(registro).data(nuevosDatos).draw();
						}
					})
					.catch((error) => {
						// Control de excepciones
						feedback.classList.remove("bg-success");
						feedback.style.display = "block";
						feedback.innerHTML = `Error procesando los datos en el servidor. (${error})`;
					});
			} else {
				// Algún input no pasó las expresiones regulares
				feedback.style.display = "block";

				if (
					datos.get("nombreUsuario").trim() == "" ||
					datos.get("passwordUsuario").trim() == "" ||
					datos.get("administradorUsuario").trim() == ""
				) {
					feedback.classList.remove("bg-success");
					feedback.innerHTML = "Rellena todos los campos";
				} else {
					feedback.classList.remove("bg-success");
					feedback.innerHTML = "Datos incorrectos. Revisa los campos.";
				}
			}
		} else {
			feedback.classList.remove("bg-success");
			feedback.style.display = "block";
			feedback.innerHTML = `No existe el usuario <strong>${usuario}<strong>.`;
		}
	});

	// ============== ELIMINAR ==============
	formulariosUsuario.eliminar.addEventListener("submit", (e) => {
		e.preventDefault();

		let inputs = document.querySelectorAll(".eliminarUsuario input");

		// Obtengo los valores de los input
		let valores = [];
		for (let input of inputs) {
			valores.push(input.value);
		}

		let usuario = valores[0];

		// Busco en la tabla el registro concreto para eliminarlo
		let datosTabla = tabla.data();

		let registro;
		for (let i in datosTabla) {
			if (datosTabla[i][0] == usuario) {
				registro = i;
			}
		}

		let datos = new FormData(formulariosUsuario.eliminar);
		let feedback = document.querySelector(".eliminarUsuario .invalid-feedback");

		// Si se ha encontrado el registro y se pasa la expresión regular, se elimina el usuario
		if (registro != undefined) {
			if (validar(datos.get("nombreUsuario"), /^[A-Za-z0-9]{1,8}$/)) {
				fetch("operaciones.php", {
					method: "POST",
					body: datos,
				})
					.then((respuesta) => respuesta.json())
					.then((datos) => {
						feedback.style.display = "block";
						feedback.innerHTML = datos;

						if (datos == "Usuario eliminado.") {
							feedback.classList.add("bg-success");
							// Actualizo la tabla
							tabla.row(registro).remove().draw();
						}
					})
					.catch((error) => {
						// Control de excepciones
						feedback.classList.remove("bg-success");
						feedback.style.display = "block";
						feedback.innerHTML = `Error procesando los datos en el servidor. (${error})`;
					});
			} else {
				// Algún input no ha pasado las expresiones regulares
				feedback.style.display = "block";
				feedback.classList.remove("bg-success");
				if (datos.get("nombreUsuario").trim() == "") {
					feedback.innerHTML = "Rellena todos los campos";
				} else {
					feedback.innerHTML = "Datos incorrectos. Revisa los campos.";
				}
			}
		} else {
			// El usuario no existe en la tabla, no se puede eliminar
			feedback.classList.remove("bg-success");
			feedback.style.display = "block";
			feedback.innerHTML = `No existe el usuario <strong>${usuario}<strong>.`;
		}
	});
});
