$(document).ready(function () {
	var tabla = $(".table-responsive table").DataTable();

	// Formularios en los tabs
	let formulariosVideojuego = {
		anadir: document.querySelector(".anadirVideojuego"),
		modificarPrecio: document.querySelector(".modificarPrecioVideojuego"),
	};

	// ============== AÑADIR ==============
	formulariosVideojuego.anadir.addEventListener("submit", (e) => {
		e.preventDefault();

		let inputs = document.querySelectorAll(".anadirVideojuego input");

		// Obtengo los valores para luego imprimirlos en la tabla
		let valores = [];
		for (let input of inputs) {
			if (
				input.name != "operacion" &&
				input.name != "pegiVideojuego" &&
				input.name != "fechaVideojuego" &&
				input.name != "trailerVideojuego"
			) {
				valores.push(input.value);
			}
		}

		valores[1] = parseInt(valores[1]);
		valores[2] = parseInt(valores[2]);

		let valoresTabla = [
			0,
			valores[0],
			valores[1] - (valores[1] * valores[2]) / 100 + "€",
			valores[1] + "€",
			valores[2] + "%",
		];

		let datos = new FormData(formulariosVideojuego.anadir);
		let feedback = document.querySelector(
			".anadirVideojuego .invalid-feedback"
		);

		fetch("operaciones.php", {
			method: "POST",
			body: datos,
		})
			.then((respuesta) => respuesta.json())
			.then((datos) => {
				feedback.style.display = "block";
				feedback.innerHTML = datos;

				if (datos == "Videojuego añadido.") {
					// Actualizo la tabla con los valores recogidos
					tabla.row.add(valoresTabla).draw(false);
				}
			})
			.catch((error) => {
				// Control de excepciones
				feedback.style.display = "block";
				feedback.innerHTML = `Error procesando los datos en el servidor. (${error})`;
			});
	});

	// ============== MODIFICAR PRECIO ==============
	formulariosVideojuego.modificarPrecio.addEventListener("submit", (e) => {
		e.preventDefault();
		let feedback = document.querySelector(
			".modificarPrecioVideojuego .invalid-feedback"
		);
		let datos = new FormData(formulariosVideojuego.modificarPrecio);

		let inputs = document.querySelectorAll(".modificarPrecioVideojuego input");

		// Recojo los valores para luego actualizar la tabla
		let flag = false;
		let valores = [];
		for (let input of inputs) {
			// Evito el input de operacion (no tiene valor) y si algún input tiene un valor vacio, no sigo
			if (input.name != "operacion") valores.push(parseInt(input.value));
			if (input.value == "") flag = true;
		}

		if (flag) {
			feedback.classList.remove("bg-success");
			feedback.style.display = "block";
			feedback.innerHTML = "Rellena todos los campos";
		} else {
			// Encuentra la fila en la tabla para modificarla
			let id = valores[0];

			let datosTabla = tabla.data();

			let registro;
			for (let i in datosTabla) {
				if (datosTabla[i][0] == id) {
					registro = i;
				}
			}

			// Si existe el videojuego, hago la petición al servidor y actualizo la tabla
			if (registro != undefined) {
				nuevosValores = [
					datosTabla[registro][0],
					datosTabla[registro][1],
					valores[1] - (valores[1] * valores[2]) / 100 + "€",
					valores[1] + "€",

					valores[2] + "%",
				];
				fetch("operaciones.php", {
					method: "POST",
					body: datos,
				})
					.then((respuesta) => respuesta.json())
					.then((datos) => {
						feedback.style.display = "block";
						feedback.innerHTML = datos;

						if (datos == "Precio modificado.") {
							feedback.classList.add("bg-success");
							// Actualizo la tabla
							tabla.row(registro).data(nuevosValores).draw();
						}
					})
					.catch((error) => {
						// Control de errores
						feedback.classList.add("remove-success");
						feedback.style.display = "block";
						feedback.innerHTML = `Error procesando los datos en el servidor. (${error})`;
					});
			} else {
				// No existe el videojuego en la tabla
				feedback.classList.remove("bg-success");
				feedback.style.display = "block";
				feedback.innerHTML = `No existe el videojuego con ID <strong>${id}<strong>.`;
			}
		}
	});
});
