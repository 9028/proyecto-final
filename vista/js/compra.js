let formularioCompra = document.querySelector(".compra form");

// ============ FORMULARIO COMPRA ============
formularioCompra.addEventListener("submit", (e) => {
	e.preventDefault();

	let datos = new FormData(formularioCompra);
	let feedback = document.querySelector(".compra .invalid-feedback");

	fetch("operaciones.php", {
		method: "POST",
		body: datos,
	})
		.then((respuesta) => respuesta.json())
		.then((datos) => {
			feedback.innerHTML = datos;
			feedback.style.display = "block";
			if (datos == "Videojuego añadido al carrito.") {
				feedback.classList.add("bg-success");
			} else {
				feedback.classList.remove("bg-success");
			}
		})
		.catch((error) => {
			feedback.style.display = "block";
			feedback.innerHTML = `Error procesando los datos en el servidor. (${error})`;
		});
});

// ============ ASEGURARME DE QUE EL USUARIO HA ELEGIDO UNA PLATAFORMA ============
function detectarOption(event) {
	let select = document.querySelector("select");
	let botonComprar = document.querySelector(".botonComprar");
	if (select.value == "") {
		let feedback = document.querySelector(".compra .invalid-feedback");
		feedback.innerHTML = "Selecciona una plataforma.";
		feedback.style.display = "block";
		event.preventDefault();
	} else {
		botonComprar.href = botonComprar.href + "&p=" + select.value;
	}
}
