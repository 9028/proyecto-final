let botonBusqueda = document.querySelector(".search-icon");
let barraBusqueda = document.querySelector(".searchbar");
let menuDesplegable = document.querySelector(".navbar-collapse");

// ============ LISTENER BOTON BÚSQUEDA ============
botonBusqueda.addEventListener("click", () => {
	botonBusqueda.classList.toggle("search-active");
	let busquedaActiva = botonBusqueda.classList.contains("search-active");

	if (busquedaActiva) {
		// Oculto las demás partes del menú dejando solo la búsqueda
		botonBusqueda.innerHTML = '<i class="fas fa-times"></i>';
		document.querySelector(".left").style.display = "none";
		document.querySelector(".right").style.display = "none";
		document.querySelector(".center").style.width = "100%";
		barraBusqueda.style.display = "flex";

		// Si el usuario pulsó buscar con el menú desplegado, lo cierro
		if (menuDesplegable.classList.contains("show"))
			menuDesplegable.classList.toggle("show");
	} else {
		// Se vuelve al aspecto normal del menú
		botonBusqueda.innerHTML = '<i class="fas fa-search"></i>';
		document.querySelector(".left").style.display = "flex";
		document.querySelector(".right").style.display = "flex";
		document.querySelector(".center").style.width = "auto";
		barraBusqueda.style.display = "none";
	}
});

// ============ REAJUSTAR MENÚ ============
// En móvil se verá una lupa (que abrirá una barra de búsqueda) y en tablet y escritorio permanecerá una barra de búsqueda
let resNoMovil = window.matchMedia("(min-width: 500px)");

function reajustarMenu(e) {
	let botonBusqueda = document.querySelector(".search-icon");
	let barraBusqueda = document.querySelector(".searchbar");

	// Tablet o escritorio
	if (e.matches) {
		// Si se redimensiona a tablet/escritorio con una búsqueda activa, se resetea la visualización
		if (botonBusqueda.classList.contains("search-active"))
			botonBusqueda.click();

		// Oculto el botón de búsqueda y muestro la barra
		barraBusqueda.style.display = "flex";
		botonBusqueda.style.display = "none";

		// Móvil
	} else {
		// Justo lo contrario
		barraBusqueda.style.display = "none";
		botonBusqueda.style.display = "flex";
	}
}

// Listener que está pendiente de la resolución actual
resNoMovil.addListener(reajustarMenu);

// ============ LOGIN ============
let formularioLogin = document.querySelector("form");

formularioLogin.addEventListener("submit", (e) => {
	e.preventDefault();
	let datos = new FormData(formularioLogin);
	let usuario = datos.get("usuario");
	let feedback = document.querySelector(".invalid-feedback");

	fetch("login.php", {
		method: "POST",
		body: datos,
	})
		.then((respuesta) => respuesta.json())
		.then((datos) => {
			// console.log(datos);
			if (datos == "Login correcto") {
				window.location.reload();
			} else {
				feedback.style.display = "block";
				feedback.innerHTML = datos;
			}
		})
		.catch((error) => {
			feedback.style.display = "block";
			feedback.innerHTML = `Error procesando los datos en el servidor. (${error})`;
		});
});

// ============ VIDEOJUEGOS EN BARRA DE BÚSQUEDA ============
let inputBusqueda = document.querySelector(".inputBusqueda");
let resultadoBusqueda = document.querySelector(".resultadoBusqueda");

inputBusqueda.addEventListener("input", (e) => {
	let valorActual = inputBusqueda.value.trim();

	// Empiezo a buscar videojuegos a partir de 3 caracteres
	if (valorActual.length < 3) {
		resultadoBusqueda.innerHTML = "";
	} else {
		fetch(`busqueda.php?busqueda=${valorActual}`)
			.then((respuesta) => respuesta.text())
			.then((datos) => {
				if (datos == "[]") {
					resultadoBusqueda.innerHTML = "<div>No hay resultados..</div>";
				} else {
					let juegos = JSON.parse(datos);
					let nuevoContenido = "";

					// Voy acumulando los resultados para hacer un append al final
					for (let juego of juegos) {
						nuevoContenido += `<a href="videojuego.php?id=${juego.id}"><img src="../vista/img/portadas/${juego.id}.jpg" width="50">${juego.titulo}</a>`;
					}

					// Me ahorro actualizar los resultados si la búsqueda es la misma
					if (nuevoContenido != resultadoBusqueda.innerHTML) {
						resultadoBusqueda.innerHTML = nuevoContenido;
					}
				}
			});
	}
});

// Formateo la búsqueda al entrar/salir de la lupa (modo móvil)
botonBusqueda.addEventListener("click", () => {
	resultadoBusqueda.innerHTML = "";
	inputBusqueda.value = "";
});

// Al hacer clic en buscar, que lleve a filtros con esa petición
let botonAccionBusqueda = document.querySelector(".botonAccionBusqueda");

botonAccionBusqueda.addEventListener("click", () => {
	let busquedaActual = document.querySelector(".inputBusqueda").value;

	if (busquedaActual != "") {
		window.location.href = `./filtros.php?busqueda=${busquedaActual}`;
	}
});

// Que al pulsar enter haga lo mismo que un formulario (si hubiese contenido)
barraBusqueda.addEventListener("keypress", function (e) {
	let busquedaActual = document.querySelector(".inputBusqueda").value.trim();
	if (e.key == "Enter" && busquedaActual != "") {
		botonAccionBusqueda.click();
	}
});
