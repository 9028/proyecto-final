let formularioFiltros = document.querySelector(".filtros");
let slice = false;

let escaparate = document.querySelector(".escaparate");
// ============ CARGAR MÁS VIDEOJUEGOS ============
$(document).ready(function () {
	let tarjetas = document.querySelectorAll(".tarjeta");
	// Oculto las portadas
	tarjetas.forEach((tarjeta) => {
		tarjeta.style.display = "none";
		tarjeta.style.transition = "none";
		tarjeta.style.transform = "scale(1)";
	});

	// Muestro de 4 en 4, si ya no hay mas portadas ocultas, desaparece el botón de cargar más
	$(".tarjeta").slice(0, 4).show(500);

	if ($(".tarjeta:hidden").length == 0) {
		$(".loadMore").fadeOut("slow");
	} else {
		$(".loadMore").show();
	}

	$(".loadMore").on("click", function () {
		$(".tarjeta:hidden").slice(0, 4).slideDown();
		if ($(".tarjeta:hidden").length == 0) {
			$(".loadMore").fadeOut("slow");
		}
	});
});

// ============ FORMULARIO FILTROS ============
formularioFiltros.addEventListener("submit", (e) => {
	e.preventDefault();

	let datos = new FormData(formularioFiltros);

	fetch("operaciones.php", {
		method: "POST",
		body: datos,
	})
		.then((respuesta) => respuesta.text())
		.then((datos) => {
			escaparate.innerHTML = datos;

			// Cada vez que hago una consulta, reseteo el proceso de ocultar las tarjetas y mostrarlas de 4 en 4
			$(document).ready(function () {
				$(".loadMore").show();
				let tarjetas = document.querySelectorAll(".tarjeta");
				tarjetas.forEach((tarjeta) => {
					tarjeta.style.display = "none";
					tarjeta.style.transition = "none";
					tarjeta.style.transform = "scale(1)";
				});
				$(".tarjeta").slice(0, 4).show(500);

				if ($(".tarjeta:hidden").length == 0) {
					$(".loadMore").fadeOut("slow");
				} else {
					$(".loadMore").show();
				}
			});
		})
		.catch((error) => {
			// Control de excepciones
			escaparate.innerHTML = `Error procesando los datos en el servidor. (${error})`;
		});
});
