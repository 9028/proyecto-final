import { validar, test } from "./validaciones.js";

// ============ FORMULARIO CAMBIAR CONTRASEÑA ============
let formularioCambioContrasena = document.querySelector(".registro form");

formularioCambioContrasena.addEventListener("submit", (e) => {
	e.preventDefault();
	let datos = new FormData(formularioCambioContrasena);
	let feedback = document.querySelector(".registro .invalid-feedback");

	if (
		validar(
			datos.get("newPassword"),
			/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._+:;=#?!@$%^&*-]).{8,}$/
		) &&
		validar(
			datos.get("oldPassword"),
			/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._+:;=#?!@$%^&*-]).{8,}$/
		)
	) {
		fetch("cambiarContrasena.php", {
			method: "POST",
			body: datos,
		})
			.then((respuesta) => respuesta.json())

			.then((datos) => {
				if (datos == "Contraseña modificada.") {
					feedback.classList.add("bg-success");
				} else {
					feedback.classList.remove("bg-success");
				}
				feedback.style.display = "block";
				feedback.innerHTML = datos;
			})
			.catch((error) => {
				// Control de excepciones
				feedback.style.display = "block";
				feedback.innerHTML = `Error procesando los datos en el servidor. (${error})`;
			});
	} else {
		// Algún input no ha validado las expresiones regulares
		feedback.style.display = "block";

		if (
			datos.get("newPassword").trim() == "" ||
			datos.get("oldPassword").trim() == ""
		) {
			feedback.innerHTML = "Rellena todos los campos.";
		} else {
			feedback.innerHTML = "Datos incorrectos. Revisa las instrucciones.";
		}
	}
});
