// ============ PEDIR LAS CLAVES DE UN VIDEOJUEGO PARTICULAR ============
$(document).ready(function () {
	$(".portadaBiblioteca").click(function () {
		var idVideojuego = $(this).data("id");

		$.ajax({
			url: "biblioteca.php",
			type: "post",
			data: { idVideojuego: idVideojuego },
			success: function (response) {
				// Añado la respuesta del servidor al modal
				$(".modal-body").html(response);
			},
		});
	});
});
