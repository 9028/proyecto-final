import { validar, test } from "./validaciones.js";

// ============ FORMULARIO REGISTRO ============
let formularioRegistro = document.querySelector(".registro form");

formularioRegistro.addEventListener("submit", (e) => {
	e.preventDefault();
	let datos = new FormData(formularioRegistro);
	let feedback = document.querySelector(".registro .invalid-feedback");

	// Antes de llegar al servidor, es validado por expresiones regulares
	if (
		validar(datos.get("usuario"), /^[A-Za-z0-9]{1,8}$/) &&
		validar(
			datos.get("password"),
			/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._+:;=#?!@$%^&*-]).{8,}$/
		) &&
		validar(
			datos.get("password2"),
			/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._+:;=#?!@$%^&*-]).{8,}$/
		)
	) {
		fetch("registro.php", {
			method: "POST",
			body: datos,
		})
			.then((respuesta) => respuesta.json())

			.then((datos) => {
				if (datos == "Usuario creado.") {
					window.location.href = "./articulos.php";
				}
				// Si el usuario no se ha creado correctamente, imprimo el fallo al usuario
				feedback.style.display = "block";
				feedback.innerHTML = datos;
			})
			.catch((error) => {
				// Control de excepciones
				feedback.style.display = "block";
				feedback.innerHTML = `Error procesando los datos en el servidor. (${error})`;
			});
	} else {
		// Algún input no ha validado las expresiones regulares
		feedback.style.display = "block";

		if (
			datos.get("usuario").trim() == "" ||
			datos.get("password").trim() == "" ||
			datos.get("password2").trim() == ""
		) {
			feedback.innerHTML = "Rellena todos los campos.";
		} else {
			feedback.innerHTML = "Datos incorrectos. Revisa las instrucciones.";
		}
	}
});
