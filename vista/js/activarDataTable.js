// $(document).ready(function () {
// 	var tabla = $(".table-responsive table").DataTable();

// 	// USUARIO
// 	let formulariosUsuario = {
// 		anadir: document.querySelector(".anadirUsuario"),
// 		eliminar: document.querySelector(".eliminarUsuario"),
// 		modificar: document.querySelector(".modificarUsuario"),
// 	};

// 	formulariosUsuario.anadir.addEventListener("submit", (e) => {
// 		e.preventDefault();
// 		let inputs = document.querySelectorAll(".anadirUsuario input");
// 		let valores = [];
// 		for (let input of inputs) {
// 			valores.push(input.value);
// 		}

// 		let datos = new FormData(formulariosUsuario.anadir);
// 		let feedback = document.querySelector(".anadirUsuario .invalid-feedback");

// 		fetch("usuario.php", {
// 			method: "POST",
// 			body: datos,
// 		})
// 			.then((respuesta) => respuesta.json())
// 			.then((datos) => {
// 				feedback.style.display = "block";
// 				feedback.innerHTML = datos;

// 				if (datos == "Usuario creado.") {
// 					valores[1] = "Contraseña añadida.";
// 					tabla.row.add(valores).draw(false);
// 				}
// 			});
// 	});

// 	formulariosUsuario.modificar.addEventListener("submit", (e) => {
// 		e.preventDefault();

// 		let datos = new FormData(formulariosUsuario.modificar);

// 		let inputs = document.querySelectorAll(".modificarUsuario input");
// 		let valores = [];
// 		for (let input of inputs) {
// 			valores.push(input.value);
// 		}

// 		let usuario = valores[0];
// 		let administrador;
// 		if (valores[2] == "0") {
// 			administrador = "No";
// 		} else {
// 			administrador = "Sí";
// 		}

// 		let datosTabla = tabla.data();

// 		let registro;
// 		for (let i in datosTabla) {
// 			if (datosTabla[i][0] == usuario) {
// 				registro = i;
// 			}
// 		}

// 		let feedback = document.querySelector(
// 			".modificarUsuario .invalid-feedback"
// 		);

// 		if (registro != undefined) {
// 			fetch("usuario.php", {
// 				method: "POST",
// 				body: datos,
// 			})
// 				.then((respuesta) => respuesta.json())
// 				.then((datos) => {
// 					console.log(datos);
// 					feedback.style.display = "block";
// 					feedback.innerHTML = datos;

// 					if (datos == "El usuario ha sido modificado.") {
// 						nuevosDatos = [usuario, "Contraseña modificada.", administrador];
// 						tabla.row(registro).data(nuevosDatos).draw();
// 					}
// 				});
// 		} else {
// 			feedback.style.display = "block";
// 			feedback.innerHTML = `No existe el usuario <strong>${usuario}<strong>.`;
// 		}
// 	});

// 	formulariosUsuario.eliminar.addEventListener("submit", (e) => {
// 		e.preventDefault();

// 		let inputs = document.querySelectorAll(".eliminarUsuario input");
// 		let valores = [];
// 		for (let input of inputs) {
// 			valores.push(input.value);
// 		}

// 		let usuario = valores[0];

// 		let datosTabla = tabla.data();

// 		let registro;
// 		for (let i in datosTabla) {
// 			if (datosTabla[i][0] == usuario) {
// 				registro = i;
// 			}
// 		}

// 		let datos = new FormData(formulariosUsuario.eliminar);
// 		let feedback = document.querySelector(".eliminarUsuario .invalid-feedback");

// 		fetch("usuario.php", {
// 			method: "POST",
// 			body: datos,
// 		})
// 			.then((respuesta) => respuesta.json())
// 			.then((datos) => {
// 				console.log(datos);
// 				feedback.style.display = "block";
// 				feedback.innerHTML = datos;

// 				if (datos == "Usuario eliminado.") {
// 					tabla.row(registro).remove().draw();
// 				}
// 			});
// 	});
// });
