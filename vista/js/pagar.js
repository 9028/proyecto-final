import { validar, test } from "./validaciones.js";

let formularios = {
	tarjeta: document.querySelector(".formularioTarjeta"),
	paypal: document.querySelector(".formularioPaypal"),
};

let opciones = {
	tarjeta: document.querySelector(".metodosPago > .tarjeta"),
	paypal: document.querySelector(".metodosPago > .paypal"),
};

// Por defecto, la opción seleccionada será el pago con tarjeta
opciones.tarjeta.classList.add("opcionSeleccionada");

// ============ LISTENERS OPCIÓN DE PAGO ============
opciones.tarjeta.addEventListener("click", () => {
	formularios.paypal.style.display = "none";
	opciones.paypal.classList.remove("opcionSeleccionada");

	formularios.tarjeta.style.display = "flex";
	opciones.tarjeta.classList.add("opcionSeleccionada");
});

opciones.paypal.addEventListener("click", () => {
	formularios.tarjeta.style.display = "none";
	opciones.tarjeta.classList.remove("opcionSeleccionada");

	formularios.paypal.style.display = "flex";
	opciones.paypal.classList.add("opcionSeleccionada");
});

// ============ FORMULARIO TARJETA ============
formularios.tarjeta.addEventListener("submit", (e) => {
	e.preventDefault();

	let datos = new FormData(formularios.tarjeta);
	let feedback = document.querySelector(".fPago");

	// Antes de llegar al servidor, es validado por expresiones regulares
	if (
		validar(
			datos.get("numeroTarjeta"),
			/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14})$/
		) &&
		validar(datos.get("fechaExpiracionMes"), /^(0?[1-9]|1[012])$/) &&
		validar(datos.get("fechaExpiracionAnio"), /^([0-9]{4})$/) &&
		validar(datos.get("cv"), /^([0-9]{3})$/) &&
		validar(datos.get("nombre"), /^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$/) &&
		validar(datos.get("apellidos"), /^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$/) &&
		validar(datos.get("direccionFacturacion"), /^(.+)$/) &&
		validar(datos.get("localidad"), /^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$/) &&
		validar(datos.get("pais"), /^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$/)
	) {
		fetch("operaciones.php", {
			method: "POST",
			body: datos,
		})
			.then((respuesta) => respuesta.json())
			.then((datos) => {
				if (datos == "Compra realizada.") {
					// Oculto todo y muestro que la compra se ha realizado con éxito
					let elementosPagina = document.querySelectorAll(".wrapper > *");
					elementosPagina.forEach((element) => {
						element.style.display = "none";
					});

					let compraRealizada = document.querySelector(".compraRealizada");
					compraRealizada.style.display = "block";
				} else {
					// Muestro el error al usuario
					feedback.innerHTML = datos;
					feedback.style.display = "block";
				}
			})
			.catch((error) => {
				// Control de excepciones
				feedback.style.display = "block";
				feedback.innerHTML = `Error procesando los datos en el servidor. (${error})`;
			});
	} else {
		// Algún input no ha validado las expresiones regulares
		feedback.style.display = "block";

		if (
			datos.get("numeroTarjeta").trim() == "" ||
			datos.get("fechaExpiracionMes").trim() == "" ||
			datos.get("fechaExpiracionAnio").trim() == "" ||
			datos.get("cv").trim() == "" ||
			datos.get("nombre").trim() == "" ||
			datos.get("apellidos").trim() == "" ||
			datos.get("direccionFacturacion").trim() == "" ||
			datos.get("localidad").trim() == "" ||
			datos.get("pais").trim() == "" ||
			datos.get("cp").trim() == ""
		) {
			feedback.innerHTML = "Rellena todos los campos";
		} else {
			feedback.innerHTML = "Datos incorrectos. Revisa los campos.";
		}
	}
});

// ============ FORMULARIO PAYPAL ============
formularios.paypal.addEventListener("submit", (e) => {
	e.preventDefault();

	let datos = new FormData(formularios.paypal);
	let feedback = document.querySelector(".fPago");

	// Antes de llegar al servidor, es validado por expresiones regulares
	if (
		validar(
			datos.get("password"),
			/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._/+:;=#?!@$%^&*-]).{8,}$/
		) &&
		validar(datos.get("nombre"), /^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$/) &&
		validar(datos.get("apellidos"), /^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$/) &&
		validar(datos.get("direccionFacturacion"), /^(.+)$/) &&
		validar(datos.get("localidad"), /^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$/) &&
		validar(datos.get("pais"), /^([A-Za-zñÑàÀèÈìÌòÒùÙü ,]+)$/)
	) {
		fetch("operaciones.php", {
			method: "POST",
			body: datos,
		})
			.then((respuesta) => respuesta.json())
			.then((datos) => {
				if (datos == "Compra realizada.") {
					// Oculto todo y muestro que la compra se ha realizado con éxito
					let elementosPagina = document.querySelectorAll(".wrapper > *");
					elementosPagina.forEach((element) => {
						element.style.display = "none";
					});

					let compraRealizada = document.querySelector(".compraRealizada");
					compraRealizada.style.display = "block";
				} else {
					// Muestro el error al usuario
					feedback.innerHTML = datos;
					feedback.style.display = "block";
				}
			})
			.catch((error) => {
				// Control de excepciones
				feedback.style.display = "block";
				feedback.innerHTML = `Error procesando los datos en el servidor. (${error})`;
			});
	} else {
		// Algún input no ha validado las expresiones regulares
		feedback.style.display = "block";

		if (
			datos.get("email").trim() == "" ||
			datos.get("password").trim() == "" ||
			datos.get("nombre").trim() == "" ||
			datos.get("apellidos").trim() == "" ||
			datos.get("direccionFacturacion").trim() == "" ||
			datos.get("localidad").trim() == "" ||
			datos.get("pais").trim() == "" ||
			datos.get("cp").trim() == ""
		) {
			feedback.innerHTML = "Rellena todos los campos";
		} else {
			feedback.innerHTML = "Datos incorrectos. Revisa los campos.";
		}
	}
});
