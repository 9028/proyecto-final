<!-- VISTA LOGIN -->
<!DOCTYPE html>
    <html lang="es">
        <head> 
            <!-- ============ DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/dependenciasHeader.html"); ?>
            <link rel="stylesheet" href="../vista/css/registro.css">
        </head>
        <body>
            <!-- ============ NAVBAR ============ -->
            <?php include_once("../vista/navbar.php"); ?>

            <!-- ============ WRAPPER ============ -->
            <main class="content">
                <section class="registro encuadre">
                    <h1>Registro</h1>
                    <form method="POST">
                        <div class="form-group">
                            <label for="usuario">Usuario:</label>
                            <input type="text" name="usuario" class="form-control" id="usuario" aria-describedby="usuario" placeholder="JohnDoe, john1..." pattern="^[A-Za-z0-9]{1,8}$" title="Hasta 8 caracteres. Pueden usarse letras y números." required>
                        </div>
                        <div class="form-group">
                            <label for="password">Contraseña:</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="_Example1" pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._/+:;=#?!@$%^&*-]).{8,}$" title="8 caracteres como mínimo, siendo obligatorio que contenga al menos un: caracter especial, número y letra mayúscula." required>
                        </div>
                        <div class="form-group">
                            <label for="password2">Repite contraseña:</label>
                            <input type="password" name="password2" class="form-control" id="password2" placeholder="_Example1" pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._/+:;=#?!@$%^&*-]).{8,}$" title="8 caracteres como mínimo, siendo obligatorio que contenga al menos un: caracter especial, número y letra mayúscula." required>
                        </div>
                        <button type="submit" class="btn btn-primary boton">Enviar</button>
                    </form>
                    <div class="invalid-feedback"></div>
                </section>
                <section class="instrucciones">
                <h2>Instrucciones</h2>
                    <ul>
                        <li><strong>Usuario</strong>: hasta 8 caracteres. Pueden usarse letras y números.</li>
                        <li><strong>Contraseña</strong>: 8 caracteres como mínimo, siendo obligatorio que contenga al menos un: caracter especial, número y letra mayúscula.</li>
                    </ul>
                </section>
            </main>
            <!-- ============ FIN WRAPPER ============ -->

            <!-- ============ FOOTER Y DEPENDENCIAS ============ -->
            <?php include_once("../vista/includes/footer.html"); ?>
            <script type="module" src="../vista/js/registro.js"></script>
            <?php include_once("../vista/includes/dependenciasBody.html"); ?>
        </body>
    </html>