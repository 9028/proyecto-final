<!DOCTYPE html>
    <html lang="es">
        <head> 
            <?php include_once("../vista/includes/dependenciasHeader.html"); ?> 
            <link rel="stylesheet" href="../vista/css/configuracion.css">
        </head>
        <body>
            <?php include_once("../vista/navbar.php"); ?>
            <div class="wrapper">
                <h1>Tu configuración</h1>
                <ul class="encuadre pl-5">
                    <li><a href="./cambiarContrasena.php" class="enlace">Cambiar contraseña</a></li>
                </ul>
            </div>
            <?php include_once("../vista/includes/footer.html"); ?>
            <?php include_once("../vista/includes/dependenciasBody.html"); ?>  
        </body>
    </html>