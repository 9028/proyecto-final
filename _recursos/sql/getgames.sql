DROP DATABASE IF EXISTS getgames;
CREATE DATABASE getgames DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
CREATE USER IF NOT EXISTS getgames@localhost IDENTIFIED BY 'getgames2021';
GRANT ALL ON getgames.* TO 'getgames'@'localhost';

use getgames;

CREATE TABLE USUARIO (
  usuario varchar(10) NOT NULL,
  password varchar(200) NOT NULL,
  administrador boolean NULL,
  PRIMARY KEY(usuario)
);

CREATE TABLE VIDEOJUEGO (
  id int NOT NULL auto_increment,
  titulo varchar(40) NOT NULL,
  descripcion varchar(5000) NOT NULL,
  precio int(3) NOT NULL,
  rebaja int(3) NULL,
  edad_recomendada int(2) NOT NULL,
  fecha_lanzamiento date NOT NULL,
  trailer varchar(1000) NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE PLATAFORMA(
	id int NOT NULL auto_increment,
    plataforma varchar(15) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE CLAVE(
	clave varchar(24) NOT NULL,
    recibida boolean NOT NULL,
    id_usuario varchar(10) NULL,
    id_plataforma int(5) NOT NULL,
    id_videojuego int(5) NOT NULL,
    PRIMARY KEY(clave),
    FOREIGN KEY (id_usuario) REFERENCES USUARIO(usuario),
    FOREIGN KEY (id_videojuego) REFERENCES VIDEOJUEGO(id),
    FOREIGN KEY (id_plataforma) REFERENCES PLATAFORMA(id)
);



CREATE TABLE GENERO(
	id int NOT NULL auto_increment,
    genero varchar(12) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE VIDEOJUEGO_GENERO(
	id_videojuego int NOT NULL,
    id_genero int NOT NULL,
    PRIMARY KEY (id_videojuego, id_genero),
    FOREIGN KEY (id_videojuego) REFERENCES VIDEOJUEGO(id),
    FOREIGN KEY (id_genero) REFERENCES GENERO(id)
);

CREATE TABLE IDIOMA(
	id int NOT NULL auto_increment,
    idioma varchar(10) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE VIDEOJUEGO_IDIOMA(
	id_videojuego int NOT NULL,
    id_idioma int NOT NULL,
    PRIMARY KEY (id_videojuego, id_idioma),
    FOREIGN KEY (id_videojuego) REFERENCES VIDEOJUEGO(id),
    FOREIGN KEY (id_idioma) REFERENCES IDIOMA(id)
);

/* USUARIOS */  
INSERT INTO USUARIO VALUES
("admin","$2y$10$/C7qnFU74tDlJ/IKyeoFAelIQM1OZbHg2T6J4nmmrHWzUTZy9uWSS", true),
("adrian","$2y$10$/C7qnFU74tDlJ/IKyeoFAelIQM1OZbHg2T6J4nmmrHWzUTZy9uWSS", false),
("admindaw","$2y$10$0YGUwTQYa8oBje2NfE8E6u2alFCHvLM6.NjNMpB.vkBegDcigOwF6", true),
("userdaw","$2y$10$0YGUwTQYa8oBje2NfE8E6u2alFCHvLM6.NjNMpB.vkBegDcigOwF6", false);

/* VIDEOJUEGOS */
INSERT INTO VIDEOJUEGO (titulo, descripcion, precio, rebaja, edad_recomendada, fecha_lanzamiento, trailer) VALUES 
/* MULTIPLATAFORMA */
("The Witcher 3: Wild Hunt",
    'The Witcher 3 es la tercera entrega de la saga The Witcher desarrollada por CD Projekt para PS4, Xbox One y Pc. Se trata de un videojuego que mezcla elementos de aventura, acción y rol en un mundo abierto épico basado en la fantasía. El jugador controlará una vez más a Geralt de Rivia, el afamado cazador de monstruos, (también conocido como el Lobo Blanco) y se enfrentará a un diversificadísimo bestiario y a unos peligros de unas dimensiones nunca vistas hasta el momento en la serie, mientras recorre los reinos del Norte. Durante su aventura, tendrá que hacer uso de un gran arsenal de armas, armaduras y todo tipo de magias para enfrentarse al que hasta ahora ha sido su mayor desafío, la cacería salvaje. Este videojuego ha sido galardonado como el mejor juego del año 2015 tanto por críticos especializados como por galas de premios como los “Golden Joystick Awards”, “Game Developers Choice Awards” y “The Game Awards”. Además cuenta con 2 DLC o Expansiones: Blood and wine, y Hearts of Stone.',
    30,
    50,
    18,
    "2015-05-19",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/6qpf3YWbCcE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Dragon Ball FighterZ",
    'Dragon Ball FighterZ es un juego de luchas 2.5D basado en la famosa serie de manga japonesa del mismo nombre. El juego ha recibido excelentes críticas y es considerado por muchos como el mejor videojuego de lucha de Dragon Ball de la historia.',
    60,
    75,
    12,
    "2018-01-26",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/oBI0MU73nlc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("DOOM",
    'Doom para PC es el cuarto juego de la serie del mismo nombre y un regreso a sus orígenes, al ser una nueva versión del aclamado juego que en cierto modo podría ser considerado un tatarabuelo de los juegos modernos, ya que su primera edición fue lanzada para MS-DOS en 1993, poco después de la que la informática personal se estandarizara ampliamente. En aquella época no se podía hacer doble clic para ingresar al juego: en cambio, los comandos necesarios para echar andar el juego debían ser tecleados en DOS, ¡lo que demuestra cuánto tiempo ha pasado desde entonces!',
    20,
    50,
    18,
    "2016-05-13",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/RO90omga8D4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("The Elder Scrolls V: Skyrim",
    'Skyrim: es una versión remasterizada del aclamado RPG de acción desarrollado por Bethesda y representa la quinta edición del juego de fantasía. Los juegos de Skyrim (de la serie Elder Scroll) han recibido múltiples premios, y son conocidos por su jugabilidad de mundo abierto, mundos cuidadosamente detallados y libertad para que el jugador dirija la acción y, hasta cierto punto, la historia.',
    40,
    20,
    16,
    "2016-10-28",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/JSRtYpNRoN0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Aragami",
    'Eres Aragami, un espíritu vengativo con el poder de controlar las sombras. Has sido invocado por Yamiko, una chica aprisionada en la ciudad fortificada de Kyuryu. Embárcate en un oscuro viaje lleno de sangre y secretos para descubrir la verdad sobre los aragami. Infíltrate en la ciudad ocupada de Kyuryu con tus poderes sobrenaturales y combate la Luz con la Sombra. Descubre una historia de dos almas gemelas ligadas entre ellas por un destino que sobrepasa el tiempo y los recuerdos.',
    20,
    50,
    16,
    "2016-10-04",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/sgXt8cZD4-4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Hover: Revolt Of Gamers",
    'Hover: La Revolución de los Gamers es un electrizante juego de parkour en un mundo abierto y futurista para uno o varios jugadores.|/b] El juego se desarrolla en ECP17, una ciudad de alta tecnología también conocida como Ciudad Hover, y que se ubica en un planeta lejano. El Gran Administrador ha cortado toda comunicación con la Unión Galáctica y ha establecido una opresiva dictadura. Divertirse es ahora ilegal y se ha prohibido cualquier tipo de entretenimiento. Ahora estás a cargo de un equipo de jóvenes rebeldes, los Gamers, que luchan contra la nueva ley antiocio que asfixia la ciudad. Los Gamers toman las calles equipados con sus trajes de alta tecnología que les permiten alcanzar velocidades y alturas increíbles para destruir la propaganda tiránica, ayudar a los ciudadanos y buscar la forma de llegar a la Estación Orbital. De esta manera podrán advertir a la Unión Galáctica de la situación y poner fin a la dictadura. Hover: La Revolución de los Gamers es una experiencia para uno o varios jugadores. En cualquier momento puedes pasar de jugar solo a jugar en línea y unirte a tus amigos o a otros jugadores del mundo para progresar en la aventura y cooperar o jugar contra ellos. No importa dónde estés y lo que estés haciendo, siempre podrás conectarte. Hover: La Revolución de los Gamers es, además, un juego centrado en su comunidad y ofrece a los jugadores las herramientas para crear misiones o minijuegos.',
    20,
    50,
    6,
    "2015-04-06",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/y3ofsA7VqmA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("FIFA 21",
    'FIFA 2021 para PC es el increíble juego número 28 de esta serie. Los desarrolladores del juego han tenido años para alcanzar la perfección y, como resultado, hay poco en lo que se ha interferido para arruinar el clásico tan querido. Como siempre, es la licencia lo que atrae a los compradores, y esta edición no es diferente con la asombrosa cantidad de 17,000 jugadores de 700 clubes que juegan en 30 ligas para elegir, intercambiar y transferir. Esto significa que puedes jugar los partidos de una temporada completa sin repetir a un jugador, incluso en el mismo equipo, si así lo deseas.',
    60,
    50,
    18,
    "2020-10-09",
    '<iframe width="560" height="315" src="https://www.youtube.com/embed/tuLAn9adQpI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Mortal Kombat 11",
    'Mortal Kombat 11 para PC es un juego de lucha 2.5D que celebra –como su nombre lo sugiere- la impresionante cifra de veintisiete años de historia de la serie Mortal Kombat. Desde luego, esto ofrece a los jugadores una historia rica en trasfondo y personajes (sobre todo a los nuevos jugadores), pero también significa que los desarrolladores cuentan con una increíble cantidad de experiencia en la creación de juegos de lucha convincentes e inmersivos. ¡Y esa experiencia queda de manifiesto en la calidad de esta edición! Al hablar de 2D nos referimos a imágenes planas, que tienen longitud y amplitud. El 3D, por su parte, nos ofrece imágenes con profundidad, ya sea mediante la utilización de escenarios de la vida real o con profundidad simulada para videojuegos y películas. El término 2.5D, por su parte, es utilizado cuando la ilusión óptica del 3d se nos ofrece sobre una plataforma en 2D, o cuando un juego ha sido diseñado en su mayoría en 2D, pero cuenta con algunas características completamente en 3D. Este juego se enmarca dentro de la clasificación 2.5D.',
    60,
    50,
    18,
    "2019-04-23",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/z7f4paq1Fvg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Grand Theft Auto V",
    'Grand Theft Auto V para PC es un juego de acción y aventuras, el quinto de la serie GTA. Como ya es tradición en los juegos de esta serie, se obtienen puntos mediante la comisión de delitos. El juego alterna entre la narrativa visual y la jugabilidad en tercera y en primera persona. El jugador toma el papel de tres criminales tratando de huir de una determinada agencia del gobierno y cometiendo asaltos. No se elige un solo personaje para jugar, la narrativa del juego salta de un personaje a otro.',
    30,
    50,
    18,
    "2015-04-14",
    '<iframe width="853" height="480" src="https://www.youtube.com/embed/QkkoHAzjnUs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Hollow Knight",
    'Bajo la decadente ciudad de Dirtmouth subyace Hallownest, una civilización en ruinas que se ha visto reducida a una mera sombra de su otrora glorioso pasado. Tanto exploradores como expoliadores se aventuran bajo la superficie atraídos por sus tesoros, reliquias e incluso en pos de respuestas a antiguos secretos, pero entre este cúmulo de saqueadores también campan a sus anchas criaturas nauseabundas y monstruos grotescos, víctima de un veneno empalagoso que penetra el subsuelo. Si estás dispuesto a descender hasta la más inhóspita de las penumbras, prepárate para defenderte con uñas y dientes contra hordas de bichos fuera de sí que te pondrán los pelos de punta. Evita los ataques de los enemigos moviéndote con astucia, esquiva los proyectiles dirigidos hacia ti, domina la técnica del salto de pared para llegar a sitios más altos y, como colofón, lanza tu ofensiva con tu espada y las potentes habilidades que atesoras.',
    15,
    50,
    6,
    "2018-06-12",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/UAO2urG23S4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Stardew Valley",
    'Acabas de heredar la vieja granja que tu abuelo tenía en Stardew Valley, así que decides comenzar una nueva vida con la ayuda de unas cuantas herramientas de segunda mano y un puñado de monedas. ¿Podrás aprender a vivir de la tierra y convertir ese terreno tan descuidado en un hogar acogedor? No será fácil. Desde que Joja Corporation se instaló en el pueblo, sus habitantes han ido olvidando sus antiguas tradiciones. El centro cívico, uno de los núcleos de actividad más animados del pueblo antiguamente, ahora no es más que un montón de escombros. Sin embargo, el valle está lleno de posibilidades.',
    14,
    20,
    12,
    "2017-10-05",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/FjJx6u_5RdU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Dragon Ball Xenoverse 2",
    'Desarrollado para aprovechar al máximo la potencia de la actual generación de consolas y PCs, DRAGON BALL XENOVERSE 2 llega como continuación del exitoso DRAGON BALL XENOVERSE, con gráficos mejorados que transportarán a los jugadores al mayor y más detallado mundo Dragon Ball que existe. DRAGON BALL XENOVERSE 2 cuenta con una nueva ciudad y más opciones de personalización de personajes, multitud de nuevas características y mejoras especiales. ¡UNÍOS Y VOLAD A TRAVES DEL TIEMPO PARA PROTEGER LA HISTORIA DE DRAGON BALL!',
    60,
    50,
    12,
    "2017-08-22",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/V8b6bFHdJ_Q" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Jump Force",
    'Por primera vez, los héroes manga más famosos van a parar a un campo de batalla completamente nuevo: nuestro mundo. Uniéndose para luchar contra la amenaza más peligrosa, la Jump Force tendrá sobre sus hombros el destino de toda la humanidad. Para celebrar el 50.º aniversario de la famosa revista Shūkan Shōnen Jump, Jump Force también aprovecha al máximo las últimas tecnologías para dar vida a sus personajes con un diseño realista jamás visto hasta ahora.',
    50,
    50,
    12,
    "2020-08-28",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/7U50iYupwBE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Dead Cells",
    '¿Creciste con los juegos de tipo roguelike, fuiste testigo de los juegos roguelite y hasta de los roguelite-lite? Pues te presentamos nuestro juego de estilo roguevania, el hijo ilegítimo de un roguelite moderno (Rogue Legacy, Binding of Isaac, Enter the Gungeon, Spelunky, etc.) y un metroidvania de la vieja escuela (Castlevania: SotN y similares).',
    50,
    50,
    12,
    "2020-08-28",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/RvGaSPTcTxc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("NBA 2K20",
    'BIENVENIDO AL MAÑANA NBA 2K se ha convertido en mucho más que un simulador de baloncesto. 2K redefine de nuevo los juegos deportivos con NBA 2K20, el título con los mejores gráficos y jugabilidad, innovadores modos de juego y una personalización y manejo de los jugadores incomparables. Además, con su cautivador barrio en mundo abierto, NBA 2K20 es la plataforma donde jugadores y deportistas se darán cita para dar forma al futuro del baloncesto. Lleva tu talento al siguiente nivel con la experiencia de jugador más realista hasta la fecha, con un motor de movimientos mejorado con estilos personalizados, controles de tiro avanzados, un nuevo sistema de dribling, colisiones sin balón perfeccionadas y un nuevo sistema defensivo de predicción y reacción. Mi CARRERA El visionario Sheldon Candis dirige la experiencia cinemática de Mi CARRERA más sorprendente a nivel visual hasta la fecha. Un elenco de caras conocidas que incluye a Idris Elba, Rosario Dawson y estrellas de la NBA del pasado y del presente para dar vida a esta aventura de una manera completamente nueva y absorbente. EL BARRIO DEL MAÑANA Disfruta de un barrio más vibrante y activo. Accede a más eventos de competición 2K, desbloquea animaciones con el nuevo Stick Demostración, juega una ronda en los 9 hoyos del campo de disc golf y consigue equipo más exclusivo que nunca. LÚCETE JUGANDO Y GANA REPUTACIÓN El Parque sigue siendo el escenario principal en el que los jugadores pulen sus habilidades y compiten por ser el mejor. Y con el regreso de la reputación de Parque, todo el mundo sabrá quién lo vale y quién necesita chupar banquillo. Desbloquea artículos exclusivos al ganar reputación y úsalos con tus configuraciones de Mi JUGADOR. Hay cientos de premios disponibles con el nuevo y mejorado sistema de reputación. CREADOR DE Mi JUGADOR Más control que nunca. El nuevo creador de Mi JUGADOR te permite tomar decisiones sobre todos los aspectos que afectan al potencial de tu jugador. Puedes elegir hasta tu propio Dominio. Con más de 100 arquetipos y 50 insignias nuevas, las combinaciones son prácticamente ilimitadas. BIENVENIDO A LA WNBA Por primera vez, los 12 equipos de la WNBA y más de 140 jugadoras están en el juego y listas para darlo todo en los modos Jugar ya y Temporada. Incluye animaciones, estilos de juego y gráficos diseñados en exclusiva para los partidos femeninos. Mi EQUIPO Colección de cartas de fantasía de NBA 2K. Domina Mi EQUIPO con objetivos diarios, cartas para subir de nivel, un modo Triple amenaza rediseñado, eventos de tiempo limitado y más premios. Disfruta de una experiencia de usuario simplificada que complacerá a jugadores veteranos y novatos, y mantente conectado con la comunidad con códigos de vestuario, marcadores, consejos de los desarrolladores, el equipo de la semana y mucho más. En colaboración con Steve Stoute y United Masters, la banda sonora de este año ofrece una gran variedad de canciones de los artistas más populares y futuros talentos de todo el mundo. EQUIPOS LEGENDARIOS Juega con más de 10 nuevos equipos legendarios del pasado, como los Portland Trail Blazers de 2009-2010, los Cleveland Cavaliers de 2015-2016, los San Antonio Spurs de 2013-2014, los Phoenix Suns de 2002-2003 y equipos de todas las décadas de la historia de la NBA. Más de 100 equipos legendarios en total entre los que elegir. UNA PRESENTACIÓN DE OTRO NIVEL Una presentación del juego dinámica y de calidad, acompañada de los mayores talentos de la historia de los videojuegos deportivos y de la mano de Kevin Harlan, Ernie Johnson y muchos más. Una experiencia de sonido sin igual, con más de 60 000 líneas de diálogo nuevas, nuevos espectáculos e introducciones, entrevistas de Mi JUGADOR, cobertura de récords e hitos y más de 2000 sonidos y reacciones del público en el pabellón. Mi GM/Mi LIGA ¿Podrás crear la próxima dinastía? Asume el control total de la franquicia y crea un equipo de campeones desde cero. Con árboles de habilidades, un sistema de relaciones renovado, simulaciones personalizadas y mejoras en el sistema de seguimiento, entre otras novedades. 2KTV – TEMPORADA 6 NBA 2KTV, presentada por Alexis Morgan y Chris Manning, regresa en esta nueva temporada para aunar todo el contenido de NBA 2K. ¡Visitas de los miembros de la comunidad 2K, entrevistas en exclusiva con las estrellas de la NBA y la WNBA, las últimas noticias de 2K20, consejos y análisis directamente de la mano de los desarrolladores y las Mejores jugadas semanales!',
    60,
    50,
    6,
    "2019-09-05",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/FQ7WBnSvjIo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Shovel Knight: Treasure Trove",
    '¡Shovel Knight: Treasure Trove es la edición completa de Shovel Knight, un arrasador clásico de acción-aventura con una jugabilidad apasionante, personajes memorables y estética retro de 8-bits! Corre, salta y lucha encarnando a Shovel Knight, Caballero de la Pala, en busca de su amada perdida. Acaba con los infames miembros de la Orden Sin Cuartel y enfréntate a su amenazante líder, la Hechicera. ¡Pero eso no es todo! Shovel Knight: Treasure Trove también incluye 4 juegos completos adicionales! Toma el control de Plague Knight, Specter Knight y King Knight en sus propias aventuras y lucha contra el resto en un Showdown para cuatro jugadores en modo local. ¡Todos juntos, forman una arrolladora y enorme saga! Llévatelo todo con Shovel Knight: Treasure Trove. Juega con un amigo en la campaña cooperativa de Shovel of Hope, pon tu temple a prueba en completos modos desafío, combate contra otros jugadores en Showdown y dale la vuelta al juego con el modo Cambio de Cuerpo. ¡Defiende las virtudes de la Palería, consigue reliquias y tesoros y descubre el verdadero significado de la justicia de la pala! Shovel Knight: Shovel of Hope – ¡Afila tu pala y empieza a cavar en la aventura que lo empezó todo! Salta, combate y encuentra tesoros mientras tratas de derrotar a la Orden Sin Cuartel y a su vil líder, la Hechicera. Shovel Knight: Plague of Shadows – ¡Prepara tus pociones para convertirte en el maníaco alquimista Plague Knight en su aventura para conseguir la poción definitiva! Combina componentes explosivos para crear bombas personalizadas, encuentra nuevos objetos, áreas y jefes e incluso un burbujeante romance. Shovel Knight: Specter of Torment – ¡Enarbola tu guadaña en esta precuela de Shovel Knight! Toma el control de Specter Knight, domina un nuevo arsenal, derrota a tus enemigos y súbete por las paredes en niveles rediseñados de arriba abajo. ¡Conviértete en el segador, descubre tu melancólico pasado y recluta a la Orden Sin Cuartel! Shovel Knight: King of Cards – ¡Ponte las doradas botas de King Knight en el mayor y más noble juego de Shovel Knight! ¡Salta, carga con hombro, reúne súbditos y crea tu propia estrategia en esta precuela para convertirte en el Rey de Naipes! Shovel Knight Showdown – Vence en combates locales de hasta 4 jugadores y arrambla con gemas o toma el control de tu personaje favorito para adentrarte en el Modo Historia. Domina todos los movimientos, explora una vasta cantidad de objetos y escenarios y descubre nuevas revelaciones en este juego de lucha y plataformas.',
    40,
    50,
    6,
    "2014-06-26",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/bhG02JG7Sns" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Wolfenstein II: The New Colossus",
    'Lánzate a la segunda revolución americana en Wolfenstein II: The New Colossus para Nintendo Switch. Lucha en un Manhattan posnuclear, en un ocupado Roswell, en Nuevo México y en los pantanos y bulevares asediados de Nueva Orleans, mientras liberas al pueblo americano del régimen. En el papel de B. J. Blazkowicz, vive una historia inolvidable repleta de acción que cobra vida gracias a personajes extraordinarios. Reúnete con tus amigos y tus compañeros en la lucha por la libertad mientras te enfrentas a la malvada Frau Engel y a su ejército. Dirige la Segunda Revolución Americana como quieras, desde casa o sobre la marcha. Y, para lograr la mezcla perfecta de inmersión y precisión, usa los excepcionales controles de movimiento de Nintendo Switch para detener la amenaza del régimen.',
    60,
    50,
    18,
    "2018-06-29",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/q5-8bOdgbQ8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Final Fantasy X-X2 HD Remastered",
    'FINAL FANTASY X narra la historia de Tidus, una estrella del blitzbol que se embarca junto a la hermosa invocadora Yuna en una misión para poner fin a la espiral de destrucción que una terrible amenaza conocida como "Sinh" ha provocado en el mundo de Spira. FINAL FANTASY X-2 nos lleva de vuelta al mundo de Spira dos años después del comienzo de la Calma Eterna. Tras ver una imagen extrañamente conocida en una esfera, Yuna se convierte en cazaesferas y, en compañía de Rikku y Paine, parte en un viaje en busca de respuestas. Basado en las versiones internacionales de los juegos que previamente se publicaron en exclusiva en Japón y Europa, FINAL FANTASY X/X-2 HD Remaster lleva estos clásicos atemporales a la actual generación de fans, ya sean nuevos o de toda la vida.',
    50,
    50,
    12,
    "2016-05-12",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/Dc1A8hzXOMQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Civilization VI",
    'Civilization, creado originalmente por Sid Meier, el legendario diseñador de videojuegos, es un juego de estrategia por turnos en el que tu objetivo es construir un imperio que resista el paso del tiempo. Conquista el mundo entero estableciendo y liderando tu propia civilización desde la Edad de Piedra hasta la Era de la Información. Libra guerras, utiliza la diplomacia, fomenta el progreso de tu cultura y enfréntate cara a cara a los líderes más importantes de la historia para crear la civilización más grande jamás conocida. Civilization VI ofrece nuevas maneras de interactuar con tu mundo: ahora las ciudades se expanden físicamente en el mapa, la investigación activa de tecnología y cultura desbloquea nuevos potenciales y los líderes actúan en función de sus rasgos históricos mientras intentas conseguir la victoria de una de las cinco maneras posibles.',
    50,
    50,
    12,
    "2018-11-16",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/5KdE0p2joJw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Bioshock: The Collection",
    'Bioshock: explora la ciudad submarina de Rapture, un refugio para las mentes más brillantes de la sociedad que se ha convertido en una distópica pesadilla a causa de la arrogancia de un hombre. BioShock 2 Remastered: contempla Rapture a través de los ojos del sujeto Delta, un aterrador prototipo de Big Daddy en una misión de vida o muerte para rescatar a su desaparecida Little Sister. BioShock Infinite: The Complete Edition: endeudado con la gente equivocada, el investigador privado Booker DeWitt debe emprender una tarea imposible: viajar a Columbia, una ciudad flotante sobre las nubes, y rescatar a una misteriosa mujer llamada Elizabeth.',
    50,
    50,
    12,
    "2020-05-29",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/U3RXpEXA0jw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("The Outer Worlds",
    'The Outer Worlds es un galardonado JDR de ciencia ficción para un jugador en primera persona de Obsidian Entertainment y Private Division. En The Outer Worlds, te despiertas de la hibernación en una nave colona que se ha perdido mientras se dirigía a Halcyon, la colonia más lejana de la Tierra, al límite de la galaxia, solo para encontrarte en medio de una intrincada conspiración que amenaza con destruirla. A medida que exploras los confines del espacio y te encuentras con diversas facciones, todas ávidas de poder, la personalidad que decidas adoptar determinará cómo se desarrolla esta historia basada en el personaje jugador. En las ecuaciones de la colonia, tú eres la variable imprevista. CARACTERÍSTICAS PRINCIPALESDe acuerdo con la tradición de Obsidian, tú decides la manera en que quieres enfocar The Outer Worlds. Tus elecciones afectan no solo a la manera en que se desarrolla la historia, sino también a la conformación de tu personaje, a las historias de los compañeros y a los posibles escenarios finales. The Outer Worldsintroduce por primera vez la idea de los defectos. Los defectos forman parten también de cualquier héroe convincente. Mientras juegas a The Outer Worlds, el juego hace un seguimiento de tu experiencia para descubrir lo que no se te da del todo bien. ¿Los raptidons no dejan de atacarte? Adoptar el defecto de la raptidofobia te penalizará a la hora de enfrentarte a estas feroces criaturas, pero te compensará de inmediato con una ventaja adicional para el personaje. Esta aproximación opcional al juego te ayuda a crear el personaje que quieres mientras exploras Halcyon. Durante el viaje hasta la colonia más lejana, encontrarás a muchos personajes que querrán formar parte de tu tripulación. Armados con habilidades únicas, estos compañeros tendrán sus propias misiones, motivaciones e ideales. Tú decides cómo ayudarlos a alcanzar sus objetivos o cómo utilizarlos para tus propios fines. Halcyon es una colonia que está en los confines de la galaxia y que es propiedad de una junta corporativa, que la dirige. Lo controlan todo... excepto los monstruos alienígenas que aparecieron cuando la terraformación de los dos planetas de la colonia no salió exactamente según los planes. Busca una nave, reúne una tripulación y explora los asentamientos, las estaciones espaciales y otros lugares misteriosos de Halcyon.',
    60,
    40,
    16,
    "2020-04-23",'<iframe width="616" height="347" src="https://www.youtube.com/embed/j_JtczD-9Ko" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Streets of Rage 4",'El clásico de todos los tiempos Streets of Rage, conocido como Bare Knuckle (ベア・ナックル Bea Nakkuru) en Japón, es una trilogía de beat ‘em ups famosa por su mecánica de juego intemporal y su música influida por el dance electrónico. Streets of Rage 4 desarrolla el estilo de juego de la trilogía clásica con nuevas mecánicas, magníficos gráficos dibujados a mano y una banda sonora del máximo nivel. Una saga emblemática como Streets of Rage cuenta con personajes emblemáticos: Axel, Blaze y otros veteranos, reunidos para limpiar las calles. Con algunos movimientos nuevos y temas flipantes para escuchar, nuestros héroes están listos para repartir estopa a todo trapo contra un grupo emergente de imprudentes criminales. Streets of Rage 4 será la primera entrega de la saga principal en 25 años y supondrá el glorioso retorno de Axel y Blaze a las palizas con scroll lateral. Con lujosas animaciones dibujadas a mano, nuevas habilidades de combate y temas originales de un impresionante equipo de compositores, Streets of Rage 4 será al mismo tiempo un magistral homenaje y una revitalización de la acción clásica que tanto gusta a los fans. La saga es famosa en todo el mundo por su música influida por el dance electrónico. De la banda sonora de Streets of Rage 4 se encarga un elenco estelar de músicos como Yuzō Koshiro, Motohiro Kawashima, Yoko Shimomura, Hideki Naganuma, Keiji Yamagishi y muchos más, que crearán temas nuevos e impresionantes para este nuevo capítulo de la saga Streets of Rage.',
    25,
    50,
    16,
    "2020-04-23",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/j_JtczD-9Ko" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Undertale",
    'Baja al inframundo para explorar un hilarante y acogedor mundo lleno de monstruos peligrosos. Ten una cita con un esqueleto, baila con un robot, cocina con una pescadera... o destruye a todos sin pestañear.',
    15,
    50,
    12,
    "2018-09-18",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/SqjY_-beWi0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Celeste",
    'Ayuda a Madeline a sobrevivir a los demonios de su interior en su viaje a la cima de la montaña Celeste en este cuidadísimo plataformas diseñado a mano por los creadores del clásico multijugador TowerFall. Los controles son sencillos y accesibles: salta, acelera en el aire y escala, pero con varias capas de profundidad de expresión que habrá que dominar, pues cada fracaso enseña una lección. Las reapariciones relámpago te permiten seguir escalando mientras desvelas misterios y te enfrentas a numerosos peligros. Es el momento, Madeline. Respira hondo. Tú puedes hacerlo.',
    20,
    50,
    6,
    "2018-01-25",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/70d9irlxiB4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("L.A. Noire",
    'Un thriller policíaco oscuro y violento ambientado en el Los Ángeles de la década de 1940. En plena edad de oro del Hollywood de posguerra, Cole Phelps es detective de policía en Los Ángeles, una ciudad que se ahoga en su propio éxito. La corrupción campa a sus anchas, el tráfico de drogas está en ebullición y la tasa de criminalidad nunca había sido tan elevada. En su lucha por ascender y hacer lo correcto, Phelps debe descubrir la verdad que se oculta tras una serie de incendios provocados, extorsiones y asesinatos brutales. Deberá enfrentarse al hampa de Los Ángeles e incluso a los miembros de su departamento para sacar a la luz un secreto que podría sacudir los cimientos de la ciudad. Gracias a una revolucionaria tecnología de animación, que captura todos los matices de la interpretación facial de los actores con un nivel de detalle asombroso, L.A. Noire combina la acción apabullante con el trabajo de detective para ofrecer una experiencia interactiva sin precedentes. Resuelve crímenes brutales, tramas y conspiraciones inspiradas en hechos reales que sucedieron en Los Ángeles en 1947, una de las épocas más violentas y corruptas en la historia de la ciudad. Busca pistas, persigue a los sospechosos e interroga a los testigos mientras buscas la verdad en una ciudad donde todo el mundo esconde algo.',
    50,
    30,
    18,
    "2017-11-14",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/ZbPxNGh7dto" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

/* PS4 */
("God of War",
    'Las compañías Santa Monica Studio y Sony Interactive Entertainment han unido sus fuerzas para catapultar al mercado un nuevo episodio en la historia de uno de los personajes de videojuegos más famosos de todos los tiempos. Con esta nueva edición, los jugadores se adentrarán en las tierras heladas de Midgard, un ambiente repleto de mitología nórdica. ¡Compra God of War PS4 CD segunda mano y expande tu galería física de juegos al mismo tiempo que ahorras dinero! Kratos emprende un viaje para cumplir con el último deseo de su esposa fallecida: esparcir sus cenizas desde el punto más alto de las montañas que recorren los nueve reinos. Su hijo emprenderá el viaje con él y lo ayudará al momento de enfrentarse con monstruos y dioses que habitan en aquellas tierras. Pero Kratos reprime su naturaleza divina, y el no haber estado presente en la infancia de su hijo Atreus trae ahora complicaciones en su relación con él; dos factores que ponen en riesgo toda la misión.',
    60,
    40,
    18,
    "2018-04-20",
    '<iframe width="853" height="480" src="https://www.youtube.com/embed/dK42JGgkoF8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Bloodborne",
    'Hidetaka Miyazaki creador junto con From Software de juegos muy hardcore como Demon’s y Dark Souls, nos traen otro juego de culto al género de acción RPG. Bloodborne es un juego en tercera persona en el que te tendrás que enfrentar a diferentes tipos de bestias y monstruos mientras exploras lo que el mapa tiene que ofrecer a la vez que descubres los secretos de la historia. Este es un juego apto sólo para los más capaces y atrevidos, además de tener mucha paciencia. Hay algunos niveles que tendrás que intentar pasarlo varias veces. ¡No apto para noobs, eso está claro!',
    50,
    75,
    16,
    "2015-05-24",
    '<iframe width="853" height="480" src="https://www.youtube.com/embed/G203e1HhixY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("The Last Of Us Parte 2",
    'Cinco años después de su peligroso viaje por un Estados Unidos postapocalíptico, Ellie y Joel se han asentado en Jackson, Wyoming. La vida en una próspera comunidad de supervivientes les ha permitido disfrutar de paz y estabilidad, a pesar de la amenaza constante de los infectados y de otros supervivientes más desesperados. Sin embargo, tras un hecho violento que altera esa paz, Ellie se embarca en un viaje implacable en busca de justicia. A medida que va dando caza a los culpables uno a uno, deberá afrontar las devastadoras consecuencias, tanto físicas como emocionales, de sus acciones.',
    60,
    50,
    18,
    "2019-04-26",
    '<iframe width="853" height="480" src="https://www.youtube.com/embed/FKtaOY9lMvM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Days Gone",
    '¿Tienes la habilidad y el nervio que hay que tener para sobrevivir a los horrores de un mundo destrozado en esta aventura de acción que incluye un enorme mundo abierto? Ponte en los zapatos salpicados de barro del otrora forajido Deacon St. John, un motero cazarrecompensas que intenta buscar una razón por la que vivir en una tierra repleta de muerte. Investiga asentamientos abandonados en busca de equipamiento para crear objetos valiosos y armas o arriesgarte a tratar con otros supervivientes que se ganan la vida a duras penas mediante intercambios justos... u otros métodos más violentos. En un contexto en el que una pandemia global ha arrasado la humanidad y unas criaturas feroces conocidas como engendros le dan caza, cualquier error podría ser tu último intento de labrarte una nueva vida en el hostil desierto del noroeste pacífico.',
    70,
    30,
    12,
    "2019-04-26",
    '<iframe width="853" height="480" src="https://www.youtube.com/embed/FKtaOY9lMvM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Uncharted 4: El Desenlace del Ladrón",
    'Tres años después de los hechos acaecidos en Uncharted 3: La Traición de Drake, Nathan Drake ha dejado atrás la búsqueda de tesoros. Sin embargo, el destino no tarda en llamar a su puerta cuando su Sam reaparece pidiéndole ayuda para salvar su vida, además de ofrecerle participar en una aventura ante la que Drake no puede resistirse.   Los dos parten a la caza del tesoro perdido del capitán Henry Avery y en busca de Libertalia, el utópico refugio pirata que se halla en lo más profundo de los bosques de Madagascar. Uncharted 4: El Desenlace del Ladrón embarca al jugador en un viaje alrededor del mundo por islas selváticas, grandes ciudades y nevados picos montañosos en busca del tesoro de Avery.',
    60,
    50,
    12,
    "2016-05-10",
    '<iframe width="853" height="480" src="https://www.youtube.com/embed/hh5HV4iic1Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Marvel's Spider-Man",
    'Sony Interactive Entertainment, Insomniac Games y Marvel se han unido para crear una aventura original de Spider-Man. Este Spider-Man no se parece al que ya conocías ni al que has visto en las pelis. Hablamos de un Peter Parker veterano que ha pulido sus habilidades combatiendo el crimen en la Nueva York de Marvel. A su vez, también lucha por poner en orden su caótica vida personal y su carrera, con el destino de millones de neoyorquinos en sus manos.',
    50,
    10,
    12,
    "2019-08-27",
    '<iframe width="853" height="480" src="https://www.youtube.com/embed/q4GdJVvdxss" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Ghost of Tsushima",
    'A finales del siglo XIII, el Imperio mongol asola naciones enteras en su conquista del Este. La isla de Tsushima es todo lo que se interpone entre las islas principales de Japón y una gigantesca flota de invasores mongoles liderada por el astuto y despiadado general Khotun Kan. Mientras la isla arde tras la primera oleada del asalto mongol, el guerrero samurái Jin Sakai se convertirá en uno de los últimos supervivientes de su clan. Jin está decidido a hacer lo que sea, cueste lo que cueste, para proteger a su gente y recuperar su hogar. Para ello, deberá dejar a un lado las tradiciones que le han formado como guerrero para forjar un nuevo camino, el del Fantasma, y librar una guerra poco convencional por la libertad de Tsushima.',
    70,
    50,
    16,
    "2020-07-17",
    '<iframe width="853" height="480" src="https://www.youtube.com/embed/iqysmS4lxwQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Gran Turismo Sport",
    'La premiada serie del simulador de conducción real llega por primera vez a PlayStation 4 para ponerte rumbo a una experiencia trepidante de alto octanaje. Compartiendo el volante con la FIA (Federación Internacional del Automóvil), Polyphony Digital ha diseñado un juego accesible y puesto a punto para que todo el mundo lo disfrute, desde los pilotos menos experimentados hasta los ases del motor.   Abróchate el cinturón y prepárate para dos campeonatos online: representa a tu país en la Copa de Naciones o conduce en nombre de tu marca de coches favorita en la Copa de Fabricantes. ¿Te alzarás con una victoria histórica en el primer videojuego reconocido oficialmente en el mundo real del automovilismo?',
    30,
    40,
    6,
    "2017-10-17",
    '<iframe width="853" height="480" src="https://www.youtube.com/embed/moDdHXpxWrw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

/* PC */
("Half-Life",
    'Half-Life significó un antes y un después para los FPS ya que integraba tecnología que nos presentaba un mundo completamente inmersivo y sumamente realista para la época. Por esta razón fue galardonado como Juego del Año por más de 50 publicaciones en todo el mundo, incluso algunos se atreven a catalogarlo como "el mejor título para PC en la historia". Un juego obligado.',
    10,
    30,
    12,
    "1998-11-8",
    '<iframe width="853" height="480" src="https://www.youtube.com/embed/moDdHXpxWrw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Half-Life 2",
    'Años más adelante Valve conseguiría lo impensable, hacer una secuela de Half-Life que superara a la original, aunque aquí las opiniones están divididas. Lo que es un hecho, es que Half-Life 2 retomó lo mejor del primera parte, lo mejoró, añadió nuevos elementos y creó una joya que hoy día no tiene rival. El realismo, los retos y esa carga emocional lo hacen un juego único.',
    15,
    50,
    12,
    "2004-11-16",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/ID1dWN3n7q4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Deus Ex",
    'Deus Ex llegó en su momento a cambiar todo el panorama de juegos debido a que abordaba una narrativa de temas complejos dentro de un escenario cyberpunk. Desigualdad social, avance tecnológico y sobrepoblación, hasta contaminación, enfermedades y terrorismo. La brecha social es cada vez mayor y el mundo está por colapsar si no hacemos algo. Esta aventura mezcla de forma magistral elementos RPG en primera persona con estrategia y el uso de tácticas furtivas.',
    10,
    50,
    12,
    "2000-06-22",
    '<iframe width="580" height="435" src="https://www.youtube.com/embed/ZpEoLH0cUSw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Quake",
    'Si DOOM es el padre de los FPS, Quake es su hermano casi gemelo. Tras conseguir un éxito sin precedentes, id Software repitió la hazaña y regresó con un segundo bombazo. Quake mejoró lo aprendido en DOOM y lo llevó a un nuevo nivel. Entornos 3D en un mundo gótico medieval que atrapó a todos no sólo por esa estética, sino también por su enfoque multiplayer de ¡8 jugadores! Toda una revolución para esa época. Quake también fue el primer shooter en tener mapas específicos para el modo multijugador.',
    12,
    50,
    18,
    "1996-06-22",
    '<iframe width="580" height="435" src="https://www.youtube.com/embed/DV2vhmtGBqM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Portal",
    'Portal es la nueva aventura para un solo jugador de Valve. Ambientado en los misteriosos laboratorios de Aperture Science, Portal ha sido calificado como uno de los juegos más innovadores de los últimos tiempos y ofrece incontables horas de rompecabezas nunca vistos. El juego está diseñado para cambiar radicalmente el modo en que los jugadores enfocan, sopesan y reaccionan a las circunstancias en un entorno determinado, al igual que la pistola antigravedad abrió un nuevo mundo de posibilidades a la hora de manipular objetos. Los jugadores deben resolver rompecabezas y desafíos basados en las leyes físicas abriendo portales y desplazando objetos, o incluso sus propios avatares, a través del espacio.',
    10,
    50,
    6,
    "2007-10-10",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/TluRVBhmf8w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Age of Empires II",
    'Age of Empires II: HD Edition enamora tanto a los fanáticos de la edición original como a los jugadores que se lanzan por primera vez a la aventura de Age of Empires II. Explora todas las campañas de un jugador originales incluidas tanto en The Age of Kings como en The Conquerors; elige entre 18 civilizaciones que comprenden un período histórico de más de mil años; transpórtate al universo en línea y desafía a otros jugadores de Steam para completar la conquista del mundo medieval. Desarrollado en su versión original por Ensemble Studios, y ahora reeditado en alta definición por Hidden Path Entertainment, Skybox Labs y Forgotten Empires ¡Microsoft Studios se enorgullece en traer Age of Empires II: HD Edition a Steam!',
    10,
    50,
    6,
    "2013-04-10",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/ZOgBVR21pWg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Duke Nukem 3D",
    'Mata como en 1996. Únete al héroe de acción más grande en Duke Nukem 3D: Gira mundial del 20.º aniversario mientras vuelve a salvar la Tierra, apalizando alienígenas y salvando churris. Ábrete paso a través de hordas de alienígenas en cuatro episodios clásicos de Duke Nukem 3D además de un NUEVO QUINTO EPISODIO de la mano de los DISEÑADORES DE LOS EPISODIOS ORIGINALES con MÚSICA NUEVA del COMPOSITOR ORIGINAL y NUEVOS diálogos de Duke con la VOZ ORIGINAL de Duke Nukem. • NUEVO episodio CINCO de los diseñadores originales Allen Blum III and Richard “Levelord®” Gray. • Nuevo contenido en el episodio 5: Duke Nukem logra un INCINERADOR para freír a los "firefly". • Nuevas "frases de Duke" y regrabaciones del actor de doblaje original de Duke Nukem: Jon St. John. • Nueva banda sonora del episodio 5 creada por el autor del tema y compositor original, Lee Jackson. • ¡Código fuente original de 1996 y motor "2.5D" remasterizado en 3D real para PC!',
    12,
    20,
    50,
    "2016-10-11",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/jNOU3BSB_bc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Myst",
    'Entra en un mundo en el que nada es lo que parece... ¡y la aventura no tiene límites! Viaja a una inquietante isla misteriosa en la que cada roca, cada trozo de papel, cada sonido transportado por el viento contiene una pista para resolver un antiguo misterio. Entra, si te atreves, en un crudo y bello escenario envuelto por la intriga y la injusticia. ¡Sólo tu ingenio y tu imaginación poseen la clave para descifrar una sorprendente traición que aconteció en una época pasada! Piérdete en una fantástica exploración virtual, ahora más irresistible que nunca, en el increíble Myst® Masterpiece Edition. Un impresionante realismo gráfico que difumina la línea que separa ficción y realidad y que pondrá a prueba tu ingenio, tu instinto y tu capacidad de observación de un modo que no te imaginas. Siente la llamada de la fantasía... ¿podrás resistirte?',
    5,
    50,
    12,
    "1999-01-02",
    '<iframe width="657" height="370" src="https://www.youtube.com/embed/1UYfMaDnsoY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

/* NINTENDO SWITCH */
("The Legend of Zelda: Breath of the Wild",
    'The Legend of Zelda: Breath of the Wild para Nintendo Switch es un juego de acción y aventuras de Nintendo, la última entrega de la larga serie de The Legend of Zelda. El juego ofrece un formato de mundo abierto, con amplia libertad y pocas instrucciones para los jugadores. La jugabilidad consiste en recolectar utensilios y materiales, completar misiones secundarias y resolver ingeniosos puzles y mazmorras.',
    70,
    50,
    6,
    "2017-03-03",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/ofH5ptn5w-A" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Super Smash Bros. Ultimate",
    'Super Smash Bros. Ultimate para Nintendo Switch es un aclamado crossover de lucha que cuenta con una amplia gama de personajes de otros juegos y franquicias, lo cual ha sido posible gracias a la colaboración entre Bandai, Sora y Nintendo. Naturalmente, dado el título del juego, todos los personajes del mundo de la serie de Mario Bros son incluidos, haciendo gala de diferentes habilidades y grados de personalización. Esta es la quinta entrega de la franquicia, y nos ofrece la inmensa cantidad de setenta (¡70!) personajes jugables.  Algunas de las series cuyos personajes aparecen en este juego son: Animal Crossing, Castlevania y Metroid, además de algunos Pokémon selectos del universo de Pokémon.',
    70,
    50,
    6,
    "2018-12-07",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/WShCN-AYHqA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Super Mario Maker 2",
    'Los fanáticos del pequeño fontanero italiano y su pandilla se deleitarán con cualquier juego de Mario, pero Super Mario Maker 2 ofrece posibilidades infinitas para jugadores de todos los niveles y edades.',
    60,
    80,
    6,
    "2019-05-12",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/HrTZIzLZ8hU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Super Mario Odyssey",
    'Super Mario Odyssey para Nintendo Switch es un simpático juego de plataformas en 3D basado en la recolección de lunas que cuenta con una jugabilidad innovadora pero a la vez reconfortantemente familiar, ¡la cual te ofrecerá diversión por montones!',
    70,
    50,
    6,
    "2017-10-27",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/5kcdRBHM7kM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("The Legend of Zelda: Link's Awakening",
    'The Legend of Zelda: Links Awakening para Nintendo Switch es un remake del juego de 1993 originalmente lanzado para la Game Boy. A tono con los orígenes del juego, los gráficos y el estilo de la jugabilidad presentan características retro que le resultarán de lo más atractivas y familiares a cualquiera que haya crecido junto a la industria de los videojuegos o que guste de jugar títulos de la vieja escuela. El juego fue relanzado para la Game Boy Color, y más adelante para la Nintendo 3DS.',
    60,
    50,
    6,
    "2019-09-20",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/PtC6U8hOZTk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Animal Crossing: New Horizons",
    'Animal Crossing: New Horizons para Nintendo Switch es un juego de simulación que es principalmente para un jugador con algunos elementos multijugador y es la quinta entrega del juego que presenta la cara familiar y querida de Tom Nook, esta vez en una isla desierta. Al igual que con los otros juegos de Animal Crossing, construirás tu hogar, conocerás y ayudarás a tus vecinos y amigos animales y juntos construiréis una comunidad.',
    60,
    50,
    6,
    "2020-03-20",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/5YPxiTLMcdg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Xenoblade Chronicles Definitive Edition",
    'Un clásico de los juegos de rol renace como Xenoblade Chronicles: Definitive Edition para Nintendo Switch. A consecuencia de una invasión devastadora, deberás embarcarte en un viaje que te llevará más allá del horizonte. ¿Lograrás cambiar el futuro o se verá tu pueblo abocado a la extinción?',
    60,
    50,
    6,
    "2020-05-29",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/KfjVIQug6_E" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Xenoblade Chronicles 2",
    'Viaja hasta la cuna de la humanidad: el Elíseo. En el enorme RPG Xenoblade Chronicles 2 para Nintendo Switch, explorarás un océano infinito de nubes entre las que perviven los restos de una civilización sobre los lomos de enormes bestias llamadas titanes. Descubre la historia de Rex y su nueva amiga Pyra, un ser misterioso ser conocido como Blade que le otorga enorme poder. Juntos emprenderán la búsqueda del hogar perdido de Pyra, el Elíseo, la cuna de la humanidad. Un mundo construido sobre titanes El mundo de Alrest se ha desarrollado sobre los cuerpos de enormes criaturas llamadas titanes. En el centro de Alrest crece el Árbol del Mundo, y sobre él se encuentra el paraíso definitivo para toda la humanidad, el Elíseo. Blades y Pilotos: un destino compartido En este mundo, ciertos individuos conocidos como Pilotos establecen una curiosa simbiosis con los Blades, formas de vida artificial que les prestarán sus armas y poderes durante el combate. Un mundo desgarrado por la rivalidad. Rex y sus compañeros no son los únicos que buscan el Elíseo. Entre la gente a la que se encontrarán también hay enemigos que buscan hacerse con la Égida Pyra. Lucha con los Blades.  Todo tipo de monstruos aguardan a nuestros héroes. Alterna entre Blades en medio del combate, cada uno con sus propias habilidades y armas, y planta cara al enemigo hasta las últimas consecuencias.',
    60,
    50,
    6,
    "2020-12-05",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/Xjpao-nD5YU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

/* XBOX ONE */
("State of Decay 2",
    'Lo más peligroso en un apocalipsis de zombies no siempre son los zombies, sino la disminución de la comida, enfermedades o luchas internas. En State of Decay 2 debes transformar a un grupo de supervivientes inexpertos en una comunidad estable capaz de sostenerse y defenderse contra ataques de zombies, mientras te aventuras en las áreas circundantes, en gran parte invadidas, en busca de información sobre una cura para la plaga. El juego es parte acción y parte de estrategia, y los niveles de suministro de tu grupo son casi siempre tu principal preocupación, pero disminuyen a un ritmo lo suficientemente lento como para evitar que State of Decay 2 sea excesivamente estresante.',
    30,
    50,
    18,
    "2018-03-21",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/1ljRGIDGdwU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Halo 5: Guardians",
    'Halo 5: Guardians ofrece experiencias multijugador épicas que incluyen distintos modos, completas herramientas de construcción de niveles y la historia más dramática de Halo hasta la fecha. ¡Con la publicación de 10 contenidos gratuitos desde el lanzamiento del juego, Halo 5: Guardians ofrece más contenido, más locura multijugador y más variedad que cualquier Halo hasta la fecha! UNA CAMPAÑA ÉPICA La saga del Jefe Maestro continúa en una experiencia cooperativa para cuatro jugadores que se desarrolla en tres mundos distintos. Una fuerza misteriosa e imparable amenaza la galaxia. El Jefe Maestro ha desaparecido y su lealtad es puesta en duda. Embárcate en un viaje que cambiará la historia y el futuro de la humanidad. ZONA DE GUERRA MULTIJUGADOR Juega con tus amigos y compite contra tus rivales en tres enormes modos multijugador nunca vistos en la franquicia Halo: Zona de guerra, Asalto en zona de guerra y Tiroteo en zona de guerra.',
    70,
    60,
    18,
    "2021-01-01",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/Rh_NXwqFvHc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Dance Central Spotlight",
    'El último juego de la galardonada serie Dance Central se centra específicamente en la experiencia de baile. Con más rutinas por canción y una enorme biblioteca de canciones a la venta, Dance Central Spotlight te permite personalizar tus sesiones de baile más que nunca.Rompe a sudar con un modo Fitness mejorado que incluye series de fitness creadas especialmente, o activa con la voz el modo "Vamos a practicar", para practicar movimientos difíciles inmediatamente sin cambiar de canción. Y con coreografías diseñadas para todo el mundo, desde los usuarios nuevos a los avanzados, cualquiera puede saltar al escenario.',
    10,
    50,
    6,
    "2014-06-02",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/sKmo9AloGtE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Gears of War 4",
    'Prepara tus Lancers para el multijugador y el cooperativo con el pase de temporada de Gears of War 4. Únete a esta experiencia VIP y consigue las siguientes ventajas:',
    50,
    50,
    18,
    "2016-10-11",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/mu2RsEUOA3M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Forza Motorsport 7",
    'FORZA MOTORSPORT 7 ES UN TÍTULO DE XBOX PLAY ANYWHERE QUE OFRECE CAPACIDAD DE JUEGO ENTRE LAS PLATAFORMAS DE XBOX ONE Y PC CON WINDOWS 10. Experimenta la emoción del automovilismo al límite en el juego de carreras de coches más completo hermoso y auténtico jamás desarrollado. Disfruta los asombrosos gráficos a 60fps y resolución nativa 4K en HDR. Colecciona y conduce más de 700 coches entre los que destaca la colección más grande hasta ahora de Ferrari Porsche y Lamborghini. Pon a prueba tus habilidades en 30 lugares famosos y 200 listones con condiciones variables cada vez que regresas a la pista. El sistema Xbox Play Anywhere requiere una compra digital. Las características pueden variar entre las versiones del juego para Xbox One y Windows 10. El disco para Xbox One solo funciona con ese sistema. Necesitas una membresía Xbox Live Gold (se vende por separado) para jugar en multijugador en Xbox One. El juego entre plataformas solo está disponible en países donde Xbox Live esté habilitado consulta el sitio http://www.xbox.com/live/countries. Algunas opciones de música podrían no estar disponibles en determinados países. Visita http://forzamotorsport.net para obtener más información.',
    50,
    70,
    6,
    "2017-10-03",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/9aAr5blVy0g" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Ori and the Will of the Wisps",
    'Descubre el pasado de Naru en dos nuevos escenarios. • Domina dos nuevas y poderosas habilidades: Deslizar y Ráfaga de luz. • Encuentra nuevas áreas secretas y explora Nibel más rápido teletransportándote entre pozos de espíritu. El bosque de Nibel perece. Tras una poderosa tormenta que desencadena una serie de devastadores acontecimientos, Ori emprende una travesía para encontrar el valor y enfrentarse a una némesis oscura para salvar el bosque de Nibel. "Ori and the Blind Forest" cuenta la historia de un joven huérfano destinado a realizar heroicas hazañas, todo ello en un juego de acción y plataforma visualmente impresionante de Moon Studios. Cuenta con dibujos hechos a mano, personajes animados de forma meticulosa, una banda sonora sin igual y decenas de nuevas características. Esta Definitive Edition de "Ori and the Blind Forest" explora una profunda y emotiva historia sobre el amor, el sacrificio y la esperanza que vive dentro cada uno de nosotros.',
    50,
    70,
    6,
    "2018-06-10",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/2kPSl2vyu2Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Grounded",
    'El mundo es un lugar inmenso, precioso y peligroso (sobre todo cuando te han encogido al tamaño de una hormiga). Explora, construye y sobrevive, todo al mismo tiempo, en esta aventura multijugador de supervivencia en primera persona. ¿Podrás prosperar junto a las hordas de insectos gigantes, luchando por sobrevivir a los peligros del jardín trasero?',
    30,
    50,
    6,
    "2020-07-28",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/ciVgq5xO5G0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),

("Deep Rock Galactic ",
    'Deep Rock Galactic es un shooter cooperativo de ciencia ficción en primera persona con enanos espaciales de armas tomar, entornos totalmente destruibles, cuevas generadas procedimentalmente y hordas interminables de monstruos alienígenas.Trabajad en equipo para excavar, explorar y abriros paso peleando a lo largo de un enorme sistema de cuevas repletas de enemigos mortíferos y valiosos recursos. ¡Necesitaréis la ayuda de vuestros compañeros si queréis sobrevivir a los sistemas de cuevas más mortíferos de la galaxia!Escoge la clase apropiada para cada tarea. Aniquila a los enemigos como Gunner, ve por delante y alumbra las cuevas como Scout, atraviesa la roca viva como Driller o da apoyo al equipo construyendo estructuras defensivas y torretas como Engineer.Destruye todo lo que te rodea para alcanzar tu objetivo. No hay un camino establecido, puedes completar la misión a tu manera. Taladra en línea recta hasta llegar a tu objetivo o crea una intrincada red de galerías para explorar tus alrededores: tú decides. Pero ve con cuidado, ¡no querrás toparte con un enjambre de alienígenas sin ir preparado!Explora una red de sistemas de cuevas generadas procedimentalmente y llenas de enemigos con los que luchar y riquezas que recoger. Siempre hay algo nuevo que descubrir, y no hay dos recorridos iguales.Los enanos saben lo que necesitan para hacer un buen trabajo. Cuentan con las armas más potentes y las herramientas más avanzadas de la galaxia: lanzallamas, ametralladoras montadas, lanzadores de plataformas portátiles, y mucho, mucho más.Las cuevas subterráneas son oscuras y albergan terrores. Tendrás que llevar tus propias luces para poder iluminar estas cavernas oscuras como bocas de lobo.',
    30,
    50,
    6,
    "2018-02-28",
    '<iframe width="616" height="347" src="https://www.youtube.com/embed/1FaT_khr48U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>')

/*("titulo",'descripcion',precio,edad_recomendada,fecha,'trailer'),*/


;

/* GENEROS */
INSERT INTO GENERO(genero) VALUES
("Acción"),
("Arcade"),
("Aventura"),
("Beat'em all"),
("Carreras"),
("Cooperación"),
("Deporte"),
("Estrategia"),
("FPS"),
("Lucha"),
("Multijugador"),
("Plataformas"),
("RPG"),
("Simulación")
;

/* IDIOMAS */
INSERT INTO IDIOMA(idioma) VALUES
("Español"),
("Inglés"),
("Francés"),
("Italiano"),
("Alemán"),
("Checo"),
("Holandés"),
("Japonés"),
("Coreano"),
("Polaco"),
("Portugués"),
("Ruso"),
("Chino")
;

/* PLATAFORMAS */
INSERT INTO PLATAFORMA(plataforma) VALUES
("PS4"),
("PC"),
("Nintendo Switch"),
("XBOX One")
;

/* CLAVES */
INSERT INTO CLAVE VALUES
("ONR8-1A42-4Z0Y", false, NULL, 1, 1),
("ZD28-MZU189-6ICTX", false, NULL, 2, 1),
("D8FZ-U189-6ICT-AS12", false, NULL, 3, 1),
("EE1UP-WHXXU-8TABZ-6ICTX", false, NULL, 4, 1),

("F3FK-5CT2D-SN7L", false, NULL, 1, 2),
("B4HQH-J57GT-27JYI", false, NULL, 2, 2),
("NMZF-N20J-UVD8-NMZF", false, NULL, 3, 2),
("3QT6Y-FHDTX-VK26V-VK26V", false, NULL, 4, 2),

("9XI7-9PND-BW02", false, NULL, 1, 3),
("HWFSR-H92GX-OZJBY", false, NULL, 2, 3),
("IXN4-XP1C-S0AH-9PND", false, NULL, 3, 3),
("MHQ6K-OAQWO-0FUWY-VK26V", false, NULL, 4, 3),

("98YK-IQB5-X485", false, NULL, 1, 4),
("2IQPV-ZCK6N-WOXK1", false, NULL, 2, 4),
("U95E-W5QJ-T3WT-9PND", false, NULL, 3, 4),
("BJCOJ-TOK4Z-CEPQD-VK26V", false, NULL, 4, 4),

("X1MY-J3IQ-MG6G", false, NULL, 1, 5),
("YA9KS-FKZ3C-XQ8CD", false, NULL, 2, 5),
("M6IH-K3CG-R4UA-9PND", false, NULL, 3, 5),
("GYUCY-QJNM1-D1JNH-VK26V", false, NULL, 4, 5),

("9F31-7YIQ-Y119", false, NULL, 1, 6),
("7KIMR-ND3C6-EPUIX", false, NULL, 2, 6),
("O5SK-8GVO-053E-9PND", false, NULL, 3, 6),
("5ZQGO-P4RUA-A1Y11-VK26V", false, NULL, 4, 6),

("NSSF-BUZ8-XSV4", false, NULL, 1, 7),
("WWEZL-JQ4IP-2M4AN", false, NULL, 2, 7),
("P19M-X86K-TW1R-9PND", false, NULL, 3, 7),
("TMJ5A-YXW52-B2BPU-VK26V", false, NULL, 4, 7),

("MC3I-SHYF-2GTJ", false, NULL, 1, 8),
("GH2L4-XV1D8-EXF4U", false, NULL, 2, 8),
("GTF3-FY7G-CGKW-9PND", false, NULL, 3, 8),
("ZOMLQ-BHOVK-KWQJU-VK26V", false, NULL, 4, 8),

("31ID-0OYP-13ZQ", false, NULL, 1, 9),
("QCSR1-SPXVB-HVKYN", false, NULL, 2, 9),
("0UDH-6HE8-YKXZ-9PND", false, NULL, 3, 9),
("TRM84-SA4BR-5CF3N-VK26V", false, NULL, 4, 9),

("UXXL-0F57-4PEB", false, NULL, 1, 10),
("WQXF6-FZH3M-GA4CO", false, NULL, 2, 10),
("1IV7-VF9K-QR1F-9PND", false, NULL, 3, 10),
("JNBLN-QPBD8-7QH8B-VK26V", false, NULL, 4, 10),

("030D-6UCG-4R49", false, NULL, 1, 11),
("WTN3A-J9VHF-V1GD8", false, NULL, 2, 11),
("0OJ0-QGO1-28E0-9PND", false, NULL, 3, 11),
("2X02M-GLSZE-OELZ2-VK26V", false, NULL, 4, 11),

("KSHQ-JHPY-NQR8", false, NULL, 1, 12),
("CN0B8-ACHZZ-JVOHL", false, NULL, 2, 12),
("F192-0W0H-CRVI-9PND", false, NULL, 3, 12),
("EVJCD-0TRIV-Q8H6U-VK26V", false, NULL, 4, 12),

("NGQB-FZRC-8N81", false, NULL, 1, 13),
("7ZT9K-9GBHM-6BL2B", false, NULL, 2, 13),
("375W-B8M9-32XN-9PND", false, NULL, 3, 13),
("DRF0A-O77OI-V1N3V-VK26V", false, NULL, 4, 13),

("4LFC-HBIQ-L62Y", false, NULL, 1, 14),
("UQIYP-I205N-PJPVR", false, NULL, 2, 14),
("OF85-BKTE-7ZFA-9PND", false, NULL, 3, 14),
("91VJ0-R288T-KM2TE-VK26V", false, NULL, 4, 14),

("TDJI-E3IN-XGQS", false, NULL, 1, 15),
("NKIXZ-1WQ8D-6NFHW", false, NULL, 2, 15),
("3QHX-6L1H-AFK09-9PND", false, NULL, 3, 15),
("N03GH-VAZQ1-FFRHG-VK26V", false, NULL, 4, 15),

("T1LZ-8I7Y-6STH", false, NULL, 1, 16),
("QU4DL-5S5J6-HLYMQ", false, NULL, 2, 16),
("S3X1-19XO-RM0M-9PND", false, NULL, 3, 16),
("P9AFD-1XDDG-BQSLD-VK26V", false, NULL, 4, 16),

("XS6B-Q6PE-S1JQ", false, NULL, 1, 17),
("WNS12-BW21S-FH1A2", false, NULL, 2, 17),
("O1NO-KKS8-8VYK-9PND", false, NULL, 3, 17),
("FYW5Y-1POJB-S36TU-VK26V", false, NULL, 4, 17),

("5R33-K2SP-178U", false, NULL, 1, 18),
("9SVD9-S7WLP-B0RA1", false, NULL, 2, 18),
("WBE2-4EE9-3BX1-9PND", false, NULL, 3, 18),
("KJFFC-F9NJR-F53L9-VK26V", false, NULL, 4, 18),

("T21J-8U0V-7RF7", false, NULL, 1, 19),
("QDO6W-6A0AP-1NG35", false, NULL, 2, 19),
("35DI-RLAR-J4CU-9PND", false, NULL, 3, 19),
("EYVO6-MICQD-QHPUG-VK26V", false, NULL, 4, 19),

("37UW-NX5H-CRRW", false, NULL, 1, 20),
("9EA84-RPKFQ-MD63Q", false, NULL, 2, 20),
("8V5O-9JT8-K6BB-9PND", false, NULL, 3, 20),
("75JX5-LVVKJ-JQADD-VK26V", false, NULL, 4, 20),

("5O8J-28YK-2YTB", false, NULL, 1, 21),
("F71D9-WZ1C7-C3JKV", false, NULL, 2, 21),
("SKGN-4OT0-UJRO-9PND", false, NULL, 3, 21),
("3IAMH-B1WPD-96QN3-VK26V", false, NULL, 4, 21),

("NPRR-SUKN-CP34", false, NULL, 1, 22),
("DNPA1-922C0-UQRHE", false, NULL, 2, 22),
("42NA-TOHS-NHXR-9PND", false, NULL, 3, 22),
("RMBDC-SUCBU-D091C-VK26V", false, NULL, 4, 22),

("TVOA-6LM8-5I3P", false, NULL, 1, 23),
("3FKVB-LSORF-QAAKM", false, NULL, 2, 23),
("H2GM-8GO3-4ZLR-9PND", false, NULL, 3, 23),
("N9JAV-9336Z-296I0-VK26V", false, NULL, 4, 23),

("P332-5GPY-TCSE", false, NULL, 1, 24),
("K5P8P-70I74-7AURC", false, NULL, 2, 24),
("E6UN-GD97-OL9D-9PND", false, NULL, 3, 24),
("3IZ40-00QOV-VCER6-VK26V", false, NULL, 4, 24),

("70PZ-PQ5J-9U4Y", false, NULL, 1, 25),
("XNZ2C-7KZSQ-QWBIB", false, NULL, 2, 25),
("SW47-RT5Z-DX9J-9PND", false, NULL, 3, 25),
("VD5UK-T4139-AYUXA-VK26V", false, NULL, 4, 25),

("3SKY-MWYY-THXY", false, NULL, 1, 26),
("FYQ9-1V4Z-SMFK", false, NULL, 1, 27),
("DJ7I-32A8-1FUG", false, NULL, 1, 28),
("PLSV-ZQZE-Q3WP", false, NULL, 1, 29),
("GYG9-I79K-LPBU", false, NULL, 1, 30),
("VVZY-O80G-PMVI", false, NULL, 1, 31),
("L07H-81X7-ORJJ", false, NULL, 1, 32),
("TMLG-GMER-KO7X", false, NULL, 1, 33),

("BE953-0Z5WG-RMP8L", false, NULL, 2, 34),
("I4732-2BEF1-FVZ1Y", false, NULL, 2, 35),
("8FPP3-5KB7F-8826U", false, NULL, 2, 36),
("NN113-PU2CJ-VZZ7W", false, NULL, 2, 37),
("Q2J7Y-ZQ0BL-DFS4W", false, NULL, 2, 38),
("7FVYH-D7ZAC-M3XH8", false, NULL, 2, 39),
("45C3J-WMXSB-G8XQC", false, NULL, 2, 40),
("3FXZW-8IVW5-RL3JK", false, NULL, 2, 41),

("6C4X-VAH1-M4PD-M4PD", false, NULL, 3, 42),
("UU3O-26LH-QDXU-M4PD", false, NULL, 3, 43),
("3R99-WF0L-S6GL-M4PD", false, NULL, 3, 44),
("ZA42-ROOB-UW18-M4PD", false, NULL, 3, 45),
("WA3O-V7KE-U757-M4PD", false, NULL, 3, 46),
("F4HF-EKNE-D89P-M4PD", false, NULL, 3, 47),
("3NHF-HU47-RNWW-M4PD", false, NULL, 3, 48),
("SKVC-U9MQ-L9WR-M4PD", false, NULL, 3, 49),

("ZYLAJ-IUG0C-FUZKL-FUZKL", false, NULL, 4, 50),
("EEEOX-S6N1G-41DHW-FUZKL", false, NULL, 4, 51),
("O8954-EHHCR-F7WWI-FUZKL", false, NULL, 4, 52),
("8YNIN-CMIQF-UUX3D-FUZKL", false, NULL, 4, 53),
("SG92S-H36K1-019XK-FUZKL", false, NULL, 4, 54),
("469HZ-PHLEU-C0M3H-FUZKL", false, NULL, 4, 55),
("533Q9-F7QPU-NMICV-FUZKL", false, NULL, 4, 56),
("57LT5-6PPG4-8PQ12-FUZKL", false, NULL, 4, 57)


;

/* N:M VIDEOJUEGO-GENERO*/
INSERT INTO VIDEOJUEGO_GENERO VALUES 
-- MULTIPLATAFORMAS
(1,1), (1,3), (1,12),
(2,1), (2,2), (2,10),
(3,1), (3,2), (3,9),
(4,1), (4,3), (4,13),
(5,1), (5,3), (5,8),
(6,1), (6,2), (6,5), (6,7), (6,11),
(7,1), (7,2), (7,3), (7,4),
(8,1), (8,10), (8,11),
(9,1), (9,3), (9,5), (9,11),
(10,1), (10,3), (10,12),
(11,6), (11,8), (11,11), (11,13), (11,14),
(12,1), (12,3), (12,10), (12,11),
(13,1), (13,3), (13,10), (13,11),
(14,1), (14,3), (14,12),
(15,1), (15,7), (15,11), (15,14),
(16,1), (16,2), (16,3), (16,12),
(17,1), (17,9),
(18,8), (18,13),
(19,8), (19,14),
(20,1), (20,9),
(21,1), (21,3), (21,9), (21,13),
(22,1), (22,2), (22,4), (22,10),
(23,13),
(24,3), (24,12),
(25,1),

-- PS4
(26,1), (26,3),
(27,1), (27,3), (27,13),
(28,1), (28,3), (28,13),
(29,1), (29,3), (29,13),
(30,1), (30,3), (30,12), (30,13),
(31,1), (31,3), (31,12),
(32,1), (32,3), (32,12), (32,13),
(33,5), (33,7),

-- PC
(34,1), (34,3), (34,9),
(35,1), (35,3), (35,9),
(36,1), (36,8), (36,9), (36,13),
(37,1), (37,9), (37,11),
(38,6), (38,8), (38,9), (38,12),
(39,8),
(40,1), (40,2), (40,9),
(41,8),

-- NINTENDO SWITCH
(42,1), (42,3), (42,13),
(43,1), (43,10), (43,11),
(44,3), (44,12),
(45,3), (45,12),
(46,1), (46,3), (46,13),
(47,3), (47,11),
(48,1), (48,3), (48,13),
(49,1), (49,3), (49,13),

-- PC
(50,1), (50,3), (50,6), (50,9), (50,13),
(51,1), (51,3), (51,9), (51,11),
(52,2), (52,6),
(53,1), (53,3), (53,9), (53,11),
(54,5),
(55,3), (55,12),
(56,1), (56,3), (56,9),
(57,1), (57,3), (57,9)

;

/* N:M VIDEOJUEGO-IDIOMA*/
INSERT INTO VIDEOJUEGO_IDIOMA VALUES 
(1,1), (1,2), (1,3), (1,4), (1,5), (1,6),
(2,1), (2,2), (2,3), (2,4), (2,5), (2,7),
(3,1), (3,2), (3,3), (3,4), (3,5), (3,8),
(4,1), (4,2), (4,3), (4,4), (4,5), (4,9),
(5,1), (5,2), (5,3), (5,4), (5,5), (5,10),
(6,1), (6,2), (6,3), (6,4), (6,5), (6,11),
(7,1), (7,2), (7,3), (7,4), (7,5), (7,12),
(8,1), (8,2), (8,3), (8,4), (8,5), (8,13),
(9,1), (9,2), (9,3), (9,4), (9,5), (9,13),
(10,1), (10,2), (10,3), (10,4), (10,5), (10,6),
(11,1), (11,2), (11,3), (11,4), (11,5), (11,7),
(12,1), (12,2), (12,3), (12,4), (12,5), (12,8),
(13,1), (13,2), (13,3), (13,4), (13,5), (13,9),
(14,1), (14,2), (14,3), (14,4), (14,5), (14,10),
(15,1), (15,2), (15,3), (15,4), (15,5), (15,11),
(16,1), (16,2), (16,3), (16,4), (16,5), (16,12),
(17,1), (17,2), (17,3), (17,4), (17,5), (17,13),
(18,1), (18,2), (18,3), (18,4), (18,5), (18,13),
(19,1), (19,2), (19,3), (19,4), (19,5), (19,6),
(20,1), (20,2), (20,3), (20,4), (20,5), (20,7),
(21,1), (21,2), (21,3), (21,4), (21,5), (21,8),
(22,1), (22,2), (22,3), (22,4), (22,5), (22,9),
(23,1), (23,2), (23,3), (23,4), (23,5), (23,10),
(24,1), (24,2), (24,3), (24,4), (24,5), (24,11),
(25,1), (25,2), (25,3), (25,4), (25,5), (25,12),
(26,1), (26,2), (26,3), (26,4), (26,5), (26,13),
(27,1), (27,2), (27,3), (27,4), (27,5), (27,13),
(28,1), (28,2), (28,3), (28,4), (28,5), (28,6),
(29,1), (29,2), (29,3), (29,4), (29,5), (29,7),
(30,1), (30,2), (30,3), (30,4), (30,5), (30,8),
(31,1), (31,2), (31,3), (31,4), (31,5), (31,9),
(32,1), (32,2), (32,3), (32,4), (32,5), (32,10),
(33,1), (33,2), (33,3), (33,4), (33,5), (33,11),
(34,1), (34,2), (34,3), (34,4), (34,5), (34,12),
(35,1), (35,2), (35,3), (35,4), (35,5), (35,13),
(36,1), (36,2), (36,3), (36,4), (36,5), (36,13),
(37,1), (37,2), (37,3), (37,4), (37,5), (37,6),
(38,1), (38,2), (38,3), (38,4), (38,5), (38,7),
(39,1), (39,2), (39,3), (39,4), (39,5), (39,8),
(40,1), (40,2), (40,3), (40,4), (40,5), (40,9),
(41,1), (41,2), (41,3), (41,4), (41,5), (41,10),
(42,1), (42,2), (42,3), (42,4), (42,5), (42,11),
(43,1), (43,2), (43,3), (43,4), (43,5), (43,12),
(44,1), (44,2), (44,3), (44,4), (44,5), (44,13),
(45,1), (45,2), (45,3), (45,4), (45,5), (45,13),
(46,1), (46,2), (46,3), (46,4), (46,5), (46,6),
(47,1), (47,2), (47,3), (47,4), (47,5), (47,7),
(48,1), (48,2), (48,3), (48,4), (48,5), (48,8),
(49,1), (49,2), (49,3), (49,4), (49,5), (49,9),
(50,1), (50,2), (50,3), (50,4), (50,5), (50,10),
(51,1), (51,2), (51,3), (51,4), (51,5), (51,11),
(52,1), (52,2), (52,3), (52,4), (52,5), (52,12),
(53,1), (53,2), (53,3), (53,4), (53,5), (53,13),
(54,1), (54,2), (54,3), (54,4), (54,5), (54,13),
(55,1), (55,2), (55,3), (55,4), (55,5), (55,6),
(56,1), (56,2), (56,3), (56,4), (56,5), (56,7),
(57,1), (57,2), (57,3), (57,4), (57,5), (57,8)
;
