<?php
    require_once("../modelo/Conexion.php");
    // Búsqueda en el servidor de el título de un videojuego
    if(isset($_GET['busqueda']) && (trim($_GET['busqueda'])!="")) {
        $cadena = trim($_GET['busqueda']);

        $sql = "SELECT id,titulo FROM VIDEOJUEGO WHERE titulo like '%".$cadena."%'";

        $resultado = Conexion::consulta($sql);

        $coincidencias = [];

        if($resultado) {
            while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                array_push($coincidencias,  $row);
            }
            // sleep(1);

            echo json_encode($coincidencias,JSON_UNESCAPED_UNICODE);
        } else {
            echo "Sin resultados";
        }
    }
?>