<?php
    require_once("../modelo/Usuario.php");
    session_start();

    require_once("../modelo/Conexion.php");
    require_once("../modelo/Videojuego.php");

    if(!Usuario::usuarioLogeado()) {
       echo "<script>window.location.href = './articulos.php'</script>";
    }

    if(isset($_POST['newPassword']) && isset($_POST['oldPassword'])) {
        $newPassword = trim($_POST['newPassword']);
        $oldPassword = trim($_POST['oldPassword']);

        if($newPassword=="" || $oldPassword == "") {
            echo json_encode("Rellena todos los campos.");
        } else if(!preg_match('/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._+:;=#?!@$%^&*-]).{8,}$/',$newPassword) || !preg_match('/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._+:;=#?!@$%^&*-]).{8,}$/',$oldPassword)) {
            echo  json_encode("Valores no aceptados por el servidor, revisa las instrucciones.");
        
        } else {
            if(isset($_SESSION['usuario'])) {

                $_SESSION['usuario']->password=$newPassword;
                if(!$_SESSION['usuario']->administrador) {
                    $_SESSION['usuario']->administrador=0;
                } else {
                    $_SESSION['usuario']->administrador=1;
                }
                Conexion::modificarUsuario($_SESSION['usuario']);
                echo json_encode("Contraseña modificada.");
            } else {
                echo json_encode("Ha habido un problema con la sesión. Inténtalo de nuevo.");
            }
            
        }

    } else {
        include_once("../vista/cambiarContrasena.php");
    }

   
?>

