<?php
    require_once("../modelo/Usuario.php");
    session_start();

    require_once("../modelo/Conexion.php");

    // Recogida de datos del usuario
    if(isset($_POST['usuario']) && isset($_POST['password']) && isset($_POST['password2'])) {
        $usuario = trim($_POST['usuario']);
        $password = trim($_POST['password']);
        $password2 = trim($_POST['password2']);

        if($usuario == "" || $password == "" || $password2 == "") {
            echo json_encode("Rellena todos los campos.");
        } else if ($password != $password2) {
            echo json_encode("Las contraseñas no coinciden.");
        } else if (preg_match('/^[A-Za-z0-9]{1,8}$/',$usuario)==false || preg_match('/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._+:;=#?!@$%^&*-]).{8,}$/',$password)==false || preg_match('/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._+:;=#?!@$%^&*-]).{8,}$/',$password2)==false){
            echo json_encode("Datos incorrectos. Revisa las instrucciones.");

        } else {
            $user = Usuario::nuevoUsuario($usuario, $password);

            if(Conexion::existeUsuario($user)) {
                echo json_encode("El usuario ya existe.");

            } else {
                Conexion::crearUsuario($user);
                $_SESSION['usuario'] = $user;
                echo json_encode("Usuario creado.");
            }
        }
    } else {
        // Cargar vista
        include_once("../vista/registro.php");
    }
?>