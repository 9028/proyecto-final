<?php
    require_once("../modelo/Usuario.php");
    session_start();

    require_once("../modelo/Conexion.php");
    require_once("../modelo/Videojuego.php");


 
    // $videojuegos = Conexion::obtenerUltimosVideojuegos(8);
    $generos = Conexion::obtenerGeneros();
    $plataformas = Conexion::obtenerPlataformas();

    // Cuando se entra a través de un banner de publicidad, preparo la consulta para que al usuario le salga la búsqueda al entrar
    if(isset($_GET['genero'])) {
        $genero = trim($_GET['genero']);

        
        $sql = "SELECT * FROM VIDEOJUEGO WHERE id = ANY(SELECT c.id_videojuego FROM CLAVE c JOIN VIDEOJUEGO_GENERO vg on c.id_videojuego=vg.id_videojuego WHERE vg.id_genero=$genero)";

        $videojuegos = Conexion::obtenerCiertosVideojuegos($sql);
    }else if(isset($_GET['busqueda'])) {
        $busqueda = trim($_GET['busqueda']);

        if($busqueda!="") {
            $sql = "SELECT * FROM VIDEOJUEGO WHERE titulo LIKE '%$busqueda%'";

            $videojuegos = Conexion::obtenerCiertosVideojuegos($sql);


        }
    } else if (isset($_GET['plataforma'])) {
        $plataforma = $_GET['plataforma'];
        $sql = "SELECT * FROM VIDEOJUEGO WHERE id = ANY(SELECT c.id_videojuego FROM CLAVE c WHERE c.id_plataforma=$plataforma)";

        $videojuegos = Conexion::obtenerCiertosVideojuegos($sql);
    }

    
    include_once("../vista/filtros.php");
    
    
?>
