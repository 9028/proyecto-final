<?php
    require_once("../modelo/Carrito.php");
    session_start();
    require_once("../modelo/Usuario.php");
    require_once("../modelo/Conexion.php");
    require_once("../modelo/Clave.php");


    function subir_fichero($directorio_destino, $nombre_fichero, $post, $captura=false) {
        $tmp_name = $_FILES[$post]['tmp_name'];
        
        if(!is_dir($directorio_destino)) {
            mkdir($directorio_destino,0744);
        }
        //si hemos enviado un directorio que existe realmente y hemos subido el archivo    
        if (is_dir($directorio_destino) && is_uploaded_file($tmp_name))
            {
                $img_file = $_FILES[$post]['name'];
                $img_type = $_FILES[$post]['type'];
                

                // Si se trata de una imagen   
                if ((strpos($img_type, "jpg") || strpos($img_type, "jpeg")))
                {
                    //¿Tenemos permisos para subir la imágen?
                    if(!$captura) {
                    if (move_uploaded_file($tmp_name, $directorio_destino . '/' . $nombre_fichero .'.jpg'))
                        {
                            return true;
                        }
                    } else {
                        if (move_uploaded_file($tmp_name, $directorio_destino . '/' . $captura .'.jpg'))
                        {
                            return true;
                        }
                    }

                }
            }
            //Si llegamos hasta aquí es que algo ha fallado
            return false;
        }

        function anadirIdiomas($idVideojuego) {
            $idiomas = [];
            if(isset($_POST['idiomaEspanol'])) {
                array_push($idiomas,$_POST['idiomaEspanol']);
            }

            if(isset($_POST['idiomaIngles'])) {
                array_push($idiomas,$_POST['idiomaIngles']);
            }

            if(isset($_POST['idiomaFrances'])) {
                array_push($idiomas,$_POST['idiomaFrances']);
            }

            if(isset($_POST['idiomaItaliano'])) {
                array_push($idiomas,$_POST['idiomaItaliano']);
            }

            if(isset($_POST['idiomaAleman'])) {
                array_push($idiomas,$_POST['idiomaAleman']);
            }

            if(isset($_POST['idiomaCheco'])) {
                array_push($idiomas,$_POST['idiomaCheco']);
            }

            if(isset($_POST['idiomaHolandes'])) {
                array_push($idiomas,$_POST['idiomaHolandes']);
            }

            if(isset($_POST['idiomaJapones'])) {
                array_push($idiomas,$_POST['idiomaJapones']);
            }
            
            if(isset($_POST['idiomaCoreano'])) {
                array_push($idiomas,$_POST['idiomaCoreano']);
            }
            
            if(isset($_POST['idiomaPolaco'])) {
                array_push($idiomas,$_POST['idiomaPolaco']);
            }

            if(isset($_POST['idiomaPortugues'])) {
                array_push($idiomas,$_POST['idiomaPortugues']);
            }

            if(isset($_POST['idiomaRuso'])) {
                array_push($idiomas,$_POST['idiomaRuso']);
            }

            if(isset($_POST['idiomaChino'])) {
                array_push($idiomas,$_POST['idiomaChino']);
            }

            foreach($idiomas as $idioma) {
                Conexion::consulta("INSERT INTO VIDEOJUEGO_IDIOMA VALUES ($idVideojuego, $idioma)");
            }

        }

        function anadirGeneros($idVideojuego) {
            $generos = [];
            if(isset($_POST['generoAccion'])) {
                array_push($generos,$_POST['generoAccion']);
            }

            if(isset($_POST['generoArcade'])) {
                array_push($generos,$_POST['generoArcade']);
            }

            if(isset($_POST['generoAventura'])) {
                array_push($generos,$_POST['generoAventura']);
            }

            if(isset($_POST['generoBeatemall'])) {
                array_push($generos,$_POST['generoBeatemall']);
            }

            if(isset($_POST['generoCarreras'])) {
                array_push($generos,$_POST['generoCarreras']);
            }

            if(isset($_POST['generoCooperacion'])) {
                array_push($generos,$_POST['generoCooperacion']);
            }

            if(isset($_POST['generoDeporte'])) {
                array_push($generos,$_POST['generoDeporte']);
            }

            if(isset($_POST['generoEstrategia'])) {
                array_push($generos,$_POST['generoEstrategia']);
            }

            if(isset($_POST['generoFPS'])) {
                array_push($generos,$_POST['generoFPS']);
            }

            if(isset($_POST['generoLucha'])) {
                array_push($generos,$_POST['generoLucha']);
            }

            if(isset($_POST['generoMultijugador'])) {
                array_push($generos,$_POST['generoMultijugador']);
            }

            if(isset($_POST['generoPlataformas'])) {
                array_push($generos,$_POST['generoPlataformas']);
            }

            if(isset($_POST['generoRPG'])) {
                array_push($generos,$_POST['generoRPG']);
            }
            
            if(isset($_POST['generoSimulacion'])) {
                array_push($generos,$_POST['generoSimulacion']);
            }

            foreach($generos as $genero) {
                Conexion::consulta("INSERT INTO VIDEOJUEGO_GENERO VALUES ($idVideojuego, $genero)");
            }

        }


    // =========== OPERACIONES CPANEL  ===========
    if(isset($_POST['operacion'])) {
        $operacion = $_POST['operacion'];

        // =========== USUARIO  ===========
        
        // AÑADIR USUARIO
        if($operacion == "anadirUsuario") {
            if(isset($_POST['nombreUsuario']) && isset($_POST['passwordUsuario']) && isset($_POST['administradorUsuario'])) {
                $usuario = trim($_POST['nombreUsuario']);
                $password = trim($_POST['passwordUsuario']);
                $administrador = trim($_POST['administradorUsuario']);

                if($usuario == "" || $password == "" || $administrador == "") {
                    echo json_encode("Rellena todos los campos.");
                } else if (!preg_match('/^[A-Za-z0-9]{1,8}$/', $usuario) || !preg_match('/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._+:;=#?!@$%^&*-]).{8,}$/', $password) || !preg_match('/^[0-1]$/', $administrador)) {
                    echo json_encode("Los valores enviados no cumplen con los requisitos. Revísalos.");
                
                } else {

                    $user = Usuario::nuevoUsuario($usuario, $password, $administrador);

                    if(Conexion::existeUsuario($user)) {
                        echo json_encode("El usuario ya existe.");

                    } else {
                        Conexion::crearUsuario($user);
                        echo json_encode("Usuario creado.");
                    }
                }
            } else {
                echo json_encode("Error");
            }
        
        // MODIFICAR USUARIO
        } else if ($operacion  =="modificarUsuario") {
            if(isset($_POST['nombreUsuario']) && isset($_POST['passwordUsuario']) && isset($_POST['administradorUsuario'])) {
                $usuario = trim($_POST['nombreUsuario']);
                $password = trim($_POST['passwordUsuario']);
                $administrador = trim($_POST['administradorUsuario']);

                if($usuario == "" || $password == "" || $administrador == "") {
                    echo json_encode("Rellena todos los campos.");
                } else if (!preg_match('/^[A-Za-z0-9]{1,8}$/', $usuario) || !preg_match('/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[._+:;=#?!@$%^&*-]).{8,}$/', $password) || !preg_match('/^[0-1]$/', $administrador)) {
                    echo json_encode("Los valores enviados no cumplen con los requisitos. Revísalos.");
                } else {
                    $user = Usuario::nuevoUsuario($usuario, $password, $administrador);

                    if(Conexion::existeUsuario($user)) {

                        Conexion::modificarUsuario($user);
                        echo json_encode("El usuario ha sido modificado.");
                    } else {
                        echo json_encode("El usuario no existe.");
                    }
                }
            }

        // ELIMINAR USUARIO
        } else if ($operacion == "eliminarUsuario") {
            if(isset($_POST['nombreUsuario'])) {
                $nombreUsuario = trim($_POST['nombreUsuario']);
                if($nombreUsuario=="") {
                    echo json_encode("Rellena todos los campos");
                } else if(!preg_match('/^[A-Za-z0-9]{1,8}$/', $nombreUsuario)) {
                    echo json_encode("Los valores enviados no cumplen con los requisitos. Revísalos.");
                } else {
                    $usuarioPrueba = Usuario::nuevoUsuario($nombreUsuario, "test", "0");
                    if(Conexion::existeUsuario($usuarioPrueba)) {
                        if(Conexion::obtenerNumeroClavesUsuario($nombreUsuario)>0) {
                            echo json_encode("No se pudo eliminar la cuenta, ya que tiene <strong>claves asociadas</strong>.");
                        } else {
                            Conexion::eliminarUsuario($usuarioPrueba);
                             echo json_encode("Usuario eliminado.");
                        }

                    } else {
                        echo json_encode("No existe este usuario.");
                    }
                }

            } else {
                echo json_encode("Error.");
            }

        // =========== VIDEOJUEGO  ===========

        // Añadir videojuego
        } else if($operacion == "anadirVideojuego") {
            if(isset($_POST['tituloVideojuego']) && isset($_POST['precioVideojuego']) && isset($_POST['rebajaVideojuego']) && isset($_POST['pegiVideojuego']) && isset($_POST['fechaVideojuego']) && isset($_POST['trailerVideojuego']) && isset($_POST['descripcionVideojuego']) && isset($_FILES['portadaVideojuego'])) {
                $titulo = trim($_POST['tituloVideojuego']);
                $precio = trim($_POST['precioVideojuego']);
                $rebaja = trim($_POST['rebajaVideojuego']);
                $edad_recomendada = trim($_POST['pegiVideojuego']);
                $fecha = trim($_POST['fechaVideojuego']);
                $trailer = trim($_POST['trailerVideojuego']);
                $descripcion = trim($_POST['descripcionVideojuego']);
                $id = Conexion::obtenerUltimoIdVideojuegos()+1;
                

                if ($titulo=="" || $precio == "" || $rebaja == "" || $edad_recomendada == "" || $fecha == "" || $trailer == "" || $descripcion == "") {
                    echo json_encode("Rellena todos los campos.");
                } else if(!preg_match('/^[0-9]+$/',$precio) || !preg_match('/^[0-9]+$/',$rebaja) || !preg_match('/^[0-9]+$/',$edad_recomendada) || !preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',$fecha) ) {
                   echo json_encode("Rellena todos los campos.");
                } else if (!subir_fichero("../vista/img/portadas/",$id,"portadaVideojuego")) {
                    echo json_encode("Error al subir la imagen");
                } else if (!subir_fichero("../vista/img/screenshots/$id",$id,"screenshotVideojuego","1")) {
                    echo json_encode("Error al subir la captura 1");
                }else if (!subir_fichero("../vista/img/screenshots/$id",$id,"screenshot2Videojuego","2")) {
                     echo json_encode("Error al subir la captura 2"); 
                } else {
                    $videojuego = Videojuego::nuevoVideojuego(0, $titulo, $descripcion, $precio, $edad_recomendada, $fecha, $trailer, $rebaja);
                    if(Conexion::existeVideojuego($videojuego)) {
                        echo json_encode("El videojuego ya existe.");
                    } else {
                        Conexion::anadirVideojuego($videojuego);

                        // Actualizo otras tablas que conectan con el videojuego
                        anadirIdiomas($id);
                        anadirGeneros($id);

                        echo json_encode("Videojuego añadido.");
                    }
                }


                
            }
        // Modificar precio videojuego
        } else if($operacion == "modificarPrecioVideojuego") {
            if(isset($_POST['idVideojuego']) && isset($_POST['precioVideojuego']) && isset($_POST['descuentoVideojuego'])) {
                $id = trim($_POST['idVideojuego']);
                $precio = trim($_POST['precioVideojuego']);
                $descuento = trim($_POST['descuentoVideojuego']);

                if($id == "" || $precio == "" || $descuento == "") {
                    echo json_encode("Rellena todos los campos");
                } else {
                    $videojuego = Conexion::obtenerVideojuego($id);
                    Conexion::modificarPrecioVideojuego($videojuego, $precio, $descuento);
                    echo json_encode("Precio modificado.");
                }
            }

        // =========== CLAVES  ===========

        // Añadir clave
        } else if($operacion == "anadirClave") {
            if(isset($_POST['plataformaClave']) && isset($_POST['claveClave']) && isset($_POST['videojuegoClave'])) {
                $plataforma = trim($_POST['plataformaClave']);
                $clave = trim($_POST['claveClave']);
                $videojuego = trim($_POST['videojuegoClave']);

                if($plataforma == "" || $clave == "" || $videojuego == "") {
                    echo json_encode("Rellena todos los campos");
                } else if (!preg_match('/^([A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5})|([A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5})|([A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4})|([A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4})$/',$clave)) {
                    echo json_encode("Clave con formato invalido");
                
                } else {
                    $claveActual = Clave::nuevaClave($clave, false, $plataforma, $videojuego);
                    if(Conexion::claveRepetida($claveActual)) {
                        echo  json_encode("La clave ya existe.");
                    }else {
                        Conexion::anadirClave($claveActual);
                        echo json_encode("Clave añadida.");
                    }

                }
            }

        // =========== CARRITO  ===========

        // Añadir  a carrito
        } else if ($operacion == "anadirVideojuegoCarrito") {
            if(isset($_POST['idVideojuego']) && Usuario::usuarioLogeado()) {
                if(isset($_POST['opcionPlataforma'])) {
                    $idVideojuego = $_POST['idVideojuego'];
                    $idPlataforma = $_POST['opcionPlataforma'];

                    $carrito = Carrito::cargarCarrito();
                    $clavesDisponibles = Conexion::obtenerNumeroClavesVideojuego($idVideojuego, $idPlataforma);
                    $clavesCarrito  = $carrito->obtenerCantidadClaves($idVideojuego, $idPlataforma);

                    if($clavesCarrito < $clavesDisponibles ) {
                        $carrito->anadirProducto($idVideojuego, $idPlataforma);
                        echo json_encode("Videojuego añadido al carrito.");

                    } else {
                        echo json_encode("No hay mas claves para esta plataforma.");
                    }

                } else {
                    echo json_encode("Selecciona una plataforma.");
                }
            } else {
                echo json_encode("Logueate antes de comprar.");
            }

        // =========== PAGAR (TARJETA)  ===========
        } else if ($operacion == "pagar") {
            if(isset($_POST['numeroTarjeta']) && isset($_POST['fechaExpiracionMes']) && isset($_POST['fechaExpiracionAnio']) && isset($_POST['cv']) && isset($_POST['nombre']) && isset($_POST['apellidos']) && isset($_POST['direccionFacturacion']) && isset($_POST['localidad']) && isset($_POST['pais']) && isset($_POST['cp'])) {
                $numeroTarjeta = trim($_POST['numeroTarjeta']);
                $fechaExpiracionMes = trim($_POST['fechaExpiracionMes']);
                $fechaExpiracionAnio = trim($_POST['fechaExpiracionAnio']);
                $cv = trim($_POST['cv']);
                $nombre = trim($_POST['nombre']);
                $apellidos = trim($_POST['apellidos']);
                $direccionFacturacion = trim($_POST['direccionFacturacion']);
                $localidad = trim($_POST['localidad']);
                $pais = trim($_POST['pais']);
                $cp = trim($_POST['cp']);

                $inputsFormulario = [];
                array_push($inputsFormulario, $numeroTarjeta, $fechaExpiracionMes, $fechaExpiracionAnio, $cv, $nombre, $apellidos, $direccionFacturacion, $localidad, $pais, $cp);

                foreach($inputsFormulario as $input) {
                    if($input == "") {
                        echo json_encode("Rellena todos los campos.");
                        exit();
                    }
                }

                $anioActual = date("Y");
                $mesActual = date("m");

                if($fechaExpiracionAnio<$anioActual || $fechaExpiracionMes < $mesActual) {
                    echo json_encode("La tarjeta está caducada");
                    exit();
                }

                $carrito = Carrito::cargarCarrito();
                $usuario = Usuario::obtenerNombreUsuario();
                
                // VIDEOJUEGOS
                foreach($carrito->obtenerProductos() as $producto) {
                    $videojuego = $producto['videojuego'];
                    $plataformas = $producto['plataformas'];
                    
                    // PLATAFORMAS
                    foreach($plataformas as $idPlataforma=>$plataforma) {
                        $cantidadClavesPedida = intval($plataforma['cantidad']);
                        $cantidadClavesDisp = Conexion::obtenerNumeroClavesVideojuego($videojuego->id, $idPlataforma);

                        // Si el usuario pide 3 claves y solo hay 2, no se hará la operación.
                        if($cantidadClavesPedida <= $cantidadClavesDisp) {
                            $claves = Conexion::obtenerClavesDisponibles($videojuego->id, $idPlataforma);
                            
                            foreach($claves as $clave) {
                                Conexion::asignarClave($clave, $usuario);
                            }

                        } else {
                            echo  json_encode("Error. No hay suficientes claves disponibles.");
                            exit();
                        }
                    }
                }
                $carrito->vaciarCarrito();
                echo json_encode("Compra realizada.");

                // =========== PAGAR (PAYPAL)  ===========
            } else if(isset($_POST['email']) && isset($_POST['password']) && isset($_POST['nombre']) && isset($_POST['apellidos']) && isset($_POST['direccionFacturacion']) && isset($_POST['localidad']) && isset($_POST['pais']) && isset($_POST['cp'])) {
                $email = trim($_POST['email']);
                $password = trim($_POST['password']);
                $nombre = trim($_POST['nombre']);
                $apellidos = trim($_POST['apellidos']);
                $direccionFacturacion = trim($_POST['direccionFacturacion']);
                $localidad = trim($_POST['localidad']);
                $pais = trim($_POST['pais']);
                $cp = trim($_POST['cp']);

                $inputsFormulario = [];
                array_push($inputsFormulario, $email, $password, $nombre, $apellidos, $direccionFacturacion, $localidad, $pais, $cp);

                foreach($inputsFormulario as $input) {
                    if($input == "") {
                        echo json_encode("Rellena todos los campos.");
                        exit();
                    }
                }

                $carrito = Carrito::cargarCarrito();
                $usuario = Usuario::obtenerNombreUsuario();

                // VIDEOJUEGOS
                foreach($carrito->obtenerProductos() as $producto) {
                    $videojuego = $producto['videojuego'];
                    $plataformas = $producto['plataformas'];
                    
                    // PLATAFORMAS
                    foreach($plataformas as $idPlataforma=>$plataforma) {
                        $cantidadClavesPedida = intval($plataforma['cantidad']);
                        $cantidadClavesDisp = Conexion::obtenerNumeroClavesVideojuego($videojuego->id, $idPlataforma);

                        if($cantidadClavesPedida <= $cantidadClavesDisp) {
                            $claves = Conexion::obtenerClavesDisponibles($videojuego->id, $idPlataforma);
                            
                            foreach($claves as $clave) {
                                Conexion::asignarClave($clave, $usuario);
                            }

                        } else {
                            echo  json_encode("Error. No hay suficientes claves disponibles.");
                            exit();
                        }
                    }
                }

                $carrito = Carrito::cargarCarrito();
                $carrito->vaciarCarrito();

                echo json_encode("Compra realizada.");
                exit();
            } else {
                echo json_encode("Rellena todos los campos final");
            }

        // =========== FILTROS  ===========
        } else if ($operacion == "busquedaFiltros") {
            $response = "";

            $sql = "SELECT * FROM VIDEOJUEGO WHERE id = ANY(SELECT c.id_videojuego FROM CLAVE c";
            if(isset($_POST['genero'])) {
                $idGenero = $_POST['genero'];
               $sql .= " JOIN VIDEOJUEGO_GENERO vg on c.id_videojuego=vg.id_videojuego WHERE vg.id_genero=$idGenero";
            }


            if(isset($_POST['plataforma'])) {
                $idPlataforma = $_POST['plataforma'];
                if($idPlataforma!="todas") {
                    if(strpos($sql, "JOIN")) {
                        $sql .= " AND c.id_plataforma=$idPlataforma";
                    }  else {
                        $sql .= " WHERE c.id_plataforma=$idPlataforma";
                    }
                    
                }
            }

            $sql .= " )";

            if(isset($_POST['precio'])) {
                $tipoPrecio = $_POST['precio'];
                if($tipoPrecio=="mayor") {
                    $sql .= " ORDER BY precio-((precio*rebaja)/100) DESC";
                } else {
                    $sql .= " ORDER BY precio-((precio*rebaja)/100) ASC";
                }
            }

            $resultado = Conexion::consulta($sql);
            $videojuegos = [];

             if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($videojuegos, new Videojuego($row));
                }
            } else {
                echo "No hay resultados";
                exit;
            }

            // Devuelvo los videojuegos filtrados
            foreach($videojuegos as $videojuego) {
                $idVideojuego = $videojuego->id;
                $portadaVideojuego = $videojuego->obtenerPortada();
                $precioVideojuego = $videojuego->obtenerPrecioActual()."€";
                $tituloVideojuego = $videojuego->titulo;
                $response .= "<div class='tarjeta'>";
                    $response .= "<a href='./videojuego.php?id=$idVideojuego'>$portadaVideojuego</a>";
                    $response .= "<div class='precioTarjeta'>$precioVideojuego</div>";
                    $response .= "<div class='titulo'>$tituloVideojuego</div>";
                $response .=  "</div>";
            }

            echo $response;
        }

    }
?>