<?php
    require_once("../modelo/Usuario.php");
    session_start();

    require_once("../modelo/Conexion.php");
    require_once("../modelo/Videojuego.php");

    if(!Usuario::usuarioLogeado()) {
       echo "<script>window.location.href = './articulos.php'</script>";
    }

    include_once("../vista/configuracion.php");
?>

