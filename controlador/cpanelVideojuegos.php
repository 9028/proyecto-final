<?php
    require_once("../modelo/Usuario.php");
    session_start();

    require_once("../modelo/Conexion.php");
    require_once("../modelo/Videojuego.php");

    if(!Usuario::usuarioLogeado() || !Conexion::usuarioAdministrador($_SESSION['usuario'])) {
        echo "<script>window.location.href = './articulos.php'</script>";
    }

    
    // Muestro vista en caso de que no haya POST

        $videojuegos = Conexion::obtenerVideojuegos();

        include_once("../vista/cpanelVideojuegos.php");
    
?>

