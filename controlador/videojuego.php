<?php
    require_once("../modelo/Usuario.php");
    require_once("../modelo/Carrito.php");
    session_start();
    require_once("../modelo/Conexion.php");
    require_once("../modelo/Videojuego.php");

    if(!isset($_GET['id']) || $_GET['id']=="") {
        ?>
        <script>window.location.href = "./articulos.php";</script>
        <?php
        exit();
    }

    // Crearlo o recuperarlo
    $carrito = Carrito::cargarCarrito();

    $videojuegoActual = Conexion::obtenerVideojuego($_GET['id']);
   $idVideojuego = $videojuegoActual->id;

    // Sus plataformas
    $plataformas = Conexion::obtenerPlataformas();
    $plataformasDisponibles = [];
    foreach($plataformas as $plataforma) {
        $idPlataforma = $plataforma['id'];
        $nombrePlataforma = $plataforma['plataforma'];
        if(Conexion::juegosDisponiblesPlataforma($videojuegoActual,$idPlataforma)) {
            array_push($plataformasDisponibles,["idPlataforma" => $idPlataforma, "nombrePlataforma" => $nombrePlataforma]);
        }
    }

    // Sus idiomas
    $idiomas = Conexion::obtenerIdiomasVideojuego($videojuegoActual);

    // Sus géneros
    $generos = Conexion::obtenerGenerosVideojuego($videojuegoActual);

    // Videojuegos más vendidos
    $videojuegosMasVendidos = [];
    for($i = 1; $i<=4; $i++) {
        array_push($videojuegosMasVendidos,Conexion::obtenerVideojuego($i));
    }

    include_once("../vista/videojuego.php");
?>

