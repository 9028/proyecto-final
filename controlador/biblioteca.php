<?php
    require_once("../modelo/Usuario.php");
    session_start();

    require_once("../modelo/Conexion.php");
    require_once("../modelo/Videojuego.php");

    if(!Usuario::usuarioLogeado()) {
        echo "<script>window.location.href = './articulos.php'</script>";
        exit();
    }
    
    $usuario = Usuario::obtenerNombreUsuario();

    // Petición al servidor donde consigo todas las claves asociadas a un juego que posee el usuario
    if(isset($_POST['idVideojuego'])) {
        
        $claves = Conexion::obtenerMisClavesVideojuego($_POST['idVideojuego'],$usuario);
        $response = "<table class='table table-modal'>";
        foreach($claves as  $clave) {
            $codigo = $clave->clave;
            $plataforma = Videojuego::obtenerLogoPlataforma($clave->id_plataforma);
            $response .= "<tr class='table-info'>";
                $response .= "<td>$plataforma</td><td>$codigo</td>";
            $response .= "</tr>";
        }
        $response .= "</table>";

        echo $response;
        exit;


    } else {
       
        $misVideojuegos = Conexion::obtenerMisVideojuegos($usuario);
    
        // Muestro vista en caso de que no haya POST

        include_once("../vista/biblioteca.php");
    }


    
?>
