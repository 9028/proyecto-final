<?php
    require_once("../modelo/Usuario.php");
    require_once("../modelo/Carrito.php");
    session_start();

    require_once("../modelo/Conexion.php");
    require_once("../modelo/Videojuego.php");

    $carrito = Carrito::cargarCarrito();

    if(!Usuario::usuarioLogeado() || $carrito->estaVacia()) {
        echo "<script>window.location.href = './articulos.php'</script>";
    }
    
    // Muestro vista en caso de que no haya POST

    include_once("../vista/pagar.php");
    
?>
