<?php
    require_once("../modelo/Usuario.php");
    require_once("../modelo/Carrito.php");
    session_start();
    require_once("../modelo/Conexion.php");
    require_once("../modelo/Videojuego.php");

    $carrito = Carrito::cargarCarrito();

    // Con esta variable controlo si se pueden añadir mas cantidad del mismo producto
    $disabled = "";

    if(isset($_POST['anadir'])) {
        $producto = $_POST['anadir'];
        $plataforma  = $_POST['plataforma'];

        $clavesDisponibles = Conexion::obtenerNumeroClavesVideojuego($producto, $plataforma);
        $clavesCarrito  = $carrito->obtenerCantidadClaves($producto, $plataforma);

        if($clavesCarrito < $clavesDisponibles ) {
            $carrito->anadirProducto($producto,$plataforma);
            $clavesCarrito  = $carrito->obtenerCantidadClaves($producto, $plataforma);
            if($clavesCarrito==$clavesDisponibles) {
                $disabled = "disabled";
            }
        } else {
            $disabled = "disabled";
        }
    } else if (isset($_POST['restar'])) {
        $producto = $_POST['restar'];
        $plataforma  = $_POST['plataforma'];
        $carrito->reducirProducto($producto,$plataforma);
    } else if (isset($_GET['v']) && isset($_GET['p'])) {
        $producto = $_GET['v'];
        $plataforma = $_GET['p'];

        // Controlo que haya tanto vidoejuego como plataforma
        if($producto!="" && $plataforma != "") {
            if(!($carrito->productoEnCarrito($producto, $plataforma))) {
                $carrito->anadirProducto($producto, $plataforma);
            }
        }


    }

    include_once("../vista/carrito.php");

?>