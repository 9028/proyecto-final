<?php
    session_start();
    require_once("../modelo/Usuario.php");
    require_once("../modelo/Conexion.php");
    

    // Consulta asíncrona
    $usuario = trim($_POST['usuario']);
    $password = trim($_POST['password']);

    if($usuario == "" || $password == "") {
        echo json_encode("Rellena todos los campos");
    } else {
        $user = Usuario::nuevoUsuario($usuario, $password);

        if(Conexion::loginCorrecto($user) && !Usuario::usuarioLogeado()) {
            $user->administrador=Conexion::usuarioAdministrador($user);
            $_SESSION['usuario'] = $user;
            // header("Location: articulos.php");
            echo json_encode("Login correcto");
        } else {
            echo json_encode("Usuario o contrase&nacute;a incorrectos");
        }
    }
    
?>

