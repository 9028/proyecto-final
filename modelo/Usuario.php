<?php

    class Usuario {
        protected $usuario;
        protected $password;
        protected $administrador;
        
        public function __construct($row) {
            $this->usuario=$row['usuario'];
            $this->password=$row['password'];
            $this->administrador=$row['administrador'];
        }

        public function __get($atributo) {
            return $this->$atributo;
        }

        public function __set($atributo,$valor) {
            return $this->$atributo=$valor;
        }

        // Constructor alternativo para crear usuarios sin necesidad de una array
        public static function nuevoUsuario($usuario, $password, $administrador=false) {
            return new Usuario([
                                   'usuario' => $usuario,
                                   'password' => $password,
                                   'administrador' => $administrador
                                ]);
        }


        // Comprueba si hay un usuario logeado actualmente
        public static function usuarioLogeado() {
            if(isset($_SESSION['usuario'])) {
                return true;
            }
            return false;
        }

        public static function cerrarSesion() {
            session_unset();
        }

        public static function sino($flag) {
            if($flag) {
                return "Sí";
            } 
            return  "No";
        }

        public static function obtenerNombreUsuario() {
            if(isset($_SESSION['usuario'])) {
                return $_SESSION['usuario']->usuario;
            }
        }
    }

?>