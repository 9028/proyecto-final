<?php

    class Clave {
        protected $clave;
        protected $recibida;
        protected $id_usuario;
        protected $id_plataforma;
        protected $id_videojuego;

        
        public function __construct($row) {
            $this->clave=$row['clave'];
            $this->recibida=$row['recibida'];
            $this->id_usuario=$row['id_usuario'];
            $this->id_plataforma=$row['id_plataforma'];
            $this->id_videojuego=$row['id_videojuego'];

        }

        public function __get($atributo) {
            return $this->$atributo;
        }

        public function __set($atributo,$valor) {
            return $this->$atributo=$valor;
        }

        // Constructor alternativo para crear usuarios sin necesidad de una array
        public static function nuevaClave($clave, $recibida, $id_plataforma, $id_videojuego, $id_usuario=null) {
            return new Clave([
                                   'clave' => $clave,
                                   'recibida' => $recibida,
                                   'id_plataforma' => $id_plataforma,
                                   'id_videojuego' => $id_videojuego,
                                   'id_usuario' => $id_usuario
                                ]);
        }

        public static function sino($flag) {
            if($flag) {
                return "Sí";
            } 
            return  "No";
        }

        public static function plataformaClave($valor) {
            if($valor ==  1) {
                return "PS4";
            } else if ($valor == 2) {
                return "PC";
            } else if ($valor == 3) {
                return "Nintendo Switch";
            } else if ($valor == 4) {
                return "XBOX One";
            }
        }

    }

?>