<?php
    require_once("Conexion.php");
    class Carrito {
        private $listaProductos=[];

        public function anadirProducto($id, $plataforma) {
            $p = Conexion::obtenerVideojuego($id);

            // Compruebo si el producto está en la lista
            if(isset($this->listaProductos[$id])) {
                // Compruebo si también la plataforma lo está
                if(isset(($this->listaProductos[$id]["plataformas"][$plataforma]))) {
                    // En ese caso, aumento la cantidad
                    $this->listaProductos[$id]["plataformas"][$plataforma]["cantidad"]++;

                } else {
                    // El juego está en el carrito pero no para la plataforma actual, se lo añado
                    $this->listaProductos[$id]["plataformas"][$plataforma] = ["cantidad" => 1];
                    
                }
            } else {
                // El juego no está en el carrito, lo añado
                $this->listaProductos[$id] = [                          
                                            "videojuego" => $p,
                                            "plataformas" => [
                                                                $plataforma => [
                                                                                "cantidad" => 1
                                                                                ]
                                                            ]
                                            ];
            }
        }

        public function obtenerProductos() {
            return $this->listaProductos;
        }

        public function eliminarProducto($id) {
            if (isset($this->listaProductos[$id])) {
                unset($this->listaProductos[$id]);
            }
        }

        public function estaVacia() {
            return count($this->listaProductos)==0;
        }

        public function reducirProducto($producto, $plataforma) {
            $this->listaProductos[$producto]["plataformas"][$plataforma]['cantidad']--;
            if($this->listaProductos[$producto]["plataformas"][$plataforma]['cantidad']==0) {
                unset($this->listaProductos[$producto]["plataformas"][$plataforma]);
            }

            if(count($this->listaProductos[$producto]["plataformas"])==0) {
                $this->eliminarProducto($producto);
            }
        }

        static public function cargarCarrito() {
            if (!isset($_SESSION['carrito'])) {
                $_SESSION['carrito'] = new Carrito();
            }
            return $_SESSION['carrito'];
        }

        public function obtenerTotalPrecio() {
            $resultado = 0;
            
            foreach($this->listaProductos as $producto) {
                $cantidad = 0;
                foreach($producto['plataformas'] as $plataforma) {
                    
                    $cantidad += $plataforma['cantidad'];
                }
                $resultado += ($producto['videojuego']->obtenerPrecioActual()) * $cantidad;
            }
            return $resultado;
        }

        public function vaciarCarrito() {
            if(isset($_SESSION['carrito'])) {
                unset($_SESSION['carrito']);
            }
            
        }

        public function productoEnCarrito($producto, $plataforma) {
            if(isset($this->listaProductos[$producto]["plataformas"][$plataforma])) {
                return true;
            }
            return false;
        }

        public function obtenerCantidadClaves($producto, $plataforma) {
            if($this->productoEnCarrito($producto, $plataforma)) {
                return intval($this->listaProductos[$producto]["plataformas"][$plataforma]["cantidad"]);
            } else {
                return 0;
            }
        }
    }
?>