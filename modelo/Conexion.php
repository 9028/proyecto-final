<?php
    require_once("Videojuego.php");
     require_once("Usuario.php");
     require_once("Clave.php");

    class Conexion {
        // HOSTING
        // static private $host = "db5002211234.hosting-data.io";
        // static private $usuario = "dbu148604";
        // static private $passwd = "AdrianHR8*";
        // static private $bd = "dbs1786095";
        
        // LOCAL
        static private $host = "localhost";
        static private $usuario = "getgames";
        static private $passwd = "getgames2021";
        static private $bd = "getgames";
        
        static function consulta($sql) {
            $host = self::$host;
            $usuario = self::$usuario;
            $passwd = self::$passwd;
            $bd = self::$bd;
            
            // Se crea una conexión por cada consulta
            try {
                $conexion = new PDO("mysql:host=$host;dbname=$bd;charset=utf8",$usuario,$passwd);
            } catch (Exception $e) {
                exit("Error: ".$e->getMessage());
            }

            // Distinguir si es SELECT u otra operación para PDO
            $operacion = strtoupper(explode(" ",trim($sql))[0]);

           if ($operacion == "SELECT") {
               $resultado = $conexion->query($sql);
           } else {
                $resultado = $conexion->exec($sql);
           }

           if (!$resultado) exit("Error ejecutando la consulta: $sql");

           return $resultado;
        }

        static function obtenerVideojuegos() {
            $sql = "SELECT * FROM VIDEOJUEGO";
            $resultado = self::consulta($sql);
            $videojuegos = [];

            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($videojuegos, new Videojuego($row));
                }
            }

            return $videojuegos;
        }

        static function obtenerVideojuego($id) {
            $sql = "SELECT * FROM VIDEOJUEGO WHERE id = '$id'";
            $resultado = self::consulta($sql);

            if($resultado) {
                $videojuego = $resultado->fetch(PDO::FETCH_ASSOC);
                return new Videojuego($videojuego);
            }

            return -1;
        }

        static function anadirVideojuego($videojuego) {
            $titulo = $videojuego->titulo;
            $descripcion = $videojuego->descripcion;
            $precio =  $videojuego->precio;
            $edad_recomendada = $videojuego->edad_recomendada;
            $fecha_lanzamiento = $videojuego->fecha_lanzamiento;
            $trailer = $videojuego->trailer;
            $rebaja = $videojuego->rebaja;

            $sql = "INSERT INTO VIDEOJUEGO
                    (titulo, descripcion, precio, rebaja, edad_recomendada, fecha_lanzamiento, trailer)
                    VALUES ('$titulo', '$descripcion', $precio, $rebaja, $edad_recomendada, '$fecha_lanzamiento', '$trailer')";

            $resultado = self::consulta($sql);


        }

        // Cantidad de videojuegos, usuarios, plataformas...
        static function getCantidad($tabla = "VIDEOJUEGO") {
            $sql = "SELECT count(*) as cantidad FROM $tabla";
            $resultado = self::consulta($sql);
            
            $cantidad = intval($resultado->fetch(PDO::FETCH_ASSOC)['cantidad']);

            return $cantidad;
        }

        static function loginCorrecto($usuario) {
            $user = $usuario->usuario;
            $password = $usuario->password;

            $sql = "SELECT usuario, password FROM USUARIO WHERE usuario='$user'";
            $resultado = self::consulta($sql);

            if($resultado) {
                $passwordHash = $resultado->fetch(PDO::FETCH_ASSOC)['password'];
                return password_verify($password,$passwordHash);
            }

            return false;
        }

        static function existeUsuario($usuario) {
            $user = $usuario->usuario;
            $sql = "SELECT count(*) as usuario FROM USUARIO WHERE usuario='$user'";
            $resultado = self::consulta($sql);

            $existe = intval($resultado->fetch(PDO::FETCH_ASSOC)['usuario']);

            return $existe;
        }

        static function crearUsuario($usuario) {
            $user = $usuario->usuario;
            $password = password_hash($usuario->password,PASSWORD_BCRYPT);
            $sql = "INSERT INTO USUARIO VALUES ('$user', '$password',0)";

            $resultado = self::consulta($sql);
        }

        static function modificarUsuario($usuario) {
            $user = $usuario->usuario;
            $password = password_hash($usuario->password,PASSWORD_BCRYPT);
            $administrador = $usuario->administrador;
            $sql = "UPDATE USUARIO SET password  = '$password', administrador = '$administrador' WHERE usuario = '$user'";
            $resultado = self::consulta($sql);
            
        }

        static function juegosDisponiblesPlataforma($videojuego, $plataforma) {
            $idVideojuego = $videojuego->id;
            $sql = "SELECT count(*) as cantidad from CLAVE where id_videojuego='$idVideojuego' AND id_plataforma='$plataforma' AND recibida = 0 AND id_usuario IS NULL";
            $resultado = self::consulta($sql);
            return $cantidad = intval($resultado->fetch(PDO::FETCH_ASSOC)['cantidad']);
        }

        static function obtenerPlataforma($id) {
            $sql = "SELECT plataforma FROM PLATAFORMA WHERE id='$id'";
            $resultado = self::consulta($sql);

            return $resultado->fetch(PDO::FETCH_ASSOC)['plataforma'];
        }

        static function obtenerPlataformas() {
            $sql = "SELECT * FROM PLATAFORMA";
            
            $resultado = self::consulta($sql);
            $plataformas = [];

            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($plataformas, $row);
                }
            }
            return $plataformas;

        }


        static function obtenerGeneros() {
            $sql = "SELECT * FROM GENERO";
            
            $resultado = self::consulta($sql);
            $generos = [];

            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($generos, $row);
                }
            }
            return $generos;

        }
        
        static function obtenerIdiomasVideojuego($videojuego) {
            $idVideojuego = $videojuego->id;

            $sql = "SELECT id_idioma FROM VIDEOJUEGO_IDIOMA WHERE id_videojuego = '$idVideojuego'";

            $resultado = self::consulta($sql);

            $idiomas = [];
            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($idiomas, $row);
                }
            }
            return $idiomas;
        }

        static function obtenerGenerosVideojuego($videojuego) {
            $idVideojuego = $videojuego->id;
            $sql = "SELECT g.genero FROM GENERO g JOIN VIDEOJUEGO_GENERO vg ON g.id=vg.id_genero WHERE vg.id_videojuego=$idVideojuego";

                        $resultado = self::consulta($sql);

            $generos = [];
            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($generos, $row["genero"]);
                }
            }
            return $generos;
            
        }

        static function usuarioAdministrador($usuario) {
            $nombreUsuario = $usuario->usuario;
            $sql = "SELECT administrador FROM USUARIO WHERE usuario='$nombreUsuario'";
            $resultado = self::consulta($sql);
            return $esAdmin = intval($resultado->fetch(PDO::FETCH_ASSOC)['administrador']);

            if($esAdmin) {
                return true;
            } 
            return false;

        }

        static function eliminarUsuario($usuario) {
            $nombreUsuario = $usuario->usuario;
            $sql = "DELETE FROM USUARIO WHERE usuario = '$nombreUsuario'";
            $resultado = self::consulta($sql);
        }

        static function obtenerCamposTabla($tabla) {
            $sql = "select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = 'getgames' and TABLE_NAME = '$tabla'";
            $resultado = Conexion::consulta($sql);

            $campos = [];
            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($campos, $row["COLUMN_NAME"]);
                }
            }
            return $campos;
        }

        static function obtenerUsuarios() {
            $sql = "SELECT * FROM USUARIO";
            $resultado = Conexion::consulta($sql);

            $usuarios = [];
            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($usuarios, new Usuario($row));
                }
            }
            return $usuarios;
        }

        static function existeVideojuego($videojuego) {
            $titulo = $videojuego->titulo;
            $sql = "SELECT count(*) as resultado FROM VIDEOJUEGO WHERE titulo='$titulo'";
            $resultado = self::consulta($sql);

            $existe = intval($resultado->fetch(PDO::FETCH_ASSOC)['resultado']);

            return $existe;

        }


        static function modificarPrecioVideojuego($videojuego, $precio, $descuento) {
            $id = $videojuego->id;
           
            $sql = "UPDATE VIDEOJUEGO SET precio  = $precio, rebaja = $descuento WHERE id = '$id'";
            $resultado =  self::consulta($sql);

        }

        static function obtenerClaves() {
            $sql = "SELECT * FROM CLAVE";
            $resultado = self::consulta($sql);

            $claves = [];
            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($claves, new Clave($row));
                }
            }
            return $claves;
        }

        static function obtenerClavesDisponibles($idVideojuego, $idPlataforma) {
            $sql =  "SELECT * FROM CLAVE WHERE id_videojuego = $idVideojuego AND id_plataforma = $idPlataforma AND recibida = 0 AND id_usuario IS NULL";
            $resultado = self::consulta($sql);

            $claves = [];
            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($claves, new Clave($row));
                }
            }
            return $claves;

        }

        static function anadirClave($clave) {
            $key = $clave->clave;
            $plataforma = $clave->id_plataforma;
            $videojuego = $clave->id_videojuego;

            $sql = "INSERT INTO CLAVE VALUES ('$key', 0, NULL, '$plataforma', '$videojuego')";

            $resultado = self::consulta($sql);
        }

        static function asignarClave($clave, $usuario) {
            $sql = "UPDATE CLAVE SET recibida = 1, id_usuario = '$usuario' WHERE clave = '$clave->clave'";
            $resultado = Conexion::consulta($sql);

        }

        static function obtenerUltimosVideojuegos($total) {
            $sql = "SELECT * FROM VIDEOJUEGO ORDER BY fecha_lanzamiento DESC LIMIT $total";

            $resultado = Conexion::consulta($sql);
            $videojuegos = [];
            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($videojuegos, new Videojuego($row));
                }
            }
            return $videojuegos;
        }

        static function obtenerVideojuegosMasBaratos() {
            $sql = "SELECT * FROM VIDEOJUEGO WHERE (precio*rebaja)/100 < (SELECT AVG(precio*rebaja/100) FROM VIDEOJUEGO)  LIMIT 8";
            $resultado = self::consulta($sql);

            $videojuegos = [];
            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($videojuegos, new Videojuego($row));
                }
            }
            return $videojuegos;

        }

        static function obtenerUltimoIdVideojuegos() {
            $sql = "select id from VIDEOJUEGO ORDER BY id DESC LIMIT 1";
            $resultado = self::consulta($sql);

            $id = $resultado->fetch(PDO::FETCH_ASSOC)['id'];

            return $id;
        }

        static function obtenerNumeroClavesVideojuego($id, $plataforma) {
            $sql = "SELECT count(*) as total FROM CLAVE WHERE id_videojuego=$id AND id_plataforma=$plataforma AND recibida = 0 AND id_usuario IS NULL";
            $resultado = self::consulta($sql);

            $total = intval($resultado->fetch(PDO::FETCH_ASSOC)['total']);

            return $total;
        }

        static function obtenerMisVideojuegos($usuario) {
            $sql = "SELECT DISTINCT id_videojuego FROM CLAVE WHERE id_usuario = '$usuario' AND recibida = 1";
            $resultado = self::consulta($sql);

            $videojuegos = [];
            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($videojuegos, self::obtenerVideojuego($row['id_videojuego']));
                }
            }
            return $videojuegos;
        }

        static function obtenerMisClavesVideojuego($idVideojuego,$usuario) {
            $sql = "SELECT * FROM CLAVE WHERE id_videojuego = '$idVideojuego' AND id_usuario = '$usuario' AND recibida = 1";

            $resultado = self::consulta($sql);

            $claves = [];
            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($claves, new Clave($row));
                }
            }
            return $claves;

        }

        static function obtenerCiertosVideojuegos($sql) {
            $resultado = self::consulta($sql);
            $videojuegos = [];

            if($resultado) {
                while($row = $resultado->fetch(PDO::FETCH_ASSOC)) {
                    array_push($videojuegos, new Videojuego($row));
                }
            }

            return $videojuegos;
        }

        static function obtenerNumeroClavesUsuario($usuario) {
            $sql = "select count(*) total from CLAVE where id_usuario='$usuario'";
            $resultado = self::consulta($sql);

            $total = intval($resultado->fetch(PDO::FETCH_ASSOC)['total']);

            return $total;
        }

        function claveRepetida($clave) {
            $key = $clave->clave;
            $sql = "select count(*) total from CLAVE where clave='$key'";

            $resultado = self::consulta($sql);

            $total = intval($resultado->fetch(PDO::FETCH_ASSOC)['total']);

            return $total;

        }
    }



?>